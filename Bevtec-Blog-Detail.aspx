﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bevtec-Blog-Detail.aspx.cs" Inherits="Bevtec_Blog_Detail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="images/bevtec-images/Bevtec-Logo-150.png" />
    <title>BevTec</title>

    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />

    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->
    <script>
        function validateEmailAddress(target) {
            if (target.value != '' || target.value != null) {
                var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (target.value.length > 0) {
                    if (target.value.match(emailExpression)) {
                        document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageValid';
                        document.getElementById("<%= hfCanSave.ClientID %>").value = "true";
                    }
                    else {
                        document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                        document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
                    }
                }
                else {
                    document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                    document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
                }
            }
            else {
                document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
            }
        }

        function showInProgress(target) {
            if (target.value != '' || target.value != null) {
                target.className = 'validationImageInProgress';
            }
        }
    </script>
</head>
<body class="page page-id-1662 page-template-default  jt_main_content wpb-js-composer js-comp-ver-4.7.4 vc_responsive jt-sticky-enabled">
    <form id="form1" runat="server">
        <div class="wrapper">
            <!-- Wrapper -->
            <div class="container-fluid padding-zero jt-main-banner-holder">
                <div class="jt-page-header  jt-blog-page">
                    <div class="jt-banner-overlay" style="background: ;"></div>
                    <header class=" sticky-nav sticky-rev">
                        <nav class="navbar navbar-default navbar-static-top">
                            <div class="">
                                <div class="navbar-header">
                                    <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                        <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                    </a>
                                    <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                        <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                    </a>
                                </div>
                                <div class="menu-metas navbar-default navbar-right">
                                    <ul class="navbar-nav">
                                        <%--  <li id="jt-top-cart" class="jt-menu-cart">
                                            <a href="#" id="jt-top-cart-trigger">
                                                <img src="images/icons/woo-cart.png" alt="">
                                                <span class="jt-cart-num">1</span> </a>
                                            <div class="top-cart-content">
                                                <div class="header_cart_widget">
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>--%>
                                    </ul>
                                </div>
                                <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                    <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-AboutUs.aspx">About Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Categories.aspx">Categories</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Products.aspx">Products</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Blog.aspx">Blog</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Cart.aspx">
                                                <img src="images/icons/shop-cart.png" alt=" " />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <!-- Slim Menu -->
                        <div class="hidden-big-screen ">
                            <div class=" sticky-nav sticky-rev jt-slim-top">
                                <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                    <img src="upload/logo-dark.png" alt="Juster" />
                                </a>
                                <div class="menu-main-menu-container">
                                    <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-AboutUs.aspx">About Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Categories.aspx">Categories</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Products.aspx">Products</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Cart.aspx">
                                                <img src="images/icons/shop-cart.png" alt=" " />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /Slim Menu -->
                    </header>

                    <div class="slider-container jt-vintage-banner jt-vint-small-banner" style="background: url('images/Banners/Hand-Crafted-Beer.png'); background-size: cover;">
                    </div>
                    <div class="jt-agency-banner-content jt-arch-bantext-3null">
                        <h1 style="color: white" class="page_heading">Blog Post</h1>
                    </div>
                    <div class="jt-banner-graphic">
                        <div class="jt-ban-icon faa-burst animated fa-fast"></div>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1100ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1s" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="900ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="800ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="700ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="600ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="500ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="400ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="300ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                    </div>
                </div>
            </div>

            <div class="container-fluid padding-zero jt_content_holder">
                <div class="entry-content page-container content-ctrl">
                    <!-- Main Container -->
                    <div class="container-fluid padding-zero">
                        <!-- Container -->
                        <div class="container main-content-center">

                            <div class="main-content col-md-8 col-sm-7">
                                <article id="post-804" class="jt-each-post post-804 post type-post status-publish format-video has-post-thumbnail hentry category-agency category-graphic category-life-style post_format-post-format-video">
                                    <asp:Repeater ID="rpBlogsDetail" runat="server">
                                        <ItemTemplate>
                                            <div class="jt-featured-img">
                                                <div class="video-img-overlay">
                                                    <a class="popup-vimeo" href='<%#Eval ("strVideoLink") %>'>
                                                        <img src='<%#Eval ("VideoImage") %>' alt=""></a>
                                                    <br>
                                                </div>
                                                <img width="960" height="550" src='<%#Eval ("FullPathForImage") %>' class="attachment-post-thumbnail wp-post-image" alt="11" />
                                            </div>
                                            <div class="jt-post-content">
                                                <h3 class="jt-post-title"><%#Eval ("strTitle") %></h3>
                                                <ul class="jt-post-list-metas">
                                                    <li>
                                                        <img src="images/icons/clock.png" height="16" width="16" alt=""><%#Eval ("dtAdded") %></li>
                                                    <li>
                                                </ul>
                                                <div class="jt-post-excerpt">
                                                    <p><%#Eval ("strDescription") %></p>

                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </article>

                                <!-- Comments Area -->
                                <div class="jt-comments comments-area" id="comments">
                                    <div class="jt-comment-form sep-hover-control">
                                        <div id="respond" class="comment-respond">
                                            <div class="sep-hover-control">
                                                <h2 class="author-title">Comments<span class="jt-sep-two"></span>
                                                </h2>
                                            </div>
                                            <div class="author-info" style="margin-top: 0px; margin-bottom: 20%">
                                                <asp:Repeater ID="RepComentPosts" runat="server">
                                                    <ItemTemplate>
                                                        <div class="author-desc">
                                                            <h6><%#Eval ("strName") %></h6>
                                                            <p><%#Eval ("strMessage") %></p>
                                                        </div>
                                                        <span class="jt-sep-two-right"></span>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>



                                            <h3 id="reply-title" class="comment-reply-title">Leave a Reply<span class="jt-sep-two"></span> <small><a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond" style="display: none;"><i class="fa fa-times-circle"></i></a></small></h3>
                                            <div action="#" method="post" id="commentform" class="comment-form">


                                                <div class="jt-form-inputs col-sm-6 padding-zero">

                                                    <div class="controlDiv">
                                                        <div class="imageHolderCommonDiv">
                                                            <div id="div1" runat="server" class="validationImageMandatory"></div>
                                                        </div>
                                                        <asp:TextBox ID="txtName" name="author" value="" TabIndex="1" placeholder="Name" onblur="setValid(this)" runat="server"></asp:TextBox>
                                                    </div>

                                                    <div class="controlDiv">
                                                        <div class="imageHolderCommonDiv">
                                                            <div id="divImageHolder" runat="server" class="validationImageMandatory"></div>
                                                        </div>
                                                        <asp:TextBox ID="txtEmail" name="email" value="" TabIndex="2" placeholder="Email" onblur="validateEmailAddress(this)" runat="server"></asp:TextBox>
                                                        <asp:HiddenField ID="hfCanSave" runat="server" Value="false" />
                                                    </div>
                                                    <div class="controlDiv">
                                                        <div class="imageHolderCommonDiv">
                                                            <div id="div2" runat="server" class="validationImageMandatory"></div>
                                                        </div>
                                                        <asp:TextBox ID="txtPhoneNumber" name="url" value="" TabIndex="3" placeholder="Phone" onblur="setValid(this)" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="jt-form-textarea col-sm-6 padding-zero">
                                                    <div class="controlDiv">
                                                        <div class="imageHolderCommonDiv">
                                                            <div id="div3" runat="server" class="validationImageMandatory"></div>
                                                        </div>
                                                        <asp:TextBox ID="txtMessage" name="url" value="" TabIndex="3" placeholder="Message" runat="server" onblur="setValid(this)" TextMode="MultiLine"></asp:TextBox>
                                                    </div>


                                                </div>
                                                <p class="form-submit">
                                                    <asp:Button ID="BtnSubmit" CssClass="submit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" />
                                                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>

                                                </p>

                                            </div>
                                        </div>
                                        <!-- #respond -->
                                    </div>

                                </div>
                                <!-- end comments-area -->
                            </div>
                            <div class="col-md-4 col-sm-5 padding-right-zero">
                                <div class="sidebar ">
                                    <div id="search-3" class="widget sep-hover-control widget_search">
                                        <div method="get" id="searchform" action="#" class="search-new">
                                            <div>
                                                <label class="screen-reader-text" for="s"></label>
                                                <input type="text" name="s" id="s" placeholder="Search Here" />
                                                <input type="submit" id="searchsubmit" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget -->
                                    <div id="recent-posts-2" class="widget sep-hover-control widget_recent_entries">
                                        <h3 class="widget-title">Recent Posts</h3>
                                        <div class="jt-sep-two"></div>
                                        <ul>
                                            <asp:Repeater ID="RepetRecentPosts" runat="server">
                                                <ItemTemplate>
                                                    <li>
                                                        <a href='<%#Eval ("Link") %>'><%#Eval ("strTitle") %></a>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                    <!-- end widget -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Container -->
                </div>
            </div>
            <!-- End Main Container -->
            <div class="jt-nxt-pre-posts">
                <a href="../the-love-boat/index.html" class="jt-prev-post">Prev Post</a>
                <a href="../bunch-thats-the/index.html" class="jt-next-post">Next Post</a>
            </div>

            <div class="container-fluid padding-zero foot-ctrl">
                <footer class="jt-footer-style-two   ">

                    <div class="col-lg-8 col-md-8 col-sm-8 ">
                        <!-- Footer Widgets -->
                        <div class="container">
                            <p>
                                <div class="" style="height: 20px;"></div>
                                <img src="images/bevtec-images/Untitled-1.png" alt="" />
                                <div class="" style="height: 20px;"></div>
                                <ul class="jt-social-one social_eight_center">
                                    <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                                    <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                                    <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                                    <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                                    <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <!-- Copyright Widget Area -->

                    <div class="col-lg-3 col-md-3 col-sm-3 ">
                        <div class="widget">
                            <div class="jt-widget-content">
                                <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                    <h3 class="widget-title">Keep in Touch</h3>
                                    <ul class="jt-widget-address">
                                        <li class="jt-add-icon jt-add-li">
                                            <i class="pe-7s-map-2"></i>
                                            <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                        </li>
                                           <li class="jt-add-icon jt-mail-li">
                                            <i class="pe-7s-mail"></i>
                                             <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                        </li>
                                        <li class="jt-add-icon jt-phone-li">
                                            <i class="pe-7s-call"></i>
                                            <span>+27  83 709 6554</span>
                                        </li>
                                     
                                        <li class="jt-add-icon jt-fax-li">
                                            <i class="pe-7s-call"></i>
                                            <span>+27  83 785 4996</span>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end widget -->
                            </div>
                        </div>
                    </div>
                    <!-- Footer Copyrights -->
                </footer>
            </div>
        </div>

        <div>
        </div>
    </form>
    <script type='text/javascript' src='js/jquery/jquery.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

    <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            // Sticky Navbar
            $(".sticky-nav").sticky({
                topSpacing: 0
            });
        });
    </script>
    <script type='text/javascript' src='js/jquery.form.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
    <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
    <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
    <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

    <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type='text/javascript' src='js/owl.carousel.min.js'></script>
    <script type='text/javascript' src='js/animate-plus.min.js'></script>
    <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/scripts.js'></script>
    <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
    <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.sticky.js'></script>
    <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
    <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
    <script type='text/javascript' src='js/dynamic-js.js'></script>
    <script type='text/javascript' src='js/js_composer_front.js'></script>
    <script type='text/javascript' src='js/wow.min.js'></script>
    <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
    <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
    <script type='text/javascript' src='js/tiltfx.js'></script>
    <script type='text/javascript' src='js/ellipsis.js'></script>
    <script type='text/javascript' src='js/chaffle.js'></script>
    <script type='text/javascript' src='js/contact-form.js'></script>

    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_single_product_params = { "i18n_required_rating_text": "Please select a rating", "review_rating_required": "yes" };
        /* ]]> */
    </script>
    <script type='text/javascript' src='js/frontend/single-product.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
</body>
</html>
