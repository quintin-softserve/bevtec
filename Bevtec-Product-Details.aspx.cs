﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Bevtec_Product_Details : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsProducts clsProducts;
    List<clsProducts> glstProducts;

    int productId;
    protected void Page_Load(object sender, EventArgs e)
    {
        //### If the iProductID is passed through then we want to instantiate the object with that iProductID
        if ((Request.QueryString["iProductID"] != "") && (Request.QueryString["iProductID"] != null))
        {
            clsProducts = new clsProducts(Convert.ToInt32(Request.QueryString["iProductID"]));
            productId = clsProducts.iProductID;
            if (!IsPostBack)
            {
                //### Populate the form
                popProductDetails(clsProducts.iProductID);
                popCarteLink(clsProducts.iProductID);
                populateList();
            }

        }
        else
        {
            clsProducts = new clsProducts();
        }
        Session["clsProducts"] = clsProducts;

    }
    protected void btnAddToCart_Click(object sender, EventArgs e)
    {
        int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);


        clsCart.addToCart(iProductID, Convert.ToInt32(txtQuantity.Text));
        Response.Redirect("Bevtec-Cart.aspx");
    }
    private void populateList()
    {
        List<string> totalProducts = new List<string>();
        DataTable dtProductsList = clsProducts.GetProductsList();
        int iCount = 0;
        foreach (DataRow dtrListProducts in dtProductsList.Rows)
        {
            ++iCount;
            totalProducts.Add("" + dtrListProducts["iProductID"]);

        }
        int index = totalProducts.IndexOf("" + productId);
        int prev = index - 1;
        if (prev < 0)
        {

        }
        else
        {
            string productid = totalProducts[prev];
            HypePreview.NavigateUrl = "Bevtec-Product-Details.aspx?iProductID=" + productid;


        }
        index = totalProducts.IndexOf("" + productId);
        prev = index + 1;
        if (prev > iCount - 1)
        {

        }
        else
        {
            string productid = totalProducts[prev];
            HyperNext.NavigateUrl = "Bevtec-Product-Details.aspx?iProductID=" + productid;


        }



    }
    private void popCarteLink(int iProductID)
    {

        DataTable dtProducts = clsProducts.GetProductsList("iProductID='" + iProductID + "'", "");

        int iBestSellerCount = dtProducts.Rows.Count;


        DataTable dtNewProductList = new DataTable();

        dtNewProductList = dtProducts.Clone();
        dtNewProductList.Clear();

        int iCount = 0;

        foreach (DataRow dtrCategoryProducts in dtProducts.Rows)
        {
            int iProductTotal = dtProducts.Rows.Count;

            foreach (DataRow dtrProduct in dtProducts.Rows)
            {


                if (Convert.ToInt32(dtrCategoryProducts["iProductID"]) == Convert.ToInt32(dtrProduct["iProductID"]))
                {

                    dtNewProductList.ImportRow(dtrProduct);
                }
            }

        }

        rpLinkCaer.DataSource = dtNewProductList;
        rpLinkCaer.DataBind();

    }
    private void popProductDetails(int iProductID)
    {
        string strFullPathToImage = "";


        StringBuilder sbProductDetails = new StringBuilder();
        StringBuilder sbProductDetailsLower = new StringBuilder();

        sbProductDetails.AppendLine("<div class='span6 module_cont module_text_area module_none_padding'>");
        sbProductDetails.AppendLine("<ul class='add-to-links' id='etalage'>");

        if (!(clsProducts.strPathToImages.ToString() == "") || (clsProducts.strPathToImages == null))
        {

        }
        if (!(clsProducts.strPathToImages.ToString() == "") || (clsProducts.strPathToImages == null))
        {
            strFullPathToImage = "Products/" + clsProducts.strPathToImages + "/" + clsProducts.strMasterImage;
        }
        else
        {
            strFullPathToImage = "img/no-image.png";
        }
        sbProductDetails.AppendLine("<div class='images'>");
        sbProductDetails.AppendLine("<a href='" + strFullPathToImage + "' itemprop='image' class='woocommerce-main-image zoom format-image-popup jt-featured-img' title='' data-rel='prettyPhoto'><img width='546' height='600' src='" + strFullPathToImage + "' class='attachment-shop_single wp-post-image' alt='product-4' title='product-4' /></a>");
        sbProductDetails.AppendLine("</div>");
        sbProductDetails.AppendLine("<div class='summary entry-summary'>");
        sbProductDetails.AppendLine("<h1  class='product_title entry-title'>" + clsProducts.strTitle + "</h1>");
        sbProductDetails.AppendLine("<div class='woocommerce-product-rating' >");
        sbProductDetails.AppendLine("<div class='star-rating'>");
        sbProductDetails.AppendLine(" <span style='width:100%'>");
        sbProductDetails.AppendLine("<strong class='rating'>/strong> out of <span></span> based on <span class='rating'></span> customer rating </span>");
        sbProductDetails.AppendLine("</div>");
        sbProductDetails.AppendLine("<a href='#reviews' class='woocommerce-review-link' rel='nofollow'>(<span  class='count'>1</span> rating)</a> </div>");
        sbProductDetails.AppendLine("<div>");
        sbProductDetails.AppendLine("<p class='price'><span class='amount'>" + clsProducts.dblPrice + "</span></p>");
        sbProductDetails.AppendLine("</div>");
        sbProductDetails.AppendLine("<div class='prd_descrip'>");
        sbProductDetails.AppendLine("<p style='margin-bottom: 5px margin-top: 10px;'>Short Description</p>");
        sbProductDetails.AppendLine("<p style=' margin: 0;margin-bottom: 10px'>" + clsProducts.strTagLine + "</p>");
        sbProductDetails.AppendLine("</div>");
        litProducts.Text = sbProductDetails.ToString();
        StringBuilder sbProductDescription = new StringBuilder();
        sbProductDescription.AppendLine("<p>" + clsProducts.strDescription + "</p>");
        LitDescription.Text = sbProductDescription.ToString();

        litProducts.Text = sbProductDetails.ToString();
        populatePdfs(clsProducts.iProductID);

    }
    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products";


    public void populatePdfs(int iProductID)
    {
        DataTable dtProducts = clsProducts.GetProductsList("iProductID='" + iProductID + "'", "");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForPdf");
        dtProducts.Columns.Add("PdfName");


        foreach (DataRow dtrProducts in dtProducts.Rows)
        {

            if (!(dtrProducts["strMasterDocument"].ToString() == "") || (dtrProducts["strMasterDocument"] == null) || (dtrProducts["strPathToDocument"] == null))
                {
                    dtrProducts["FullPathForPdf"] = "Documents/" + dtrProducts["strPathToDocument"] + "/" + dtrProducts["strMasterDocument"];
                    dtrProducts["PdfName"] = dtrProducts["strMasterDocument"];
                }
                

            }

        rpPopulatePdf.DataSource = dtProducts;
        rpPopulatePdf.DataBind();
    }

}