﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Bevtec_Thank_youaspx : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        clearCookie();

    }
    private static void clearCookie()
    {
        HttpCookie aCookie = new HttpCookie("OrderItemsBevtec");
        aCookie.Values["OrderItemInfo"] = null;
        aCookie.Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Response.Cookies.Add(aCookie);
        HttpContext.Current.Response.Cookies["OrderItemsBevtec"]["OrderItemInfo"] = null;
        HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Session["BevtecOrder"] = "0";
    }
}