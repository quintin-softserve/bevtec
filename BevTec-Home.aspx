﻿<%@ Page Title="BevTec" Language="C#" MasterPageFile="~/BevTecMasterPage.master" AutoEventWireup="true" CodeFile="BevTec-Home.aspx.cs" Inherits="BevTec_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <div id="fullpage" class="has_animation">
        <div class="section" id="p-2155" style="background-repeat: repeat; background-attachment: fixed; background-position: center center; -webkit-background-size: cover; background-size: cover; ">
            <div class="vc_row jt_row_class wpb_row vc_row-fluid margin-bottom-zero vc_custom_1446028745430 column-have-space">
                <div class="vc_col-sm-3 jt-scrl-cnt-space-left wpb_column vc_column_container  has_animation">
                    <div>
                        <div class="space-fix " style="">
                            <div class="wpb_wrapper">
                                <div class="jt-heading jt-size-90 maring-zero" style="text-align: left;">
                                    <h1 class="jt-main-head" style="text-transform: uppercase; font-weight: 600;">Beverages</h1>
                                </div>
                                <div class="jt-heading jt-size-25 maring-zero" style="text-align: left;">
                                    <h4 class="jt-main-head" style="font-weight: 600;">And A Whole Lot More</h4>
                                    <div class="wpb_text_column wpb_content_element  style">
                                        <div class="wpb_wrapper">
                                            <div class="jt-about-wrap ">
                                                <h4 style="color: #999; font-size: 17px; font-weight: 300; margin-top: 15px">At BevTec we have over 20 years of experience in Beverage Industry Equipment.<br /></h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 22px"><span class="vc_empty_space_inner"></span></div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><span class="jt-size-15" style="color: #777; line-height: 24px; letter-spacing: 0.5px;"></span></p>

                                    </div>
                                </div>
                                <a href="BevTec-Contact-Us.aspx" class="btn-primary btn-black btn-large bg-empty " style="font-size: 11px; margin-top: 32px; padding-top: 17px; padding-right: 35px; padding-bottom: 17px; padding-left: 35px;" target="_blank">Contact Us Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_col-sm-9 wpb_column vc_column_container fadeInUp wow has_animation">
                    <div>
                        <div class="space-fix  text-right" style="">
                            <div class="wpb_wrapper">

                                <div class="wpb_single_image wpb_content_element vc_align_right   jt-img-right-space jt-img-btm-fit">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="900" height="778" src="images/Banners/PatronSaintofBeer.png" class="vc_single_image-img attachment-full" alt="layer-img-one" />
                                        </div>

                                    </figure>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="p-2159" style="background-color: #c5fbdc;">
            <div class="vc_row jt_row_class wpb_row vc_row-fluid margin-bottom-zero vc_custom_1446028745430 column-have-space">
                <div class="vc_col-sm-7 wpb_column vc_column_container fadeInDown wow has_animation" data-wow-duration="1.5s" data-wow-delay="0.5s">
                    <div>
                        <div class="space-fix " style="">
                            <div class="wpb_wrapper">

                                <div class="wpb_single_image wpb_content_element vc_align_center   jt-img-top-fit jt-img-left-space-large">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey" style="top: 14.5%;">
                                            <img width="701" height="879" src="images/Banners/water-1.png" class="vc_single_image-img attachment-full" alt="layer-img-two" />
                                        </div>
                                    </figure>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_col-sm-5 jt-scrl-cnt-space-right wpb_column vc_column_container fadeInUp wow has_animation">
                    <div>
                        <div class="space-fix  text-center" style="padding-left: 5%; padding-right: 5%;">
                            <div class="wpb_wrapper">

                                <div class="wpb_single_image wpb_content_element vc_align_center">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="106" height="112" src="images/bevtec-images/Bevtec-Logo-Transparent.png" alt="second-section-logo" />
                                        </div>
                                    </figure>
                                </div>
                                <div class="vc_empty_space" style="height: 0px"><span class="vc_empty_space_inner"></span></div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <h2 class="jt-size-36" style="font-weight: 600; letter-spacing: 3px; line-height: 50px;">Get The Best Water Equipment With BevTec.</h2>

                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 0px"><span class="vc_empty_space_inner"></span></div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><span class="jt-size-19" style="color: #666; font-family: Amiri,serif; font-style: italic; letter-spacing: 1px;">A massive range of water and beverage equipment is waiting for you.</span></p>
                                        <a href="BevTec-Products.aspx?iCategoryID=12" class="btn-primary btn-black btn-large bg-empty " style="font-size: 11px; margin-top: 32px; padding-top: 17px; padding-right: 35px; padding-bottom: 17px; padding-left: 35px;" target="_blank">Explore More</a>
                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 30px"><span class="vc_empty_space_inner"></span></div>

                                <div class="wpb_single_image wpb_content_element vc_align_center">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="98" height="10" src="upload/second-section-separator.png" class="vc_single_image-img attachment-full" alt="second-section-separator" />
                                        </div>
                                    </figure>
                                </div>
                                <div class="vc_empty_space" style="height: 15px"><span class="vc_empty_space_inner"></span></div>
                               <%-- <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="wpb_wrapper">
                                            <div class="jt-counter jt-counter-three " style="">
                                                <div class="jt-num" style="color: #35373e;"><span>16</span><em>+</em></div>
                                                <div class="jt-coun-content" style="color: #666666;">Premade Layouts</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="wpb_wrapper">
                                            <div class="jt-counter jt-counter-three " style="">
                                                <div class="jt-num" style="color: #35373e;"><span>300</span><em>+</em></div>
                                                <div class="jt-coun-content" style="color: #666666;">Total Theme Pages</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="wpb_wrapper">
                                            <div class="jt-counter jt-counter-three " style="">
                                                <div class="jt-num" style="color: #35373e;"><span>200</span><em>+</em></div>
                                                <div class="jt-coun-content" style="color: #666666;">No of shortcodes</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section" id="p-2163" style="background-repeat: no-repeat; background-attachment: fixed; background-position: center center; -webkit-background-size: cover; background-size: cover; background-image: url(images/bevtec-images/GQmhdCh.gif">
            <div class="vc_row jt_row_class wpb_row vc_row-fluid margin-bottom-zero vc_custom_1446028745430 column-have-space">
                <div class="vc_col-sm-5 jt-scrl-cnt-space-left-two wpb_column vc_column_container fadeInLeft wow has_animation">
                    <div>
                        <div class="space-fix " style="">
                            <div class="wpb_wrapper">

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p>
                                            <span style="font-family: Montserrat,sans-serif; line-height: 34px; font-weight: bold; letter-spacing: 4px;color:#fff"><span class="jt-size-65">Why BevTec?<br />
                                            </span><span class="jt-size-50"></span></span>
                                             <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                               <h4 style="color: #fff; font-size: 17px; font-weight: 300; margin-top: 15px">
                                                   Over 20 years experience.<br />
                                                   We know the best and can get the best equipment suited for you.<br />
                                                   We put you, our client, first.</h4>

                                            </div>
                                        </div>
                                        </p>

                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 48px"><span class="vc_empty_space_inner"></span></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_col-sm-7 wpb_column vc_column_container fadeInUp wow has_animation">
                    <div>
                        <div class="space-fix " style="">
                            <div class="wpb_wrapper">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="p-2165" style="background-color: #ffebdc;">
            <div id="con1"  class="vc_row jt_row_class wpb_row vc_row-fluid margin-bottom-zero vc_custom_1446028745430 column-have-space">
                <div class="vc_col-sm-7 wpb_column vc_column_container fadeInLeft wow has_animation" data-wow-duration="1.5s" data-wow-delay="0.5s">
                    <div>
                        <div class="space-fix " style="">
                            <div class="wpb_wrapper">

                                <div class="wpb_single_image wpb_content_element vc_align_left   jt-img-btm-fit">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="600" height="550" src="images/bevtec-images/Equipment-2.png" class="vc_single_image-img attachment-full" alt="layer-img-four" />
                                        </div>
                                    </figure>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_col-sm-5 jt-scrl-cnt-space-right-two wpb_column vc_column_container fadeInUp wow has_animation">
                    <div>
                        <div class="space-fix " style="padding-left: 3%; padding-right: 3%;">
                            <div class="wpb_wrapper">

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <h4 class="jt-size-70" style="font-weight: bold; letter-spacing: 4px; line-height: 68px;">BevTec.</h4>
                                        <h5 class="jt-size-30" style="font-weight: bold; letter-spacing: 4px; line-height: 40px;">Here For Your Beer Equipment Needs</h5>

                                    </div>
                                </div>

                                <div class="wpb_single_image wpb_content_element vc_align_left">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="123" height="6" src="upload/small-sep.png" class="vc_single_image-img attachment-full" alt="small-sep" />
                                        </div>
                                    </figure>
                                </div>
                                <div class="vc_empty_space" style="height: 13px"><span class="vc_empty_space_inner"></span></div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                         <h4 style="color: #999; font-size: 17px; font-weight: 300; margin-top: 15px">
                                           We pride ourselves on excellent service delivery,<br />
                                           competitive prices and the fantastic technical advice.
                                      </h4>

                                    </div>
                                </div>
                                <a href="BevTec-Products.aspx?iCategoryID=10" class="btn-primary btn-black btn-large bg-empty " style="font-size: 11px; margin-top: 32px; padding-top: 17px; padding-right: 35px; padding-bottom: 17px; padding-left: 35px;" target="_blank">Explore More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="p-2168" style="background-color: #c4ebfc;">
            <div class="vc_row jt_row_class wpb_row vc_row-fluid margin-bottom-zero vc_custom_1446028745430 column-have-space">
                <div class="vc_col-sm-5 jt-scrl-cnt-space-left-three wpb_column vc_column_container fadeInLeft wow has_animation" data-wow-duration="1.5s" data-wow-delay="0.5s">
                    <div>
                        <div class="space-fix " style="">
                            <div class="wpb_wrapper">

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <h4 class="jt-size-70" style="font-weight: bold; letter-spacing: 4px; line-height: 68px;"> </h4>
                                        <h5 class="jt-size-40" style="font-weight: bold; letter-spacing: 4px; line-height: 40px; margin-bottom: 12px;"> A Different Approach To Beverage</h5>

                                    </div>
                                </div>

                                <div class="wpb_single_image wpb_content_element vc_align_left">
                                </div>
                                <div class="vc_empty_space" style="height: 13px"><span class="vc_empty_space_inner"></span></div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p class="jt-size-15" style="color: #777; letter-spacing: 0.7px; line-height: 26px;">
                                            Contact us and come have a look at the range of equipment we can supply for you!
                                        </p>

                                    </div>
                                </div>
                                <a href="BevTec-Products.aspx?iCategoryID=11" class="btn-primary btn-black btn-large bg-empty " style="font-size: 11px; margin-top: 32px; padding-top: 17px; padding-right: 35px; padding-bottom: 17px; padding-left: 35px;">Explore More</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_col-sm-7 wpb_column vc_column_container fadeInUp wow has_animation">
                    <div>
                        <div class="space-fix " style="padding-left: 3%; padding-right: 3%;">
                            <div class="wpb_wrapper">

                                <div class="wpb_single_image wpb_content_element vc_align_right   jt-img-btm-fit jt-img-right-space-large">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="734" height="796" src="images/bevtec-images/Pin-Up-Girl.png" class="vc_single_image-img attachment-full" alt="layer-img-five" />
                                        </div>
                                    </figure>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        jQuery('a[href^="#"]').click(function (e) {
            jQuery('html,body').animate({ scrollTop: jQuery(this.hash).offset().top }, 1000);
            return false;
            e.preventDefault();
        });
    </script>
</asp:Content>

