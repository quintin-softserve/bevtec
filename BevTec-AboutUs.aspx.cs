﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BevTec_AboutUs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        popTestemonials();
    }
    private void popTestemonials()
    {
        DataTable dtTestemonials = clsCustomerTestimonials.GetCustomerTestimonialsList();
        rpTestemonials.DataSource = dtTestemonials;
        rpTestemonials.DataBind();
    }
}