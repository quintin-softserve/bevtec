﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;


public partial class Bevtec_ConactUs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtEmail);
        bCanSave = clsValidation.IsNullOrEmpty(txtMessage);
        bCanSave = clsValidation.IsNullOrEmpty(txtMessage);


        if ((bCanSave == true) && ((Convert.ToBoolean(hfCanSave.Value)) == true))
        {
            if (txtName.Text != null & txtName.Text != "Name" & txtMessage.Text != "")
            {
                if (txtMessage.Text != null & txtMessage.Text != "Message")
                {

                    mandatoryDiv.Visible = false;
                    SendEmail();
                    SendAdminEmail();
                }
            }
            else
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" >Please fill out all mandatory fields</div>";

            }

        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" >Please fill out all mandatory fields</div>";

        }
    }
       

          


                
    protected void SendAdminEmail()
    {
        Attachment[] empty = new Attachment[] { };
        string strContent = "";

        StringBuilder strbMail = new StringBuilder();
        strbMail.AppendLine("<table style='max-width:600px;'>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<p style='font-family:Calibri;'>From:&nbsp;&nbsp;&nbsp;" + txtName.Text + "<br/>    Email:&nbsp;&nbsp;&nbsp;" + txtEmail.Text + "</p><br />");
        strbMail.AppendLine("<div  style='width:100%;'>");
        strbMail.AppendLine("<p style='font-family:Calibri;text-align:center'>Message</p>");
        strbMail.AppendLine("<p style='font-family:Calibri;text-align:center'>" + txtMessage.Text + "</p>");
        strbMail.AppendLine("</div>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail(txtEmail.Text, "quintin@softservedigital.co.za", "", "", "Website Contact Form - Bevtec", strContent, empty, true);
        txtName.Text = "Name";
        txtMessage.Text = "Message";
        txtEmail.Text = "Email";
    }

    protected void SendEmail()
    {
        Attachment[] empty = new Attachment[] { };
        string strContent = "";

        StringBuilder strbMail = new StringBuilder();
        strbMail.AppendLine("<table style='max-width:600px;'>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<p style='font-family:Calibri;text-align:center'>Thank you for your enquiry. Your message is as follows:</p>");
        strbMail.AppendLine("<br/><br/></td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<div  style='width:100%;'>");
        strbMail.AppendLine("<p style='font-family:Calibri;text-align:center'>Message</p>");
        strbMail.AppendLine("<p style='font-family:Calibri;text-align:center'>" + txtMessage.Text + "</p>");
        strbMail.AppendLine("</div>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail("quintin@softservedigital.co.za", txtEmail.Text, "", "", "Website Contact Form - Bulbul", strContent, empty, true);

    }

}
