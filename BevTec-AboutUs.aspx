﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BevTecMasterPage.master" AutoEventWireup="true" CodeFile="BevTec-AboutUs.aspx.cs" Inherits="BevTec_AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <div class="container-fluid padding-zero jt_content_holder">
        <div class="entry-content page-container content-ctrl">
            <!-- Main Container -->
            <div class="container-fluid padding-zero">
                <!-- Container -->
                <div class="main-content col-lg-12 padding-zero">
                    <article id="post-11" class="post-11 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1443153938739 column-have-space">
                                <div class="container">
                                    <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                        <div>
                                            <div class="space-fix " style="">
                                                <div class="wpb_wrapper">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_text_column wpb_content_element ">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="jt-arch-title ">
                                                                            <h1 class="jt-arch-title-h1-auto " style="">Beverage Dispensing </h1>
                                                                            <h1 class="jt-arch-title-h1-auto " style="">Equipment</h1>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="wpb_text_column wpb_content_element  vc_custom_1444928935307">
                                                                    <div class="wpb_wrapper">
                                                                        <p><span style="font-size: 16px;"><strong>Beverage equipment supplier to the Micro breweries and Beverage manufacturing companies in the South African market.</strong></span></p>

                                                                    </div>
                                                                </div>

                                                                <div class="wpb_text_column wpb_content_element  para-bottom-space">
                                                                    <div class="wpb_wrapper">
                                                                        <p>"We have all that you need from push in fittings, Food grade tubing, couplers, Kegs, Beer, water and soda dispensing equipment.</p>
                                                                        <p>
                                                                            Premix and Post mix solutions and terminology Premix is where the content of product have been blended together in some form of container like a beer keg prior to it going thru a dispensing system to either cool or heat the product to the specific application required
                                                                            Post Mix is where the product will be mixed at the point of dispensing like a soda fountain unit where you would find a ration of Co2 Gas ,Water and Syrup blended to its required need at temperature required
                                                                        </p>
                                                                        <p>Bevtec is well established since 1996 and have been in the industry with quality products used in your local restaurant or bar.We assist with line cleaning to the Gauteng market of draft outlets</p>
                                                                    </div>
                                                                </div>

                                                                <div class="wpb_text_column wpb_content_element  vc_custom_1442840369947">
                                                                    <div class="wpb_wrapper">
                                                                        <%-- <p><a href="#0" target="_self" class="jt-custom-btn jt-btn-space " style="font-size: 10px;">Team Mates</a><a href="#0" target="_self" class="jt-custom-btn jt-btn-space " style="font-size: 10px;">Our Work</a></p>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_custom_1442924310657">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_single_image wpb_content_element vc_align_right">

                                                                    <figure class="wpb_wrapper vc_figure">
                                                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                            <img width="570" height="470" src="images/Bevtec-Logo-17.png" class="vc_single_image-img attachment-full" alt="architech-studio" />
                                                                        </div>
                                                                    </figure>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1442924610735">
                                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                                            <div class="wpb_wrapper">
                                                                <div class="jt-counter " style="">
                                                                    <div class="jt-num" style=""><span>387</span></div>
                                                                    <div class="jt-count-sep"></div>
                                                                    <div class="jt-coun-content" style="">Projects Finished</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                                            <div class="wpb_wrapper">
                                                                <div class="jt-counter " style="">
                                                                    <div class="jt-num" style=""><span>254</span></div>
                                                                    <div class="jt-count-sep"></div>
                                                                    <div class="jt-coun-content" style="">Happy Clients</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                                            <div class="wpb_wrapper">
                                                                <div class="jt-counter " style="">
                                                                    <div class="jt-num" style=""><span>2658</span></div>
                                                                    <div class="jt-count-sep"></div>
                                                                    <div class="jt-coun-content" style="">Working Hours</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                                            <div class="wpb_wrapper">
                                                                <div class="jt-counter " style="">
                                                                    <div class="jt-num" style=""><span>270</span>+</div>
                                                                    <div class="jt-count-sep"></div>
                                                                    <div class="jt-coun-content" style="">Beers served </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1440055062628 column-have-space">
                                <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                    <div>
                                        <div class="space-fix " style="">
                                            <div class="wpb_wrapper">
                                                <div class="jt-heading " style="text-align: center;">
                                                    <h3 class="jt-main-head" style="">Meet Our Team</h3>
                                                    <div class="jt-sep"></div>
                                                    <h4 class="sub-heading" style=""></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1442924954874 column-have-space">
                                <div class="container">
                                    <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                        <div>
                                            <div class="space-fix " style="">
                                                <div class="wpb_wrapper">
                                                    <!-- Team Members Carousel -->
                                                    <center>
                                                        <div class="jt-team-members jt-team-member-group-slide jt-team-one-wide jt-team-not-have-arrows jt-team-not-have-dots wide-vertical-team ">
                                                            <!-- Each Team Member -->
                                                            <div class="jt-each-team">
                                                                <div class="jt-team-normal">
                                                                    <img src="images/architecture/Francois-Theron.png" alt="Francois Theron" /> </div>
                                                                <div class="jt-team-over">
                                                                    <h4 style="">Francois Theron</h4>
                                                                    <div class="jt-sep-two"></div>
                                                                    <ul class="social-icons">
                                                                        <li><a href="#0" target="_blank"><i class="fa fa-facebook" style=""></i></a></li>
                                                                        <li><a href="#0" target="_blank"><i class="fa fa-twitter" style=""></i></a></li>
                                                                        <li><a href="#0" target="_blank"><i class="fa fa-linkedin" style=""></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="jt-each-team">
                                                                <div class="jt-team-normal">
                                                                    <img src="images/architecture/Jhon.png" alt="Jhon Neville" /> </div>
                                                                <div class="jt-team-over">
                                                                    <h4 style="">Jhon Neville </h4>
                                                                    <div class="jt-sep-two"></div>
                                                                    <ul class="social-icons">
                                                                        <li><a href="#0" target="_blank"><i class="fa fa-facebook" style=""></i></a></li>
                                                                        <li><a href="#0" target="_blank"><i class="fa fa-twitter" style=""></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                         
                                                        </div>
                                                        <!-- Team Members Carousel -->
                                                        </center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="cc-jt-test-center-cont-align" class="vc_row jt_row_class wpb_row vc_row-fluid jt-test-center-cont-align vc_custom_1443164083007 column-have-space">
                                <div class="container">
                                    <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                        <div>
                                            <div class="space-fix " style="">
                                                <div class="wpb_wrapper">
                                                    <div class="flnce-abt-slide flnce-abt-slide-extrawidth ">
                                                        <div class="container">
                                                            <div class="jt-heading jt-testi-head">
                                                                <p class="jt-slide-tit" style="">Testimonials</p>
                                                                <h2 style="">What They Say</h2>
                                                                <div class="jt-sep"></div>
                                                            </div>
                                                            <div id="jt-testimonial-slide" class="jt-testimonials-style-three have-number-nav">
                                                                <asp:Repeater ID="rpTestemonials" runat="server">
                                                                    <itemtemplate>
                                                                            <div class="flnce-slide-cont sep-hover-control">
                                                                                <p class="slide-cont" style="color:#35373e;font-size:20px;">“ <%#Eval ("strTestimonial") %> ”</p>
                                                                                <div class="testi-name" style="">
                                                                                 <%#Eval ("strCustomerName") %> <span class="testi-desg" style="">( <%#Eval ("strJobdesc") %> )</span>
                                                                                </div>
                                                                            </div>
                                                                </itemtemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1442841330912 column-have-space">
                                <div class="container">
                                    <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                        <div>
                                            <div class="space-fix " style="">
                                                <div class="wpb_wrapper">
                                                    <div class="jt-client-carousel jt-client-carousel-wide ">
                                                        <a href="#0" class="jt-client-logo">
                                                            <img src="images/Clients/mad-giant-logo.png" alt="client1" title="Mad Giant"/>
                                                        </a>
                                                        <a href="#0" class="jt-client-logo">
                                                            <img src="images/Clients/Draymans_LOGO_WEBSITE1-1-e1473709931759.png" alt="client2" title="Draymans" />
                                                        </a>
                                                        <a href="#0" class="jt-client-logo">
                                                            <img src="images/Clients/flare_logo1.png" alt="client3" title="Flare Bev"/>
                                                        </a>
                                                        <a href="#0" class="jt-client-logo">
                                                            <img src="images/Clients/Header_layout_960_final.png" alt="client4" title="Bottoms up"/>
                                                        </a>
                                                        <a href="#0" class="jt-client-logo">
                                                            <img src="images/Clients/logo.png" alt="client5" title="Pureau"/>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
                <!-- end main-content -->

            </div>
        </div>
        <!-- End Container -->
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid padding-zero foot-ctrl">
        <footer class="jt-footer-style-two   ">

            <div class="col-lg-8 col-md-8 col-sm-8 ">
                <!-- Footer Widgets -->
                <div class="container">
                    <p>
                        <div class="" style="height: 20px;"></div>
                        <img src="images/bevtec-images/Untitled-1.png" alt="" />
                        <div class="" style="height: 20px;"></div>
                        <ul class="jt-social-one social_eight_center">
                            <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                            <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                            <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                            <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                            <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                        </ul>
                    </p>
                </div>
            </div>
            <!-- Copyright Widget Area -->

            <div class="col-lg-3 col-md-3 col-sm-3 ">
                <div class="widget">
                    <div class="jt-widget-content">
                        <div id="widget-keep-in-touch-2" class="widget widget-custom">
                            <h3 class="widget-title">Keep in Touch</h3>
                            <ul class="jt-widget-address">
                                <li class="jt-add-icon jt-add-li">
                                    <i class="pe-7s-map-2"></i>
                                    <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                </li>
                                <li class="jt-add-icon jt-mail-li">
                                    <i class="pe-7s-mail"></i>
                                    <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                </li>
                                <li class="jt-add-icon jt-phone-li">
                                    <i class="pe-7s-call"></i>
                                    <span>+27  83 709 6554</span>
                                </li>

                                <li class="jt-add-icon jt-fax-li">
                                    <i class="pe-7s-call"></i>
                                    <span>+27  83 785 4996</span>
                                </li>
                            </ul>
                        </div>
                        <!-- end widget -->
                    </div>
                </div>
            </div>
            <div class="jt-copyright-area">
                <div class="container">
                    <p>
                        <div class="" style="height: 12px;"></div>
                        <div class="" style="height: 12px;"></div>
                        <p></p>
                </div>
            </div>
            <!-- Footer Copyrights -->
        </footer>
    </div>
</asp:Content>

