﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Bevtec_Cart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["action"] == "delete")
            {
                DeleteFromCookie(Convert.ToInt32(Request.QueryString["iProductID"]), Convert.ToInt32(Request.QueryString["iQuantity"]));

                if (Request.Cookies["OrderBevtec"] == null)
                {
                  
                }
            }
            if ((Session["BevtecOrder"] != null) && (Session["BevtecOrder"].ToString() != "0"))
            {
                populateCartFromSessionOrCookie();
                popTotals();
            }
            else
            {
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "5;url=BevTec-Products.aspx";
                this.Page.Controls.Add(meta);
                LitCartemty.Text = "  <div class='col-lg-9 padding-left-zero padding-right-cart-table'><h2>Your shopping cart is empty </br>You will now be redirected in 5 seconds</h2></div>";
                hideContent.Visible = false;
            }
        }
        else
        {

            if ((Session["BevtecOrder"] != null) && (Session["BevtecOrder"].ToString() != "0"))
            {
                populateCartFromSessionOrCookie();
                popTotals();
            }
            else
            {

            }
        }
   
    }
    protected void popTotals()
    {
        double dblCartTotal = clsCart.GetLoggedOutOrderTotal();
        double dblFinalTotal = dblCartTotal;
        spanTotal.InnerHtml = "R" + dblFinalTotal.ToString("n2");
    }

    protected void DeleteFromCookie(int iProductID, int iQuantity)
    {
        int iCartCount = clsCart.GetLoggedOutCartCount();
       

        if (iCartCount == 1)
        {
            if (Request.Cookies["BevtecOrder"] != null)
            {
                clsCart.removeItemFromCart(iProductID);
            }
            else
            {
                //divData.Visible = false;
                //divNoData.Visible = true;
            }

            clearCookie();
            Response.Redirect("Bevtec-Cart.aspx");
        }
        else
        {
            if (Request.Cookies["OrderItemsBevtec"] != null)
            {
                clsCart.removeItemFromCart(iProductID);
            }
            else
            {
                //divData.Visible = false;
                //divNoData.Visible = true;
            }
        }
    }
    private static void clearCookie()
    {
        HttpCookie aCookie = new HttpCookie("OrderItemsBevtec");
        aCookie.Values["OrderItemInfo"] = null;
        aCookie.Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Response.Cookies.Add(aCookie);
        HttpContext.Current.Response.Cookies["OrderItemsBevtec"]["OrderItemInfo"] = null;
        HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Session["BevtecOrder"] = "0";
    }
    protected void btncheckout_Click(object sender, EventArgs e) 
    {
        Response.Redirect("Bevtec-Checkout.aspx");

    }

    protected void populateCartFromSessionOrCookie()
    
    
    
    {
        if ((Session["BevtecOrder"] != null) && (Session["BevtecOrder"].ToString() != "0"))
        {
            StringBuilder sbRows = new StringBuilder();
            StringBuilder sbBackOrder = new StringBuilder();

            int iRowCount = 0;
            string iProductID = "";
            string iQuantity = "";

            string[] strCartItems = HttpContext.Current.Session["BevtecOrder"].ToString().Split(new char[] { ',' });

            foreach (string value in strCartItems)
            {
                if (!String.IsNullOrEmpty(value))
                {
                    iRowCount++;

                    if (iRowCount % 2 == 0)
                    {
                        sbRows.Append(@"<tr class='dgrAltItem'>");
                    }
                    else
                    {
                        sbRows.Append(@"<tr class='dgrItem'>");
                    }

                    string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                    iProductID = strCartItemDetails[0];
                    iQuantity = strCartItemDetails[1];
       


                    sbRows.Append(BuildCartRows(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity)));
                    sbRows.Append("</tr><tr><td><br /></td></tr>");
                }
            }

            litGridData.Text = sbRows.ToString();

            //lblGridTotal.Text = clsCart.GetLoggedOutOrderTotal().ToString("N2");
        }
        else
        {

        }
    }
    protected string BuildCartRows(int iProductID, int iQuantity)
    {
        StringBuilder sbGrid = new StringBuilder();
        {
            clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

            sbGrid.Append(@"<td class='product-name'>
                                                        <span class='product-thumbnail'>"
         + "<a href='Bevtec-Product-Details.aspx?iProductID=" + clsProducts.iProductID + "'><img width='180' height='180' src='" + getProductImage(Convert.ToInt32(iProductID)) + "' class='attachment-shop_thumbnail wp-post-image' alt='product-1' /></a></span>"
         + "<div class='product-info'>"
         + "<a class='product-title' href='#'>" + clsProducts.strTitle + "</a> </div>"
         + "</td>"

         + "<td class='product-price'>"
         + "<span class='amount'>R" + (clsProducts.dblPrice).ToString("N2") + "</span> </td>"

         + "<td class='product-quantity'>"
         + "<div class='quantity'>"
         + "<input type='number' step='1' name='cart' value='" + iQuantity + "' title='Qty' class='input-text qty text' size='" + iQuantity + "' />"
         + "</div>"
         + "</td>"

         + "<td class='product-subtotal'>"
         + "<span class='amount'>" + (clsProducts.dblPrice * Convert.ToInt32(iQuantity)).ToString("N2") + "</span> </td>"

         + "<td class='product-remove'>"
         + " <a href='Bevtec-Cart.aspx?action=delete&iProductID=" + clsProducts.iProductID + "&iQuantity=" + iQuantity + "' onclick='return ConfirmRecordDeleteCart()' class='dgrLinkDelete'><i class='pe-7s-close'></i></a> "
         + "</td>"
         );


            return sbGrid.ToString();
        }
    }
    protected string getProductImage(int iProductID)
    {
        clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

            foreach (string strImageProduct in strImagesProduct)
            {
                if (strImageProduct.Contains("_sml"))
                {
                    strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
                    break;
                }
            }
        }
        catch
        {
        }

        return strHTMLImageProduct;
    }


}