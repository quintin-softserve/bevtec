﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Bevtec_Blog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        popBlogs();


    }
    private void popBlogs()
    {

        DataTable dtblog = clsBlogPosts.GetBlogPostsList("", "strTitle ASC");
       
   

        int iBestSellerCount = dtblog.Rows.Count;
        dtblog.Columns.Add("FullPathForImage");
        dtblog.Columns.Add("Link");
        dtblog.Columns.Add("divReset");
        dtblog.Columns.Add("classForColumn");
        dtblog.Columns.Add("BlogTypeTitle");


        int iCount = 0;

        foreach (DataRow dtrblog in dtblog.Rows)
        {
            clsBlogTypes clsBlogType = new clsBlogTypes(Convert.ToInt32(dtrblog["iBlogTypeID"]));
            dtrblog["BlogTypeTitle"] = clsBlogType.strTitle;  
            ++iCount;

            if (!(dtrblog["strMasterImage"].ToString() == "") || (dtrblog["strMasterImage"] == null) || (dtrblog["strPathToImages"] == null))
            {
                dtrblog["FullPathForImage"] = "BlogPosts/" + dtrblog["strPathToImages"] + "/crop_" + dtrblog["strMasterImage"];

                dtrblog["Link"] = "Bevtec-Blog-Detail.aspx?iBlogPostID=" + dtrblog["iBlogPostID"].ToString();
            }
            else
            {
                dtrblog["FullPathForImage"] = "img/no-image.png";
            }
        }
        

        rpBlogs.DataSource = dtblog;
        rpBlogs.DataBind();

    }
}