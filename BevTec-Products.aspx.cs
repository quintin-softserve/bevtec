﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BevTec_Products : System.Web.UI.Page
{
    clsCategories clsCategories;
    protected void Page_Load(object sender, EventArgs e)
    {
        DisplayRows.Visible = false;
        DisplayList.Visible = true;
        hidePdf.Visible = false;

        if ((Request.QueryString["iCategoryID"] != "") && (Request.QueryString["iCategoryID"] != null))
        {
            int iCategoryID = Convert.ToInt32(Request.QueryString["iCategoryID"]);
            clsCategories = new clsCategories(Convert.ToInt32(Request.QueryString["iCategoryID"]));

            //### Populate the form
            NoProduct.Visible = false;
            popProducts(iCategoryID);
            popDownloadeProducts(iCategoryID);
            popCategories();
            popProductDetails();
        }
        else
        {
            if (!IsPostBack)
            {
                popProducts();
                popCategories();
                NoProduct.Visible = false;
            }
        }
    }

    #region EVENT METHODS

    protected void lnkbtnRows_Click(object sender, EventArgs e)
    {

        DisplayRows.Visible = true;
        DisplayList.Visible = false;
    }
    protected void lnkbtnList_Click(object sender, EventArgs e)
    {
        DisplayRows.Visible = false;
        DisplayList.Visible = true;
    }

    protected void btnAddToCart_Click(object sender, EventArgs e)
    {
        int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);

        clsCart.addToCart(iProductID, 1);
        Response.Redirect("Bevtec-Cart.aspx");
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        if (txtSearch.Text != "")
        {
            int iProductID = 0;
            DataTable dtProducts = clsProducts.GetProductsList("strTitle LIKE '%" + txtSearch.Text + "%' OR strDescription LIKE '%" + txtSearch.Text + "%'", "");

            int iBestSellerCount = dtProducts.Rows.Count;
            dtProducts.Columns.Add("FullPathForImage");
            dtProducts.Columns.Add("Link");
            dtProducts.Columns.Add("divReset");
            dtProducts.Columns.Add("classForColumn");
            dtProducts.Columns.Add("Price");

            int iCount = 0;

            foreach (DataRow dtrProduct in dtProducts.Rows)
            {
                ++iCount;

                if (iCount == 1)
                {
                    //dtrProduct["firstProduct"] = "";
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
                }

                if (iCount % 3 == 1 && iCount != 1)
                {
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                }

                if (iCount % 3 == 2)
                {
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                }

                if (iCount % 3 == 0)
                {
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                }

                if ((iCount % 3 == 0) || (iCount == dtProducts.Rows.Count))
                {
                    dtrProduct["divReset"] = "<div class=\"clear\"></div>";
                }
                iProductID = Convert.ToInt32(dtrProduct["iProductID"]);
                double Price = Convert.ToInt32(dtrProduct["dblPrice"]);
                if (Convert.ToInt32(dtrProduct["dblPrice"]) == 0)
                {
                    dtrProduct["Price"] = "Price on request";
                }
                else
                {
                    dtrProduct["Price"] = "R" + Price.ToString();
                }

                if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
                {
                    dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                    dtrProduct["Link"] = "Bevtec-Product-Details.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
                }
                else
                {
                    dtrProduct["FullPathForImage"] = "img/no-image.png";
                }
            }

            if (dtProducts.Rows.Count == 0)
            {
                NoProduct.Visible = true;
                mandatoryDiv.Visible = true;
                DisplayRows.Visible = false;

                lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" ><h2>No products were found matching your selection </h2> </div>";
                Hidecontent.Visible = false;


                rpProducts.DataSource = dtProducts;
                rpProducts.DataBind();
                rpProductsListing.DataSource = dtProducts;
                rpProductsListing.DataBind();
            }
            else
            {
                popDownloadeProducts(iProductID);
                NoProduct.Visible = false;
                mandatoryDiv.Visible = false;
                Hidecontent.Visible = true;

                rpProducts.DataSource = dtProducts;
                rpProducts.DataBind();
                rpProductsListing.DataSource = dtProducts;
                rpProductsListing.DataBind();
            }
        }
    }

    #endregion

    #region POP METHODS

    private void popProductDetails()
    {
        litCategoryName.Text = clsCategories.strTitle;
    }

    private void popProducts()
    {
        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");
        dtProducts.Columns.Add("Price");

        int iCount = 0;

        foreach (DataRow dtrProduct in dtProducts.Rows)
        {
            ++iCount;

            if (iCount == 1)
            {
                //dtrProduct["firstProduct"] = "";
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
            }

            if (iCount % 3 == 1 && iCount != 1)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 2)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 0)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if ((iCount % 3 == 0) || (iCount == dtProducts.Rows.Count))
            {
                dtrProduct["divReset"] = "<div class=\"clear\"></div>";
            }
            double Price = Convert.ToInt32(dtrProduct["dblPrice"]);
            if (Convert.ToInt32(dtrProduct["dblPrice"]) == 0)
            {
                dtrProduct["Price"] = "Price on request";
            }
            else
            {
                dtrProduct["Price"] = "R" + Price.ToString("#.00");
            }


            if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
            {
                dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                dtrProduct["Link"] = "Bevtec-Product-Details.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
            }
            else
            {
                dtrProduct["FullPathForImage"] = "img/no-image.png";
            }
        }
        rpProducts.DataSource = dtProducts;
        rpProducts.DataBind();
        rpProductsListing.DataSource = dtProducts;
        rpProductsListing.DataBind();
    }

    private void popProducts(int iCategoryID)
    {
        DataTable dtCategoryProducts = clsProductCategoriesLink.GetProductCategoriesLinkList("iCategoryID='" + iCategoryID + "'", "");

        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");
        dtProducts.Columns.Add("Price");

        DataTable dtNewProductList = new DataTable();

        dtNewProductList = dtProducts.Clone();
        dtNewProductList.Clear();

        int iCount = 0;
        bool containData = false;

        foreach (DataRow dtrCategoryProducts in dtCategoryProducts.Rows)
        {
          

            int iProductTotal = dtCategoryProducts.Rows.Count;

            foreach (DataRow dtrProduct in dtProducts.Rows)
            {



                if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
                {
                    dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                    dtrProduct["Link"] = "Bevtec-Product-Details.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
                }
                else
                {
                    dtrProduct["FullPathForImage"] = "img/no-image.png";
                }
                double Price = Convert.ToInt32(dtrProduct["dblPrice"]);
                if (Convert.ToInt32(dtrProduct["dblPrice"]) == 0)
                {
                    dtrProduct["Price"] = "Price on request";
                }
                else
                {
                    dtrProduct["Price"] = "R" + Price.ToString() +".00";
                }


                if (Convert.ToInt32(dtrCategoryProducts["iProductID"]) == Convert.ToInt32(dtrProduct["iProductID"]))
                {
                    containData = true;
                    ++iCount;

                    if (iCount == 1)
                    {
                        //dtrProduct["firstProduct"] = "";
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
                    }

                    if (iCount % 3 == 1 && iCount != 1)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if (iCount % 3 == 2)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if (iCount % 3 == 0)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if ((iCount % 3 == 0) || (iCount == iProductTotal))
                    {
                        dtrProduct["divReset"] = "<div class=\"clear\"></div>";
                    }

                    dtNewProductList.ImportRow(dtrProduct);
                }
                else
                {
                    NoProduct.Visible = true;
                }

            }

        }

        if (containData == false)
        {
            Hidecontent.Visible = false;
            NoProduct.Visible = true;
            mandatoryDiv.Visible = true;
            DisplayRows.Visible = false;
            lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" ><h2>No products were found matching your selection </h2> </div>";
            rpProducts.DataSource = dtNewProductList;
            rpProducts.DataBind();
            rpProductsListing.DataSource = dtNewProductList;
            rpProductsListing.DataBind();
        }
        else
        {
            rpProducts.DataSource = dtNewProductList;
            rpProducts.DataBind();
            rpProductsListing.DataSource = dtNewProductList;
            rpProductsListing.DataBind();
            mandatoryDiv.Visible = false;

        }

    }

    private void popDownloadeProducts(int iCategoryID)
    {
        DataTable dtCategoryProducts = clsProductCategoriesLink.GetProductCategoriesLinkList("iCategoryID='" + iCategoryID + "'", "");

        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");
        dtProducts.Columns.Add("FullPathForPdf");
        dtProducts.Columns.Add("PdfName");

        int iBestSellerCount = dtProducts.Rows.Count;
        DataTable dtNewProductList = new DataTable();

        dtNewProductList = dtProducts.Clone();
        dtNewProductList.Clear();

        int iCount = 0;
        bool containData = false;

        foreach (DataRow dtrCategoryProducts in dtCategoryProducts.Rows)
        {


            foreach (DataRow dtrProduct in dtProducts.Rows)
            {



                if (Convert.ToInt32(dtrCategoryProducts["iProductID"]) == Convert.ToInt32(dtrProduct["iProductID"]))
                {


                    if (!(dtrProduct["strMasterDocument"].ToString() == "") || (dtrProduct["strMasterDocument"] == null) || (dtrProduct["strPathToDocument"] == null))
                    {
                        containData = true;
                        dtrProduct["FullPathForPdf"] = "Documents/" + dtrProduct["strPathToDocument"] + "/" + dtrProduct["strMasterDocument"];
                        dtrProduct["PdfName"] = dtrProduct["strMasterDocument"];
                        dtNewProductList.ImportRow(dtrProduct);
                    }


                }
                else
                {
                    NoProduct.Visible = true;
                }

            }

        }

        if (containData == false)
        {
            hidePdf.Visible = false;
        }
        else
        {
            hidePdf.Visible = true;
            rpDownloadData.DataSource = dtNewProductList;
            rpDownloadData.DataBind();
            mandatoryDiv.Visible = false;

        }

    }
    private void popCategories()
    {
        DataTable dtCategories = clsCategories.GetCategoriesList("", "strTitle ASC");

        int iBestSellerCount = dtCategories.Rows.Count;
        dtCategories.Columns.Add("Link");



        foreach (DataRow dtrCategories in dtCategories.Rows)
        {
            dtrCategories["Link"] = "BevTec-Products.aspx?iCategoryID=" + dtrCategories["iCategoryID"].ToString();

        }
        rpCategories.DataSource = dtCategories;
        rpCategories.DataBind();
    }

    #endregion
}