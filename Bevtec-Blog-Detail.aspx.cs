﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Bevtec_Blog_Detail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Request.QueryString["iBlogPostID"] != "") && (Request.QueryString["iBlogPostID"] != null))
        {
            int iBlogPostID = Convert.ToInt32(Request.QueryString["iBlogPostID"]);

            //### Populate the form
            popBlogsDetail(iBlogPostID);
            popBlogRecentPost();
            popBlogsComents();
        }
        else
        {
            Response.Redirect("Bevtec-Blog.aspx");
        }
    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtName);
        bCanSave = clsValidation.IsNullOrEmpty(txtMessage);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmail);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhoneNumber);


        if ((bCanSave == true) && ((Convert.ToBoolean(hfCanSave.Value)) == true))
        {
            if (txtName.Text != null & txtName.Text != "Name" & txtMessage.Text != "")
            {
                if (txtMessage.Text != null & txtMessage.Text != "Message")
                {
                    SaveData();
                    //mandatoryDiv.Visible = false;
                 
                }
            }
            else
            {
                //mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" >Please fill out all mandatory fields </div>";

            }

        }
        else
        {
            //mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div style='color: red;float:' class=\"madatoryPaddingDiv\" >Please fill out all mandatory fields </div>";

        }
       
    }
    private void SaveData()
    {
        clsBlogComments clsBlogComments = new clsBlogComments();
        clsBlogComments.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogComments.iAddedBy = -10;
        //clsBlogComments.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogComments.strName = txtName.Text;
        clsBlogComments.strEmailAddress = txtEmail.Text;
        clsBlogComments.strPhoneNumber = txtPhoneNumber.Text;
        clsBlogComments.strMessage = txtMessage.Text;
        clsBlogComments.Update();
        txtMessage.Text = "";
        txtEmail.Text = "";
        txtPhoneNumber.Text = "";
        txtName.Text = "";
    }
    private void popBlogsDetail(int iBlogPostID)
    {

        DataTable dtblog = clsBlogPosts.GetBlogPostsList("iBlogPostID='" + iBlogPostID + "'", "");



        int iBestSellerCount = dtblog.Rows.Count;
        dtblog.Columns.Add("FullPathForImage");
        dtblog.Columns.Add("Link");
        dtblog.Columns.Add("divReset");
        dtblog.Columns.Add("classForColumn");
        dtblog.Columns.Add("BlogTypeTitle");
        dtblog.Columns.Add("VideoImage");


        int iCount = 0;

        foreach (DataRow dtrblog in dtblog.Rows)
        {
            clsBlogTypes clsBlogType = new clsBlogTypes(Convert.ToInt32(dtrblog["iBlogTypeID"]));
            dtrblog["BlogTypeTitle"] = clsBlogType.strTitle;
            ++iCount;

            if (!(dtrblog["strMasterImage"].ToString() == "") || (dtrblog["strMasterImage"] == null) || (dtrblog["strPathToImages"] == null))
            {
                dtrblog["FullPathForImage"] = "BlogPosts/" + dtrblog["strPathToImages"] + "/" + dtrblog["strMasterImage"];

                dtrblog["Link"] = "Bevtec-Product-Details.aspx?iBlogPostID=" + dtrblog["iBlogPostID"].ToString();
            }
            else
            {
                dtrblog["FullPathForImage"] = "img/no-image.png";
            }
            if (!(dtrblog["strVideoLink"].ToString() == "") || (dtrblog["strVideoLink"] == null) || (dtrblog["strVideoLink"] == null))
             {
                 dtrblog["VideoImage"] = "images/icons/format-video.png";

             }
            else
            {
                dtrblog["VideoImage"] = null;
            }

        }


        rpBlogsDetail.DataSource = dtblog;
        rpBlogsDetail.DataBind();
    }
    private void popBlogsComents()
    {

        DataTable dtbBlogComent = clsBlogComments.GetBlogCommentsList("bDoNotDisplay = true", "");

        RepComentPosts.DataSource = dtbBlogComent;
        RepComentPosts.DataBind();
    }
    private void popBlogRecentPost()
    {

        DataTable dtblogRecent = clsBlogPosts.GetBlogPostsList( "", "");
        dtblogRecent.Columns.Add("Link");



        int iBestSellerCount = dtblogRecent.Rows.Count;


        foreach (DataRow dtrblogRecent in dtblogRecent.Rows)
        {
            dtrblogRecent["Link"] = "Bevtec-Blog-Detail.aspx?iBlogPostID=" + dtrblogRecent["iBlogPostID"].ToString();
        }


        RepetRecentPosts.DataSource = dtblogRecent;
        RepetRecentPosts.DataBind();
    }
   
    
}