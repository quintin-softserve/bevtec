﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;

public partial class CMS_frmCMSLogin : System.Web.UI.Page
{
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUsername.Focus();

        //### Logout User
        if (Request.QueryString["action"] == "logout")
        {
            //### Clear all session variables
            Session.Clear();
        }

        if (clsDataAccess.IsServerConnected())
        {
            litConnectionStatus.Text = "<img src='CMS/images/Online-01.png' alt='Softserve CMS is Online' title='Softserve CMS is Online' height='50' width='50' />";
        }
        else
            litConnectionStatus.Text = "<img src='CMS/images/Offline-01.png' alt='Softserve CMS is Offline' title='Softserve CMS is Offline' height='50' width='50' />";       
    }

    protected void lnkbtnForgottenPassword_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = false;
        lnkbtnBackToLogin.Visible = true;
        divPassword.Visible = false;
        lnkbtnLogin.Visible = false;
        lnkbtnSubmit.Visible = true;
    }

    protected void lnkbtnBackToLogin_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = true;
        lnkbtnBackToLogin.Visible = false;
        divPassword.Visible = true;
        lnkbtnLogin.Visible = true;
        lnkbtnSubmit.Visible = false;
    }

    protected void lnkbtnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            clsUsers clsUsers = new clsUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Text));

            Session["clsUsers"] = clsUsers;
            
            Response.Redirect("CMS/frmDashboard.aspx");
        }
        catch
        {
            litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div style=\"position:relative; top:16px; left:5px; font-size:13px\">Invalid Login Credentials.</div>";

            if ((txtPassword.Text == "") || (txtPassword.Text == null))
            {
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Login requires a Password.</div>";
                divPasswordLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divPasswordTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divPasswordRight.Attributes["class"] = "roundedTextboxRightSideRed";
            }

            else if ((txtUsername.Text == "") || (txtUsername.Text == null))
            {
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Login requires an Email.</div>";
                divUsernameLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divUsernameTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divUsernameRight.Attributes["class"] = "roundedTextboxRightSideRed";
            }

            if ((txtUsername.Text == "") && (txtPassword.Text == ""))
            {
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Login requires an Email and Password.</div>";
                divUsernameLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divUsernameTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divUsernameRight.Attributes["class"] = "roundedTextboxRightSideRed";

                divPasswordLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divPasswordTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divPasswordRight.Attributes["class"] = "roundedTextboxRightSideRed";
            }
        }
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        //### Validate email
        if (txtUsername.Text == "")
        {
            litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Please enter your email address.</div>";
        }
        else
        {
            bool HasThisBeenChanged = false;

            //### Check if email address exists
            if (clsCommonFunctions.DoesRecordExist("tblUsers", "strEmailAddress = '" + txtUsername.Text + "' AND bIsDeleted = 0") == true)
            {
                //### Change password & Send mail
                string strRandomPassword = "";
                string strPassword = "";

                strRandomPassword = clsCommonFunctions.strCreateRandomPassword(8);
                strPassword = strRandomPassword;

                //### Hash random password
                strRandomPassword = clsCommonFunctions.GetMd5Sum(strRandomPassword);

                HasThisBeenChanged = true;                

                if (HasThisBeenChanged)
                { 
                    ForgottenPassword(txtUsername.Text, strRandomPassword);

                    StringBuilder strbContent = new StringBuilder();

                    strbContent.AppendLine("<div style=\"font-family:Calibri; font-weight:normal; font-size:14px;\">Dear User<br /></div><br />");
                    strbContent.AppendLine("<div style=\"font-family:Calibri; font-weight:normal; font-size:14px;\">Your password has been reset.</div><br /><br />");
                    strbContent.AppendLine("<div style=\"font-family:Calibri; font-weight:normal; font-size:14px;\">Your temporary password is shown below, please log into your account and change your password to something more suitable and easier to remember</div>:<br /><br />");
                    strbContent.AppendLine("<div style=\"font-family:Calibri; font-weight:normal; font-size:14px;\">Password: " + strPassword + "</div><br/>");

                    string strContent = strbContent.ToString();

                    //DataConnection.GetDataObject().ExecuteScalar("spForgottenPassword '" + txtUsername.Text + "', '" + strRandomPassword + "'");
                    //clsCommonFunctions.SendMail("noreply@gobundu.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, true);

                    Attachment[] empty = new Attachment[] { };

                    try
                    {
                        clsCommonFunctions.SendMail("noreply@softservedigital.co.za", txtUsername.Text, "", "andrew@softservedigital.co.za", "CMS Forgotten Password", strContent, empty, true);
                    }
                    catch { }
                }

                //### Redirect
                litValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your temporary password has been sent.</div>";
            }
            else
            {
                //### If no email address found
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">The email you have entered is not a registered <br />email address.</div>";
            }
        }
    }

    #endregion

    #region PASSWORD METHODS

    public static void ForgottenPassword(string strEmailAddress, string strRandomPassword)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@strEmailAddress", strEmailAddress),
                new SqlParameter("@strRandomPassword", strRandomPassword)
            };
        //### Executes Password Reset sp
        clsDataAccess.Execute("spForgottenPassword", sqlParameter);
    }

    #endregion
}