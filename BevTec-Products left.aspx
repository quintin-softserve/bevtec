﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BevTec-Products left.aspx.cs" Inherits="BevTec_Products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->
</head>
<body class="page-template-default  jt_main_content wpb-js-composer vc_responsive">
    <form id="form1" runat="server">
         <div class="wrapper">
        <!-- Wrapper -->
        <div class="container-fluid padding-zero jt-main-banner-holder">
            <div class="jt-page-header  jt-blog-page">
                <div class="jt-banner-overlay" style="background: ;"></div>
                <header class=" sticky-nav sticky-rev">
                    <nav class="navbar navbar-default navbar-static-top">
                        <div class="">
                            <div class="navbar-header">
                                <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                    <img src="images/logo.png" alt="Juster" />
                                </a>
                                <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                    <img src="images/logo.png" alt="Juster" />
                                </a>
                            </div>
                            <div class="menu-metas navbar-default navbar-right">
                                <ul class="navbar-nav">
                                    <li id="top-search" class="jt-menu-search">
                                        <a href="#" id="top-search-trigger">
                                            <i class="fa fa-search"></i>
                                            <i class="pe-7s-close"></i>
                                        </a>
                                    </li>
                                    <%--<li id="jt-top-cart" class="jt-menu-cart">
                                        <a href="#" id="jt-top-cart-trigger">
                                            <img src="images/icons/woo-cart-black.png" alt="">
                                            <span class="jt-cart-num">1</span> </a>
                                        <div class="top-cart-content">
                                            <div class="header_cart_widget">
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>--%>
                                </ul>
                            </div>
                                     <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                                    <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx"  class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>
                                        
                                    </li>
                                    <li class="menu-item ">
                                        <a href="BevTec-AboutUs.aspx">About Us</a>
                                    </li>
                                     <li class="menu-item ">
                                        <a href="BevTec-Products.aspx">Products</a>
                                    </li>

                                     <li class="menu-item ">
                                        <a href="BevTec-Contact-Us.aspx"> Contact Us</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="Bevtec-Cart.aspx"><img src="images/icons/shop-cart.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <!-- Slim Menu -->
                    <div class="hidden-big-screen ">
                        <div class=" sticky-nav sticky-rev jt-slim-top">
                            <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                <img src="upload/logo-dark.png" alt="Juster" />
                            </a>
                            <div class="menu-metas navbar-default navbar-right jt-slim-meta">
                                <ul class="navbar-nav">
                                    <li id="top-search-slim" class="jt-menu-search">
                                        <a href="#" id="top-search-trigger-slim">
                                            <i class="fa fa-search"></i>
                                            <i class="pe-7s-close"></i>
                                        </a>
                                    </li>
                                    <li id="jt-top-cart-slim" class="jt-menu-cart-slim">
                                        <a href="#" id="jt-top-cart-trigger-slim">
                                            <img src="images/icons/woo-cart.png" alt="">
                                            <span class="jt-cart-num">1</span> </a>
                                        <div class="top-cart-content">
                                            <div class="header_cart_widget">
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Slim Menu -->
                </header>

                <div class="slider-container jt-vintage-banner jt-vint-small-banner" style="background: url('upload/Shop.jpg'); background-size: cover;">
                    </div>
                    <div class="jt-page-banner jt-main-bantext-1null">
                        <h2 class="page_heading">Shop</h2>
                        <div class="jt-breadcrumbs">
                            <nav class="breadcrumb-trail breadcrumbs" >
                                <ul class="trail-items">
                                    <li class="trail-item trail-begin"><a href="index.html" rel="home"><span >Home</span></a>
                                    </li>
                                    <li class="trail-item trail-end"><span >Shop</span>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="jt-banner-graphic">
                        <div class="jt-ban-icon faa-burst animated fa-fast"></div>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1100ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1s" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="900ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="800ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="700ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="600ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="500ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="400ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="300ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                    </div>
                </div>
            </div>

            <div class="container-fluid padding-zero jt_content_holder">
                <div class="entry-content page-container content-ctrl">
                    <!-- Main Container -->
                    <div class="container-fluid padding-zero">
                        <!-- Container -->
                        <div class="container main-content-center">
                            <div class="main-content col-lg-12 padding-zero">
                                <article id="post-1976" class="post-1976 page type-page status-publish hentry">
                                    <div class="entry-content">
                                        <div class="vc_row jt_row_class wpb_row vc_row-fluid column-have-space">

                                            <div class="vc_col-sm-9 wpb_column vc_column_container  has_animation">
                                                <div>
                                                    <div class="space-fix " style="">
                                                        <div class="wpb_wrapper">

                                                            <!-- Products -->
                                                            <div class="vc-products jt-product-style-one jt-product-wide ">
                                                                <ul class="products">

                                                                      <asp:Repeater ID="rpProducts" runat="server">
                                                                        <ItemTemplate>
                                                                            <li class="first post-56 product type-product status-publish has-post-thumbnail product_cat-t-shirts shipping-taxable purchasable product-type-simple product-cat-t-shirts instock">

                                                                                <div class="jt-woo-product">
                                                                                    <a href="#">
                                                                                        <div class="jt-product-image"><img width="260" height="330" src='<%#Eval ("FullPathForImage") %>'class="attachment-shop_catalog wp-post-image" alt="product-14" /></div>
                                                                                        <div class="jt-product-cnt">
                                                                                            <h3><%#Eval ("strTitle") %></h3>
                                                                                            <span class="price"><span class="amount">R<%#Eval("dblPrice","{0:#,#.00}")%></span></span>
                                                                                        </div>
                                                                                    </a>
                                                                                    <a href="#" rel="nofollow" data-product_id="56" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                            </li>
                                                                       </ItemTemplate>
                                                                </asp:Repeater>
                                                                   <%-- <li class="post-67 product type-product status-publish has-post-thumbnail product_cat-occasional-outfit shipping-taxable purchasable product-type-simple product-cat-occasional-outfit instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-12-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-12" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Black Shirt</h3>

                                                                                    <span class="price"><span class="amount">&pound;46.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="67" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="post-70 product type-product status-publish has-post-thumbnail product_cat-clothing product_tag-commerce product_tag-shop sale shipping-taxable purchasable product-type-simple product-cat-clothing product-tag-commerce product-tag-shop instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-11-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-11" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Retro Outfit</h3>

                                                                                    <span class="price"><del><span class="amount">&pound;38.00</span></del> <ins><span class="amount">&pound;32.00</span></ins></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="70" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="last post-1490 product type-product status-publish has-post-thumbnail shipping-taxable purchasable product-type-simple instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-1-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-1" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Casual Suit</h3>

                                                                                    <span class="price"><span class="amount">&pound;39.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="1490" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="first post-1491 product type-product status-publish has-post-thumbnail product_cat-clothing shipping-taxable purchasable product-type-simple product-cat-clothing instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-20-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-20" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Premium Quality</h3>

                                                                                    <span class="price"><span class="amount">&pound;32.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="1491" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="post-1492 product type-product status-publish has-post-thumbnail product_cat-t-shirts shipping-taxable purchasable product-type-simple product-cat-t-shirts instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-19-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-19" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Trendy T-Shirt</h3>

                                                                                    <span class="price"><span class="amount">&pound;20.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="index6f3e.html?add-to-cart=1492" rel="nofollow" data-product_id="1492" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="post-1493 product type-product status-publish has-post-thumbnail product_cat-sport-wear product_tag-commerce product_tag-woo shipping-taxable purchasable product-type-simple product-cat-sport-wear product-tag-commerce product-tag-woo instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-18-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-18" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Beach Outfit</h3>

                                                                                    <span class="price"><span class="amount">&pound;28.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="1493" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="last post-1494 product type-product status-publish has-post-thumbnail product_cat-clothing shipping-taxable purchasable product-type-simple product-cat-clothing instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-17-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-17" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Occasional Suite</h3>

                                                                                    <span class="price"><span class="amount">&pound;35.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="1494" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>
                                                                    <li class="first post-1495 product type-product status-publish has-post-thumbnail product_cat-occasional-outfit shipping-taxable purchasable product-type-simple product-cat-occasional-outfit instock">

                                                                        <div class="jt-woo-product">
                                                                            <a href="#">

                                                                                <div class="jt-product-image"><img width="260" height="330" src="upload/product-16-260x330.jpg" class="attachment-shop_catalog wp-post-image" alt="product-16" /></div>
                                                                                <div class="jt-product-cnt">
                                                                                    <h3>Dual Shade</h3>

                                                                                    <span class="price"><span class="amount">&pound;36.00</span></span>
                                                                                </div>
                                                                            </a>

                                                                            <a href="#" rel="nofollow" data-product_id="1495" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add To Bag</a></div>
                                                                    </li>--%>
                                                                    <div class='wp-pagenavi'>
                                                                        <span class='pages'>Page 1 of 3</span><span class='current'>1</span><a class="page larger" href="#">2</a><a class="page larger" href="#">3</a><a class="nextpostslink" rel="next" href="#">&raquo;</a>
                                                                    </div>
                                                                </ul>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_col-sm-3 wpb_column vc_column_container  has_animation">
                                                <div>
                                                    <div class="space-fix " style="">
                                                        <div class="wpb_wrapper">

                                                            <div class="wpb_widgetised_column wpb_content_element">
                                                                <div class="wpb_wrapper">

                                                                    <div id="woocommerce_product_search-2" class="widget sep-hover-control woocommerce widget_product_search">
                                                                        <form role="search" method="get" class="woocommerce-product-search" action="#">
                                                                            <label class="screen-reader-text" for="s">Search for:</label>
                                                                            <input type="search" class="search-field" placeholder="Search Products&hellip;" value="" name="s" title="Search for:" />
                                                                            <input type="submit" value="Search" />
                                                                        </form>
                                                                    </div>
                                                                    <!-- end widget -->
                                                                    <!-- end widget -->
                                                                    <div id="woocommerce_product_categories-2" class="widget sep-hover-control woocommerce widget_product_categories">
                                                                        <h3 class="widget-title">Product Categories</h3>
                                                                        <div class="jt-sep-two"></div>
                                                                        <ul class="product-categories">
                                                                            <li class="cat-item cat-item-46"><a href="#">Clothing</a></li>
                                                                            <li class="cat-item cat-item-17"><a href="#">Occasional Outfit</a></li>
                                                                            <li class="cat-item cat-item-48"><a href="#">Sport Wear</a></li>
                                                                            <li class="cat-item cat-item-45"><a href="#">T-Shirts</a></li>
                                                                            <li class="cat-item cat-item-50"><a href="#">Tight Fits</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <!-- end widget -->
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                            </div>
                            <!-- end main-content -->

                        </div>
                    </div>
                    <!-- End Container -->
                </div>
            </div>
            <!-- End Main Container -->


            <div class="container-fluid padding-zero foot-ctrl">
                <footer class="jt-footer-style-two   ">

                    <div class="container-fluid padding-zero text-widget-holder">
                        <!-- Footer Widgets -->
                        <div class="container padding-zero">
                            <div class="jt-widgets-area jt_widget_count_four">
                                <div class="col-lg-3 col-md-3 col-sm-3 ">
                                    <div class="widget">
                                        <div class="jt-widget-content">
                                            <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                                <h3 class="widget-title">Keep in Touch</h3>
                                                <ul class="jt-widget-address">
                                                    <li class="jt-add-icon jt-add-li">
                                                        <i class="pe-7s-map-2"></i>
                                                        <span>44 New Design Street, Melbourne 005</span>
                                                    </li>
                                                    <li class="jt-add-icon jt-phone-li">
                                                        <i class="pe-7s-call"></i>
                                                        <span>+1 (123) 456-7890-321</span>
                                                    </li>
                                                    <li class="jt-add-icon jt-mail-li">
                                                        <i class="pe-7s-mail"></i>
                                                        <span>info@VictorThemes.com</span>
                                                    </li>
                                                    <li class="jt-add-icon jt-fax-li">
                                                        <i class="pe-7s-call"></i>
                                                        <span>(01) 800 854 633</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- end widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 ">
                                    <div class="widget">
                                        <div class="jt-widget-content">
                                            <div id="text-2" class="widget  widget_text">
                                                <h3 class="widget-title">Links</h3>
                                                <div class="textwidget">
                                                    <ul>
                                                        <li><a href="#">Home</a></li>
                                                        <li><a href="#">About Us</a></li>
                                                        <li><a href="#">Services</a></li>
                                                        <li><a href="#">Portfolio</a></li>
                                                        <li><a href="#">Blog</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- end widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 ">
                                    <div class="widget">
                                        <div class="jt-widget-content">
                                            <div id="text-3" class="widget widget_text">
                                                <h3 class="widget-title">Legal</h3>
                                                <div class="textwidget">
                                                    <ul>
                                                        <li><a href="#">Privacy Policies</a></li>
                                                        <li><a href="#">Terms & Conditions</a></li>
                                                        <li><a href="#">FAQ</a></li>
                                                        <li><a href="#">Careers</a></li>
                                                        <li><a href="#">Contact Us</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- end widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 ">
                                    <div class="widget">
                                        <div class="jt-widget-content">
                                            <div id="null-instagram-feed-4" class="widget null-instagram-feed">
                                                <h3 class="widget-title">Instagram</h3>
                                                <ul class="instagram-pics">
                                                    <li class="">
                                                        <a href="#" target="_self" class=""><img src="upload/12105059_780687482076575_1561240626_n.jpg" alt="Instagram Image" title="Instagram Image" class="" /></a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#" target="_self" class=""><img src="upload/12120443_1505155189809339_1877354409_n.jpg" alt="Instagram Image" title="Instagram Image" class="" /></a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#" target="_self" class=""><img src="upload/12063055_1507908132853521_901973526_n.jpg" alt="Instagram Image" title="Instagram Image" class="" /></a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#" target="_self" class=""><img src="upload/12104948_1672856389597555_835788239_n.jpg" alt="Instagram Image" title="Instagram Image" class="" /></a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#" target="_self" class=""><img src="upload/12081050_1657317981182776_147388951_n.jpg" alt="Instagram Image" title="Instagram Image" class="" /></a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#s" target="_self" class=""><img src="upload/12139811_1701977720034313_760471987_n.jpg" alt="Instagram Image" title="Instagram Image" class="" /></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- end widget -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- Copyright Widget Area -->
                    <div class="jt-copyright-area">
                        <div class="container">
                            <p>
                                <div class="" style="height:5px;"></div>
                            <p></p>
                            <p>© 2015 juster All Rights Reserved.</p>
                            <p>
                                <div class="" style="height:5px;"></div>
                            <p></p>
                        </div>
                    </div>
                    <!-- Footer Copyrights -->
                </footer>
            </div>
        </div>
    <div>
    
    </div>
    </form>
     <script type='text/javascript' src='js/jquery/jquery.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/frontend/add-to-cart.min.js'></script>
    <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
    <script type='text/javascript' src='js/vendors/woocommerce-add-to-cart.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
	<script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

	<script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            // Sticky Navbar
            $(".sticky-nav").sticky({
                topSpacing: 0
            });
        });
    </script>
    <script type='text/javascript' src='js/jquery.form.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
    <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
    <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
    <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

    <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type='text/javascript' src='js/owl.carousel.min.js'></script>
    <script type='text/javascript' src='js/animate-plus.min.js'></script>
    <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/scripts.js'></script>
    <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
    <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.sticky.js'></script>
    <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
    <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.easing.min.js'></script>
    <script type='text/javascript' src='js/dynamic-js.js'></script>
    <script type='text/javascript' src='js/js_composer_front.js'></script>
    <script type='text/javascript' src='js/wow.min.js'></script>
    <script type='text/javascript' src='js/dragcarousel/elastiStack.js'></script>
    <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
    <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
    <script type='text/javascript' src='js/tiltfx.js'></script>
    <script type='text/javascript' src='js/ellipsis.js'></script>
    <script type='text/javascript' src='js/chaffle.js'></script>
    <script type='text/javascript' src='js/contact-form.js'></script>
</body>
</html>
