﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bevtec-Thank-youaspx.aspx.cs" Inherits="Bevtec_Thank_youaspx" %>

<!DOCTYPE html>

<head runat="server">
    <meta charset="UTF-8">

   <link rel="shortcut icon" href="images/bevtec-images/Bevtec-Logo-150.png" />
    <title>Bevtec | Thank you</title>

    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />

    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->

</head>
<body class="page page-id-2023 page-template page-template-template-blank-page page-template-template-blank-page-php  jt_main_content wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
    <form id="form1" runat="server">
        <div>
            <div class="wrapper">
                <header class=" sticky-nav sticky-rev">
                    <nav class="navbar navbar-default navbar-static-top">
                        <div class="">
                            <div class="navbar-header">
                                <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                    <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                </a>
                                <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                    <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                </a>
                            </div>
                            <div class="menu-metas navbar-default navbar-right">
                                <ul class="navbar-nav">
                                    <%--<li id="top-search" class="jt-menu-search">
                                        <a href="#" id="top-search-trigger">
                                            <i class="fa fa-search"></i>
                                            <i class="pe-7s-close"></i>
                                        </a>
                                        <form class="container search-new" method="get" id="searchform" action="#">
                                            <input type="text" name="s" class="form-control" placeholder="Type &amp; Hit Enter..">
                                        </form>
                                    </li>--%>
                                </ul>
                            </div>
                            <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                                    <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                    </li>
                                    <li class="menu-item ">
                                        <a href="BevTec-AboutUs.aspx">About Us</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="BevTec-Categories.aspx">Categories</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="BevTec-Products.aspx">Products</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="Bevtec-Blog.aspx">Blog</a>
                                    </li>

                                    <li class="menu-item ">
                                        <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="Bevtec-Cart.aspx">
                                            <img src="images/icons/shop-cart.png" alt=" " />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <!-- Slim Menu -->
                    <div class="hidden-big-screen ">
                        <div class=" sticky-nav sticky-rev jt-slim-top">
                            <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                <img src="upload/logo-dark.png" alt="Juster" />
                            </a>
                            <div class="menu-main-menu-container">
                                <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                                    <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                    </li>
                                    <li class="menu-item ">
                                        <a href="BevTec-AboutUs.aspx">About Us</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="BevTec-Products.aspx">Products</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="Bevtec-Blog.aspx">Blog</a>
                                    </li>

                                    <li class="menu-item ">
                                        <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                    </li>
                                    <li class="menu-item ">
                                        <a href="Bevtec-Cart.aspx">
                                            <img src="images/icons/shop-cart.png" alt=" " />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Slim Menu -->
                </header>
                <!-- Wrapper -->
                <div class="container padding-zero jt_content_holder">
                    <div class="entry-content page-container content-ctrl">
                        <!-- Main Container -->
                        <div class="container">
                            <!-- Container -->
                            <div class="container main-content-center">
                                <div class="container-fluid">

                                    <article id="post-2023" class="post-2023 page type-page status-publish hentry">
                                        <div class="entry-content">
                                            <div class="vc_row jt_row_class wpb_row vc_row-fluid column-have-space">
                                                <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                                    <div>
                                                        <div class="space-fix " style="">
                                                            <div class="wpb_wrapper">

                                                                <div class="wpb_single_image wpb_content_element vc_align_center">

                                                                    <figure class="wpb_wrapper vc_figure">
                                                                        <a href="index.html" target="_self" class="vc_single_image-wrapper   vc_box_border_grey">
                                                                            <img width="60" height="60" src="upload/blank-logo.png" class="vc_single_image-img attachment-full" alt="blank-logo" /></a>
                                                                    </figure>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1445418444626 column-have-space">
                                                <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                                    <div>
                                                        <div class="space-fix  text-center" style="">
                                                            <div class="wpb_wrapper">
                                                                <img src="images/Bevtec-Logo-17.png" />
                                                                <div class="jt-heading jt-freelancer-contact-section" style="">
                                                                    <h1 class="jt-main-head" style="color: #ffffff; font-size: 26px; text-transform: uppercase; font-weight: 500;">Blank Page Template</h1>
                                                                    <div class="jt-sep"></div>
                                                                </div>
                                                                <div class="vc_empty_space" style="height: 15px"><span class="vc_empty_space_inner"></span></div>

                                                                <div class="wpb_text_column wpb_content_element ">
                                                                    <div class="wpb_wrapper">
                                                                        <p>
                                                                            <span style="font-size: 15px; line-height: 25px; letter-spacing: 1px; color: black;">Thank you for your order! You will receive an email from us confirming your order shortly.</span>
                                                                        </p>

                                                                    </div>
                                                                </div>
                                                                <a href="BevTec-Products.aspx" class="btn-primary btn-black btn-large bg-empty btn-white-hover" style="color: black; font-size: 11px; letter-spacing: 2.5px; border-color: black; margin-bottom: 0px;">Back to Products</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>



                                </div>


                            </div>
                            <script type='text/javascript' src='js/jquery/jquery.js'></script>
                            <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
                            <script type='text/javascript' src='js/frontend/add-to-cart.min.js'></script>
                            <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
                            <script type='text/javascript' src='js/vendors/woocommerce-add-to-cart.js'></script>
                            <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
                            <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

                            <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
                            <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    "use strict";
                                    // Sticky Navbar
                                    $(".sticky-nav").sticky({
                                        topSpacing: 0
                                    });
                                });
                            </script>
                            <script type='text/javascript' src='js/jquery.form.min.js'></script>
                            <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
                            <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
                            <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
                            <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
                            <script type='text/javascript' src='js/woocompare.js'></script>
                            <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
                            <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

                            <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
                            <script type='text/javascript' src='js/bootstrap.min.js'></script>
                            <script type='text/javascript' src='js/owl.carousel.min.js'></script>
                            <script type='text/javascript' src='js/animate-plus.min.js'></script>
                            <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
                            <script type='text/javascript' src='js/scripts.js'></script>
                            <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
                            <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
                            <script type='text/javascript' src='js/jquery.sticky.js'></script>
                            <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
                            <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
                            <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
                            <script type='text/javascript' src='js/slimmenu/jquery.easing.min.js'></script>
                            <script type='text/javascript' src='js/dynamic-js.js'></script>
                            <script type='text/javascript' src='js/js_composer_front.js'></script>
                            <script type='text/javascript' src='js/wow.min.js'></script>
                            <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
                            <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
                            <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
                            <script type='text/javascript' src='js/tiltfx.js'></script>
                            <script type='text/javascript' src='js/ellipsis.js'></script>
                            <script type='text/javascript' src='js/chaffle.js'></script>
                            <script type='text/javascript' src='js/contact-form.js'></script>

                            <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false'></script>
                            <script type='text/javascript' src='js/custom-gmap.js'></script>
    </form>
    <div class="container-fluid padding-zero foot-ctrl">
    </div>
</body>
</html>
