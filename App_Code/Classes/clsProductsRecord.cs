
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProducts
/// </summary>
public class clsProducts
{
    #region MEMBER VARIABLES

    private int m_iProductID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private String m_strStockCode;
    private String m_strTitle;
    private String m_strTagLine;
    private String m_strDescription;
    private double m_dblPrice;
    private String m_strPathToImages;
    private String m_strMasterImage;
    private String m_strMasterDocument;
    private String m_strPathToDocument;
    private int m_iSizeID;
    private int m_iColourID;
    private String m_strStyle;
    private String m_strWeight;
    private String m_strContents;
    private String m_strWarranty;
    private String m_strVideoLink;
    private bool m_bIsSoldOut;
    private bool m_bDoNotDisplay;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iProductID
    {
        get
        {
            return m_iProductID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public String strStockCode
    {
        get
        {
            return m_strStockCode;
        }
        set
        {
            m_strStockCode = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strTagLine
    {
        get
        {
            return m_strTagLine;
        }
        set
        {
            m_strTagLine = value;
        }
    }

    public String strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public String strPathToImages
    {
        get
        {
            return m_strPathToImages;
        }
        set
        {
            m_strPathToImages = value;
        }
    }

    public String strMasterImage
    {
        get
        {
            return m_strMasterImage;
        }
        set
        {
            m_strMasterImage = value;
        }
    }
    public String strMasterDocument
    {
        get
        {
            return m_strMasterDocument;
        }
        set
        {
            m_strMasterDocument = value;
        }
    }
    public String strPathToDocument
    {
        get
        {
            return m_strPathToDocument;
        }
        set
        {
            m_strPathToDocument = value;
        }
    }

    public int iSizeID
    {
        get
        {
            return m_iSizeID;
        }
        set
        {
            m_iSizeID = value;
        }
    }

    public int iColourID
    {
        get
        {
            return m_iColourID;
        }
        set
        {
            m_iColourID = value;
        }
    }

    public String strStyle
    {
        get
        {
            return m_strStyle;
        }
        set
        {
            m_strStyle = value;
        }
    }

    public String strWeight
    {
        get
        {
            return m_strWeight;
        }
        set
        {
            m_strWeight = value;
        }
    }

    public String strContents
    {
        get
        {
            return m_strContents;
        }
        set
        {
            m_strContents = value;
        }
    }

    public String strWarranty
    {
        get
        {
            return m_strWarranty;
        }
        set
        {
            m_strWarranty = value;
        }
    }

    public String strVideoLink
    {
        get
        {
            return m_strVideoLink;
        }
        set
        {
            m_strVideoLink = value;
        }
    }

    public bool bIsSoldOut
    {
        get
        {
            return m_bIsSoldOut;
        }
        set
        {
            m_bIsSoldOut = value;
        }
    }

    public bool bDoNotDisplay
    {
        get
        {
            return m_bDoNotDisplay;
        }
        set
        {
            m_bDoNotDisplay = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsProducts()
    {
        m_iProductID = 0;
    }

    public clsProducts(int iProductID)
    {
        m_iProductID = iProductID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iProductID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strStockCode", m_strStockCode),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strTagLine", m_strTagLine),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage),
                        new SqlParameter("@strMasterDocument", m_strMasterDocument),
                        new SqlParameter("@strPathToDocument", m_strPathToDocument),
                        new SqlParameter("@iSizeID", m_iSizeID),
                        new SqlParameter("@iColourID", m_iColourID),
                        new SqlParameter("@strStyle", m_strStyle),
                        new SqlParameter("@strWeight", m_strWeight),
                        new SqlParameter("@strContents", m_strContents),
                        new SqlParameter("@strWarranty", m_strWarranty),
                        new SqlParameter("@strVideoLink", m_strVideoLink),
                        new SqlParameter("@bIsSoldOut", m_bIsSoldOut),
                        new SqlParameter("@bDoNotDisplay", m_bDoNotDisplay)                  
                  };

                //### Add
                m_iProductID = (int)clsDataAccess.ExecuteScalar("spProductsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iProductID", m_iProductID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strStockCode", m_strStockCode),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strTagLine", m_strTagLine),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                        new  SqlParameter("@strMasterDocument", m_strMasterDocument),
                        new SqlParameter("@strPathToDocument", m_strPathToDocument),
                         new SqlParameter("@iSizeID", m_iSizeID),
                         new SqlParameter("@iColourID", m_iColourID),
                         new SqlParameter("@strStyle", m_strStyle),
                         new SqlParameter("@strWeight", m_strWeight),
                         new SqlParameter("@strContents", m_strContents),
                         new SqlParameter("@strWarranty", m_strWarranty),
                         new SqlParameter("@strVideoLink", m_strVideoLink),
                         new SqlParameter("@bIsSoldOut", m_bIsSoldOut),
                         new SqlParameter("@bDoNotDisplay", m_bDoNotDisplay)
                    };
                //### Update
                clsDataAccess.Execute("spProductsUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iProductID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@iProductID", iProductID)
            };
        //### Executes delete sp
        clsDataAccess.Execute("spProductsDelete", sqlParameter);
    }
    //public static DataTable searchProduct(String strTitle)
    //{
    //    SqlParameter[] sqlParameter = new SqlParameter[]
    //        {
    //            new SqlParameter("@strTitle", strTitle)
    //        };
    //    DataView dvProductsList = new DataView();

    //    SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
    //    dvProductsList = clsDataAccess.GetDataView("spProductsListSearcheTest", sqlParameter);

    //    return dvProductsList.ToTable();
    //    //### Populate
    //}

    public static DataTable GetProductsList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spProductsList", EmptySqlParameter);
    }

    public static DataTable GetProductsList(string strFilterExpression, string strSortExpression)
    {
        DataView dvProductsList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvProductsList = clsDataAccess.GetDataView("spProductsList", EmptySqlParameter);
        dvProductsList.RowFilter = strFilterExpression;
        dvProductsList.Sort = strSortExpression;

        return dvProductsList.ToTable();
    }

    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iProductID", m_iProductID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spProductsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_strStockCode = drRecord["strStockCode"].ToString();
            m_strTitle = drRecord["strTitle"].ToString();
            m_strTagLine = drRecord["strTagLine"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();
            m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
            m_strPathToImages = drRecord["strPathToImages"].ToString();
            m_strMasterImage = drRecord["strMasterImage"].ToString();
            m_strMasterDocument = drRecord["strMasterDocument"].ToString();
            m_strPathToDocument = drRecord["strPathToDocument"].ToString();
            m_iSizeID = Convert.ToInt32(drRecord["iSizeID"]);
            m_iColourID = Convert.ToInt32(drRecord["iColourID"]);
            m_strStyle = drRecord["strStyle"].ToString();
            m_strWeight = drRecord["strWeight"].ToString();
            m_strContents = drRecord["strContents"].ToString();
            m_strWarranty = drRecord["strWarranty"].ToString();
            m_strVideoLink = drRecord["strVideoLink"].ToString();
            m_bIsSoldOut = Convert.ToBoolean(drRecord["bIsSoldOut"]);
            m_bDoNotDisplay = Convert.ToBoolean(drRecord["bDoNotDisplay"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion
}