
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCustomerTestimonials
/// </summary>
public class clsCustomerTestimonials
{
    #region MEMBER VARIABLES

        private int m_iCustomerTestimonialID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strCustomerName;
        private String m_strJobdesc;
        private String m_strTestimonial;
        private bool m_bIsApproved;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iCustomerTestimonialID
        {
            get
            {
                return m_iCustomerTestimonialID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strCustomerName
        {
            get
            {
                return m_strCustomerName;
            }
            set
            {
                m_strCustomerName = value;
            }
        }
        public String strJobdesc
        {
            get
            {
                return m_strJobdesc;
            }
            set
            {
                m_strJobdesc = value;
            }
        }

        public String strTestimonial
        {
            get
            {
                return m_strTestimonial;
            }
            set
            {
                m_strTestimonial = value;
            }
        }

        public bool bIsApproved
        {
            get
            {
                return m_bIsApproved;
            }
            set
            {
                m_bIsApproved = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCustomerTestimonials()
    {
        m_iCustomerTestimonialID = 0;
    }

    public clsCustomerTestimonials(int iCustomerTestimonialID)
    {
        m_iCustomerTestimonialID = iCustomerTestimonialID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCustomerTestimonialID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strCustomerName", m_strCustomerName),
                        new SqlParameter("@strJobdesc", m_strJobdesc),
                        new SqlParameter("@strTestimonial", m_strTestimonial),
                        new SqlParameter("@bIsApproved", m_bIsApproved)                  
                  };

                  //### Add
                  m_iCustomerTestimonialID = (int)clsDataAccess.ExecuteScalar("spCustomerTestimonialsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCustomerTestimonialID", m_iCustomerTestimonialID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strCustomerName", m_strCustomerName),
                         new SqlParameter("@strJobdesc", m_strJobdesc),
                         new SqlParameter("@strTestimonial", m_strTestimonial),
                         new SqlParameter("@bIsApproved", m_bIsApproved)
                    };
                    //### Update
                    clsDataAccess.Execute("spCustomerTestimonialsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCustomerTestimonialID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCustomerTestimonialID", iCustomerTestimonialID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCustomerTestimonialsDelete", sqlParameter);
        }

        public static DataTable GetCustomerTestimonialsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCustomerTestimonialsList", EmptySqlParameter);
        }
        public static DataTable GetCustomerTestimonialsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCustomerTestimonialsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCustomerTestimonialsList = clsDataAccess.GetDataView("spCustomerTestimonialsList", EmptySqlParameter);
            dvCustomerTestimonialsList.RowFilter = strFilterExpression;
            dvCustomerTestimonialsList.Sort = strSortExpression;

            return dvCustomerTestimonialsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCustomerTestimonialID", m_iCustomerTestimonialID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCustomerTestimonialsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strCustomerName = drRecord["strCustomerName"].ToString();
                m_strJobdesc = drRecord["strJobdesc"].ToString();
                m_strTestimonial = drRecord["strTestimonial"].ToString();
                m_bIsApproved = Convert.ToBoolean(drRecord["bIsApproved"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}