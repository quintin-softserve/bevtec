
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAccountHolders
/// </summary>
public class clsAccountHolders
{
    #region MEMBER VARIABLES

        private int m_iAccountHolderID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iTitleID;
        private String m_strFirstName;
        private String m_strSurname;
        private String m_strEmailAddress;
        private String m_strPassword;
        private String m_strTelephoneNumber;
        private int m_iOrderCount;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iAccountHolderID
        {
            get
            {
                return m_iAccountHolderID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iTitleID
        {
            get
            {
                return m_iTitleID;
            }
            set
            {
                m_iTitleID = value;
            }
        }

        public String strFirstName
        {
            get
            {
                return m_strFirstName;
            }
            set
            {
                m_strFirstName = value;
            }
        }

        public String strSurname
        {
            get
            {
                return m_strSurname;
            }
            set
            {
                m_strSurname = value;
            }
        }

        public String strEmailAddress
        {
            get
            {
                return m_strEmailAddress;
            }
            set
            {
                m_strEmailAddress = value;
            }
        }

        public String strPassword
        {
            get
            {
                return m_strPassword;
            }
            set
            {
                m_strPassword = value;
            }
        }

        public String strTelephoneNumber
        {
            get
            {
                return m_strTelephoneNumber;
            }
            set
            {
                m_strTelephoneNumber = value;
            }
        }

        public int iOrderCount
        {
            get
            {
                return m_iOrderCount;
            }
            set
            {
                m_iOrderCount = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsAccountHolders()
    {
        m_iAccountHolderID = 0;
    }

    public clsAccountHolders(int iAccountHolderID)
    {
        m_iAccountHolderID = iAccountHolderID;
        GetData();
    }

    public clsAccountHolders(string strUsername, string strPassword)
    {

        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
                { 
                    new SqlParameter("@strEmail", strUsername), 
                    new SqlParameter("@strPassword", strPassword) 
                };

        DataRow drLoginRecord = clsDataAccess.GetRecord("spInitialiseAccountUser", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_iAccountHolderID = Convert.ToInt32(drLoginRecord["iAccountHolderID"]);

            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString();
            m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();

            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iAccountHolderID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iTitleID", m_iTitleID),
                        new SqlParameter("@strFirstName", m_strFirstName),
                        new SqlParameter("@strSurname", m_strSurname),
                        new SqlParameter("@strEmailAddress", m_strEmailAddress),
                        new SqlParameter("@strPassword", m_strPassword),
                        new SqlParameter("@strTelephoneNumber", m_strTelephoneNumber),
                        new SqlParameter("@iOrderCount", m_iOrderCount)                  
                  };

                  //### Add
                  m_iAccountHolderID = (int)clsDataAccess.ExecuteScalar("spAccountHoldersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iAccountHolderID", m_iAccountHolderID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iTitleID", m_iTitleID),
                         new SqlParameter("@strFirstName", m_strFirstName),
                         new SqlParameter("@strSurname", m_strSurname),
                         new SqlParameter("@strEmailAddress", m_strEmailAddress),
                         new SqlParameter("@strPassword", m_strPassword),
                         new SqlParameter("@strTelephoneNumber", m_strTelephoneNumber),
                         new SqlParameter("@iOrderCount", m_iOrderCount)
                    };
                    //### Update
                    clsDataAccess.Execute("spAccountHoldersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iAccountHolderID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAccountHolderID", iAccountHolderID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spAccountHoldersDelete", sqlParameter);
        }

        public static DataTable GetAccountHoldersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spAccountHoldersList", EmptySqlParameter);
        }
        public static DataTable GetAccountHoldersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvAccountHoldersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvAccountHoldersList = clsDataAccess.GetDataView("spAccountHoldersList", EmptySqlParameter);
            dvAccountHoldersList.RowFilter = strFilterExpression;
            dvAccountHoldersList.Sort = strSortExpression;

            return dvAccountHoldersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iAccountHolderID", m_iAccountHolderID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spAccountHoldersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iTitleID = Convert.ToInt32(drRecord["iTitleID"]);
                m_strFirstName = drRecord["strFirstName"].ToString();
                m_strSurname = drRecord["strSurname"].ToString();
                m_strEmailAddress = drRecord["strEmailAddress"].ToString();
                m_strPassword = drRecord["strPassword"].ToString();
                m_strTelephoneNumber = drRecord["strTelephoneNumber"].ToString();
                m_iOrderCount = Convert.ToInt32(drRecord["iOrderCount"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}