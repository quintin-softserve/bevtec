
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsOrderItems
/// </summary>
public class clsOrderItems
{
    #region MEMBER VARIABLES

        private int m_iOrderItemID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iOrderID;
        private string m_strProductTitle;
        private int m_iProductID;
        private int m_iQuantity;
        private string m_strColour;
        private string m_strSize;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iOrderItemID
        {
            get
            {
                return m_iOrderItemID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iOrderID
        {
            get
            {
                return m_iOrderID;
            }
            set
            {
                m_iOrderID = value;
            }
        }

        public int iProductID
        {
            get
            {
                return m_iProductID;
            }
            set
            {
                m_iProductID = value;
            }
        }

        public int iQuantity
        {
            get
            {
                return m_iQuantity;
            }
            set
            {
                m_iQuantity = value;
            }
        }

        public string strColour
        {
            get
            {
                return m_strColour;
            }
            set
            {
                m_strColour = value;
            }
        }
        public string strProductTitle
        {
            get
            {
                return m_strProductTitle;
            }
            set
            {
                m_strProductTitle = value;
            }
        }

        public string strSize
        {
            get
            {
                return m_strSize;
            }
            set
            {
                m_strSize = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsOrderItems()
    {
        m_iOrderItemID = 0;
    }

    public clsOrderItems(int iOrderItemID)
    {
        m_iOrderItemID = iOrderItemID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iOrderItemID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@dtEdited", m_dtEdited),
                        new SqlParameter("@iEditedBy", m_iEditedBy),
                        new SqlParameter("@iOrderID", m_iOrderID),
                        new SqlParameter("@iProductID", m_iProductID),
                        new SqlParameter("@iQuantity", m_iQuantity),
                        new SqlParameter("@strColour", m_strColour),
                        new SqlParameter("@strSize", m_strSize),
                         new SqlParameter("@strProductTitle", m_strProductTitle)

                        
                  };

                  //### Add
                  m_iOrderItemID = (int)clsDataAccess.ExecuteScalar("spOrderItemsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iOrderItemID", m_iOrderItemID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iOrderID", m_iOrderID),
                         new SqlParameter("@iProductID", m_iProductID),
                         new SqlParameter("@strProductTitle", m_strProductTitle),
                         new SqlParameter("@iQuantity", m_iQuantity),
                         new SqlParameter("@strColour", m_strColour),
                         new SqlParameter("@strSize", m_strSize)
                    };
                    //### Update
                    clsDataAccess.Execute("spOrderItemsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iOrderItemID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iOrderItemID", iOrderItemID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spOrderItemsDelete", sqlParameter);
        }

        public static DataTable GetOrderItemsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spOrderItemsList", EmptySqlParameter);
        }
        public static DataTable GetOrderItemsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvOrderItemsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvOrderItemsList = clsDataAccess.GetDataView("spOrderItemsList", EmptySqlParameter);
            dvOrderItemsList.RowFilter = strFilterExpression;
            dvOrderItemsList.Sort = strSortExpression;

            return dvOrderItemsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iOrderItemID", m_iOrderItemID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spOrderItemsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);
                m_iOrderID = Convert.ToInt32(drRecord["iOrderID"]);
                m_iProductID = Convert.ToInt32(drRecord["iProductID"]);
                m_strProductTitle = drRecord["strProductTitle"].ToString();
                m_iQuantity = Convert.ToInt32(drRecord["iQuantity"]);
                m_strColour = drRecord["strColour"].ToString();
                m_strSize = drRecord["strSize"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}