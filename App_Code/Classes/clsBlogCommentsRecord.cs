
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsBlogComments
/// </summary>
public class clsBlogComments
{
    #region MEMBER VARIABLES

     private     int m_iBlogCommentID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strName;
     private     string m_strEmailAddress;
     private     string m_strPhoneNumber;
     private     string m_strMessage;
     private     bool m_bDoNotDisplay;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iBlogCommentID
    {
        get
        {
            return m_iBlogCommentID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strName
    {
        get
        {
            return m_strName;
        }
        set
        {
            m_strName = value;
        }
    }

    public    string strEmailAddress
    {
        get
        {
            return m_strEmailAddress;
        }
        set
        {
            m_strEmailAddress = value;
        }
    }

    public    string strPhoneNumber
    {
        get
        {
            return m_strPhoneNumber;
        }
        set
        {
            m_strPhoneNumber = value;
        }
    }

    public    string strMessage
    {
        get
        {
            return m_strMessage;
        }
        set
        {
            m_strMessage = value;
        }
    }

    public    bool bDoNotDisplay
    {
        get
        {
            return m_bDoNotDisplay;
        }
        set
        {
            m_bDoNotDisplay = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsBlogComments()
    {
        m_iBlogCommentID = 0;
    }

    public clsBlogComments(int iBlogCommentID)
    {
        m_iBlogCommentID = iBlogCommentID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iBlogCommentID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strName", m_strName),
                        new SqlParameter("@strEmailAddress", m_strEmailAddress),
                        new SqlParameter("@strPhoneNumber", m_strPhoneNumber),
                        new SqlParameter("@strMessage", m_strMessage),
                        new SqlParameter("@bDoNotDisplay", m_bDoNotDisplay)                  
                  };

                  //### Add
                  m_iBlogCommentID = (int)clsDataAccess.ExecuteScalar("spBlogCommentsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iBlogCommentID", m_iBlogCommentID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strName", m_strName),
                         new SqlParameter("@strEmailAddress", m_strEmailAddress),
                         new SqlParameter("@strPhoneNumber", m_strPhoneNumber),
                         new SqlParameter("@strMessage", m_strMessage),
                         new SqlParameter("@bDoNotDisplay", m_bDoNotDisplay)
          };
          //### Update
          clsDataAccess.Execute("spBlogCommentsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iBlogCommentID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iBlogCommentID", iBlogCommentID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spBlogCommentsDelete", sqlParameter);
        }

        public static DataTable GetBlogCommentsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spBlogCommentsList", EmptySqlParameter);
        }
        public static DataTable GetBlogCommentsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvBlogCommentsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvBlogCommentsList = clsDataAccess.GetDataView("spBlogCommentsList", EmptySqlParameter);
            dvBlogCommentsList.RowFilter = strFilterExpression;
            dvBlogCommentsList.Sort = strSortExpression;

            return dvBlogCommentsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iBlogCommentID", m_iBlogCommentID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spBlogCommentsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strName = drRecord["strName"].ToString();
                   m_strEmailAddress = drRecord["strEmailAddress"].ToString();
                   m_strPhoneNumber = drRecord["strPhoneNumber"].ToString();
                   m_strMessage = drRecord["strMessage"].ToString();
                   m_bDoNotDisplay = Convert.ToBoolean(drRecord["bDoNotDisplay"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}