using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProductColoursLink
/// </summary>
public class clsProductSizesLink
{
    #region MEMBER VARIABLES

        private int m_iProductSizesLinkID;
        private int m_iSizeID;
        private int m_iProductID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iProductSizesLinkID
        {
            get
            {
                return m_iProductSizesLinkID;
            }
        }

        public int iSizeID
        {
            get
            {
                return m_iSizeID;
            }
            set
            {
                m_iSizeID = value;
            }
        }

        public int iProductID
        {
            get
            {
                return m_iProductID;
            }
            set
            {
                m_iProductID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }


    
    #endregion
    
    #region CONSTRUCTORS

    public clsProductSizesLink()
    {
        m_iProductSizesLinkID = 0;
    }

    public clsProductSizesLink(int iProductSizesLinkID)
    {
        m_iProductSizesLinkID = iProductSizesLinkID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iProductSizesLinkID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@iSizeID", m_iSizeID),
                        new SqlParameter("@iProductID", m_iProductID)                  
                  };

                  //### Add
                  m_iProductSizesLinkID = (int)clsDataAccess.ExecuteScalar("spProductSizesLinkInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iProductSizesLinkID", m_iProductSizesLinkID),
                         new SqlParameter("@iSizeID", m_iSizeID),
                         new SqlParameter("@iProductID", m_iProductID)
                    };
                    //### Update
                    clsDataAccess.Execute("spProductSizesLinkUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iProductSizesLinkID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProductSizesLinkID", iProductSizesLinkID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spProductSizesLinkDelete", sqlParameter);
        }

        public static void Clear(int iProductID)
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProductID", iProductID)
        };
            //### Executes delete sp
            clsDataAccess.Execute("spProductSizesLinkClear", sqlParameter);
        }

        public static DataTable GetProductSizesLinkList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spProductSizesLinkList", EmptySqlParameter);
        }

        public static DataTable GetProductSizesLinkList(string strFilterExpression, string strSortExpression)
        {
            DataView dvProductColoursLinkList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvProductColoursLinkList = clsDataAccess.GetDataView("spProductSizesLinkList", EmptySqlParameter);
            dvProductColoursLinkList.RowFilter = strFilterExpression;
            dvProductColoursLinkList.Sort = strSortExpression;
            return dvProductColoursLinkList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iProductSizesLinkID", m_iProductSizesLinkID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spProductSizesLinkGetRecord", sqlParameter);

                m_iSizeID = Convert.ToInt32(drRecord["iSizeID"]);
                m_iProductID = Convert.ToInt32(drRecord["iProductID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}