﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for clsValidation
/// </summary>
public class clsValidation
{
    #region CONTRUCTOR

    private clsValidation()
    {
    }

    #endregion

    #region NULL CHECK METHODS

    /// <summary>
    /// Added by J.Pefier
    /// Validates the requred textbox, setting the css class if no content is given
    /// </summary>
    /// <param name="txtValidate">The text box that must contain a value</param>
    /// <param name="regValidate">The element to set styles if the text box is not valid</param>
    /// <returns></returns>
    public static bool IsNullOrEmpty(TextBox txtValidate, bool bCurrentState)
    {
        if (!String.IsNullOrEmpty(txtValidate.Text))
        {
            SetValid(txtValidate);                
            return bCurrentState;
        }
        else
        {
            SetInvalid(txtValidate);
            txtValidate.Attributes.Remove("style");
            txtValidate.Attributes.Add("style", "border:1px solid #401414; font-family: PT Sans, sans-serif;");
            return false;
        }
    }

    /// <summary>
    /// Added by J.Pefier
    /// Validates the requred drop down list, setting the css class if the value is 0 or "--Not Selected--"
    /// </summary>
    /// <param name="lstValidate">The list box that must comtain a value</param>
    /// <param name="regValidate">The element to set styles if the text box is not valid</param>
    /// <returns></returns>
    //public static bool IsNullOrEmpty(DropDownList lstValidate, HtmlGenericControl regValidate, bool bCurrentState)
    //{
    //    if ((lstValidate.SelectedValue == "--Not Selected--") || (lstValidate.SelectedValue == "0"))
    //    {
    //        SetInvalid(lstValidate, regValidate);
    //        return false;
    //    }
    //    else
    //    {
    //        SetValid(lstValidate, regValidate);
    //        return bCurrentState;
    //    }
    //}

    /// <summary>
    /// Added by A Papoutsis
    /// Validates the requred drop down list, setting the css class if the value is 0 or "--Not Selected--"
    /// </summary>
    /// <param name="lstValidate">The list box that must comtain a value</param>
    /// <param name="regValidate">The element to set styles if the text box is not valid</param>
    /// <returns></returns>
    public static bool IsNullOrEmpty(DropDownList lstValidate, bool bCurrentState)
    {
        if ((lstValidate.SelectedValue == "--Not Selected--") || (lstValidate.SelectedValue == "0"))
        {
            SetInvalid(lstValidate);
            lstValidate.Attributes.Add("style", "border:1px solid #401414");
            return false;
        }
        else
        {
            SetValid(lstValidate);
            lstValidate.Attributes.Remove("style");
            lstValidate.Attributes.Add("style", "border-top:2px solid #ccc");
            lstValidate.Attributes.Add("style", "border-left:2px solid #ccc");
            lstValidate.Attributes.Add("style", "border-right:1px solid #ccc");
            lstValidate.Attributes.Add("style", "border-bottom:1px solid #ccc");
            return bCurrentState;
        }
    }

    /// <summary>
    /// Added by A Papoutsis
    /// Validates the requred drop down list, setting the css class if the value is 0 or "--Not Selected--"
    /// </summary>
    /// <param name="lstValidate">The list box that must comtain a value</param>
    /// <param name="regValidate">The element to set styles if the text box is not valid</param>
    /// <returns></returns>
    public static void IsNullOrEmpty(DropDownList lstValidate)
    {
        if ((lstValidate.SelectedValue == "--Not Selected--") || (lstValidate.SelectedValue == "0"))
        {
            SetInvalid(lstValidate);
            lstValidate.Attributes.Add("style", "border:1px solid #401414");
        }
        else
        {
            SetValid(lstValidate);
            lstValidate.Attributes.Remove("style");
            lstValidate.Attributes.Add("style", "border-top:2px solid #ccc");
            lstValidate.Attributes.Add("style", "border-left:2px solid #ccc");
            lstValidate.Attributes.Add("style", "border-right:1px solid #ccc");
            lstValidate.Attributes.Add("style", "border-bottom:1px solid #ccc");
        }
    }

    /// <summary>
    /// Validates the requred textbox, setting the styles if no content is given
    /// </summary>
    /// <param name="lstValidate">The list box that must comtain a value</param>
    /// <param name="regValidate">The element to set styles if the text box is not valid</param>
    /// <returns></returns>
    public static bool IsNullOrEmpty(RadioButtonList radRadioButtonList, HtmlGenericControl regValidate, bool bCurrentState)
    {
        if (radRadioButtonList.SelectedIndex == -1)
        {
            //### New implementation to Follow
            regValidate.Attributes.Add("style", "display:block");
            return false;
        }
        else
        {
            regValidate.Attributes.Add("style", "display:none");
            return bCurrentState;
        }
    }

    /// <summary>
    /// Validates the requred textbox, setting the styles if no content is given
    /// </summary>
    /// <param name="txtValidate">The text box that must comtain a value</param>
    /// <param name="regValidate">The element to set styles if the text box is not valid</param>
    /// <returns></returns>
    //public static bool IsNullOrEmpty(TextBox txtValidate, HtmlGenericControl regValidate)
    //{
    //    if (String.IsNullOrEmpty(txtValidate.Text))
    //    {
    //        SetInvalid(txtValidate, regValidate);
    //        return false;
    //    }
    //    else
    //    {
    //        SetValid(txtValidate, regValidate);
    //        return true;
    //    }
    //}

    /// <summary>
    /// Validates the requred textbox, setting the styles if no content is given. Overload for mass validations. Added by I.Erasmus.
    /// </summary>
    /// <param name="txtValidate">The text box that must comtain a value</param>
    /// <returns></returns>
    [Obsolete("Use IsNullOrEmpty(TextBox txtValidate, HtmlGenericControl regValidate, bool bCurrentState")]
    public static bool IsNullOrEmpty(TextBox txtValidate)
    {
        if (String.IsNullOrEmpty(txtValidate.Text))
        {
            txtValidate.Attributes.Add("style", "border:1px solid #401414; font-family: PT Sans, sans-serif;");
            return false;
        }
        else
        {
            txtValidate.Attributes.Remove("style");
            txtValidate.Attributes.Add("style", "border-top:2px solid #ccc; font-family: PT Sans, sans-serif;");
            txtValidate.Attributes.Add("style", "border-left:2px solid #ccc; font-family: PT Sans, sans-serif;");
            txtValidate.Attributes.Add("style", "border-right:1px solid #ccc; font-family: PT Sans, sans-serif;");
            txtValidate.Attributes.Add("style", "border-bottom:1px solid #ccc; font-family: PT Sans, sans-serif;");
            return true;
        }
    }

    public static bool IsNullOrEmptyFile(FileUpload txtValidate)
    {
        if (String.IsNullOrEmpty(txtValidate.FileName))
        {
            txtValidate.Attributes.Add("style", "border:1px solid #401414");
            return false;
        }
        else
        {
            txtValidate.Attributes.Remove("style");
            txtValidate.Attributes.Add("style", "border-top:2px solid #ccc");
            txtValidate.Attributes.Add("style", "border-left:2px solid #ccc");
            txtValidate.Attributes.Add("style", "border-right:1px solid #ccc");
            txtValidate.Attributes.Add("style", "border-bottom:1px solid #ccc");
            return true;
        }
    }

    /// <summary>
    /// Validates the requred textbox, setting the styles if no content is given. Overload for mass validations. Specialized for testing guidance content. Added by I.Erasmus.
    /// </summary>
    /// <param name="txtValidate">The text box that must comtain a value</param>
    /// <param name="strIgnoreText">The text to ignore as empty</param>
    /// <returns></returns>
    [Obsolete("Use IsNullOrEmpty(TextBox txtValidate, HtmlGenericControl regValidate, bool bCurrentState")]
    public static bool IsNullOrEmpty(TextBox txtValidate, string strIgnoreText)
    {
        if (String.IsNullOrEmpty(txtValidate.Text) || txtValidate.Text == strIgnoreText)
        {
            txtValidate.Attributes.Add("style", "border:1px solid #401414; font-family: PT Sans, sans-serif;");
            return false;
        }
        else
        {
            txtValidate.Attributes.Remove("style");
            txtValidate.Attributes.Add("style", "border-top:2px solid #ccc");
            txtValidate.Attributes.Add("style", "border-left:2px solid #ccc");
            txtValidate.Attributes.Add("style", "border-right:1px solid #ccc");
            txtValidate.Attributes.Add("style", "border-bottom:1px solid #ccc");
            return true;
        }
    }

    #endregion

    #region VALIDATION METHODS

    /// <summary>
    /// Added by J.Pefier
    /// Sets a control as valid, Removes the invalid styling/css"
    /// </summary>
    /// <param name="lstValidate">The control that is to be set valid</param>
    /// <param name="regValidate">The element hide</param>
    /// <returns></returns>
    public static void SetValid(WebControl webcValidate, HtmlGenericControl regValidate)
    {
        regValidate.Attributes.Add("style", "display:none");
        webcValidate.CssClass = webcValidate.CssClass.Replace("txtInvalid", "").Trim();
        webcValidate.Style.Clear();
    }

    /// <summary>
    /// Added by A Papoutsis
    /// Sets a control as invalid, Adds invalid styling/css"
    /// </summary>
    /// <param name="lstValidate">The control that is to be set invalid</param>
    /// <param name="regValidate">The element that si to be shown</param>
    /// <returns></returns>
    public static void SetInvalid(WebControl webcValidate)
    {
        SetValid(webcValidate);
        //regValidate.Attributes.Add("style", "display:block");
        webcValidate.CssClass += " txtInvalid";
        webcValidate.Attributes.Remove("style");        
    }

    /// <summary>
    /// Added by A Papoutsis
    /// Sets a control as valid, Removes the invalid styling/css"
    /// For use in clear methods or events.
    /// </summary>
    /// <param name="lstValidate">The control that is to be set valid</param>
    /// <returns></returns>
    public static void SetValid(WebControl webcValidate)
    {
        webcValidate.CssClass = webcValidate.CssClass.Replace("txtInvalid", "").Trim();
        webcValidate.Style.Clear();
    }

    #endregion
}