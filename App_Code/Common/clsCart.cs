﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for clsCart
/// </summary>
public class clsCart
{
    int iCartCount = 0;

    //### Cart Class.
    public clsCart()
    {
    }

    public delegate void CartChanged(EventArgs e);

    public static event CartChanged myCartEvent;

    #region CART SETUP METHODS

    //### Static method to create a new order session and a new order entry into tblOrders.
    public static void CartSetup()
    {
        if (HttpContext.Current.Session["BevtecOrder"] != "" && HttpContext.Current.Session["BevtecOrder"] != null)
        {
            clsAccountHolders clsAccountHolders;
            clsOrders clsOrders = new clsOrders();            

            if (HttpContext.Current.Session["clsAccountHolders"] == null)
            {
                // ID = -10 for orders added by users that are once off customers
                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = -10;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BUT PAYMENT BEFORE PAYMENT
               
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2"));
                clsOrders.dblDeliveryCost = 0.00;
                clsOrders.iDeliveryInfoID = 0;
                //clsOrders.iDeliveryInfoID = iDeliveryInfoID;

                clsOrders.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;
            }
            else if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];

                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = clsAccountHolders.iAccountHolderID;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BUT PAYMENT BEFORE PAYMENT
                
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2"));
                clsOrders.dblDeliveryCost = 0.00;
                clsOrders.iDeliveryInfoID = 0;
                //clsOrders.iDeliveryInfoID = clsAccountHolders.iDeliveryInfoID;

                clsOrders.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;
            }
        }
    }

    public static int CartSetup(int iDeliveryInfoID, double dblDeliveryCharge)
    {
        int iCurrentOrderID = 0;

        if ((HttpContext.Current.Session["BevtecOrder"] != "") && (HttpContext.Current.Session["BevtecOrder"] != null))
        {
            clsAccountHolders clsAccountHolders;
            clsOrders clsOrders = new clsOrders();

            if (HttpContext.Current.Session["clsAccountHolders"] == null)
            {
                // ID = -10 for orders added by users that are once off customers
                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = -10;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BEFORE PAYMENT
              
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2") + dblDeliveryCharge);
                clsOrders.dblDeliveryCost = dblDeliveryCharge;
                clsOrders.iDeliveryInfoID = iDeliveryInfoID;

                clsOrders.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;

                iCurrentOrderID = clsOrders.iOrderID;
            }
            else if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];

                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = clsAccountHolders.iAccountHolderID;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BEFORE PAYMENT
               
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2"));
                clsOrders.dblDeliveryCost = dblDeliveryCharge;
                clsOrders.iDeliveryInfoID = iDeliveryInfoID;

                clsOrders.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;

                iCurrentOrderID = clsOrders.iOrderID;
            }
        }
        return iCurrentOrderID;
    }

    #endregion

    #region ADD TO CART METHODS

    //### Static method to add items to the user's cart.
    public static void addToCart(int iProductID, int iQuantity)
    {
        //### Check for an Order in session
        if (HttpContext.Current.Session["iOrderID"] == null)
        {
            if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                //### If there is no order in session and the user is logged in, create a new order entry to tblOrders.
                CartSetup();
            }
        }

        //### Check to see if the user is logged in.
        if (HttpContext.Current.Session["clsAccountHolders"] != null)
        {
            //### If the user is logged in, create the order as normal.
            //addCartItem(iProductID, iQuantity); --->Replacing to be handled by the cookie
            addCartItemCookie(iProductID, iQuantity);
        }
        else
        {
            //### If the user is not logged in, create a logged out order(in session order + cookie order).
            //### The cookie gives the user the ability to register and continue with the order within 7 days.

            addCartItemCookie(iProductID, iQuantity);
        }
    }

    //### Static method to add cart items to tblOrderItems.
    public static void addCartItem(int iProductID, int iQuantity)
    {
        int iOrderID = Convert.ToInt32(HttpContext.Current.Session["iOrderID"]);

        //### Check if the order item exists.
        if (!clsCommonFunctions.DoesRecordExist("tblOrderItems", "iOrderID = " + iOrderID + " AND iProductID = " + iProductID + " AND bIsDeleted=0"))
        {
            //### If the order item exists create a new entry.
            clsOrderItems clsOrderItems = new clsOrderItems();

            clsOrderItems.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            if ((HttpContext.Current.Session["clsAccountHolders"] != null))
            {
                clsAccountHolders clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];
                clsOrderItems.iAddedBy = clsAccountHolders.iAccountHolderID;
            }
            else
                clsOrderItems.iAddedBy = 0;

            clsOrderItems.iOrderID = iOrderID;
            clsOrderItems.iProductID = iProductID;
            clsOrderItems.iQuantity = iQuantity;

            clsOrderItems.Update();
        }
    }

    #endregion

    #region ADD TO CART COOKIE METHODS

    //### Static method to add a cart item to a cookie.
    public static void addCartItemCookie(int iProductID, int iQuantity)
    {
        //### Trap for logged out order in session.
        if (HttpContext.Current.Session["BevtecOrder"] == null)
        {
            //### If there is no order in session check to see if a cookie exists.
            if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"] != null)
            {
                //### If a cookie exists, add the cookie values to the current session.
                if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"] != null)
                {
                    HttpContext.Current.Session["BevtecOrder"] = HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"];
                    //### Loop through this method to add the current item to the existing list of items.
                    addCartItemCookie(iProductID, iQuantity);
                }
                else
                {
                    //### If no cookie exists, create a completely new session.
                    HttpContext.Current.Session["BevtecOrder"] = iProductID + "|" + iQuantity;
                }
            }
            else
            {
                //### If no cookie exists, create a completely new session.
                HttpContext.Current.Session["BevtecOrder"] = iProductID + "|" + iQuantity;
            }
        }
        else
        {
            //### If there is an order in session add a new cart item to that session.
            string strCartValues = "";
            string iProdID = "";
            string iQnty = "";

            //### Trap for empty Session.
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["BevtecOrder"].ToString()))
            {
                strCartValues = HttpContext.Current.Session["BevtecOrder"].ToString();
            }

            //### Trap for multiple cart items (if the string contains a "," then we know there are multiple cart items.)
            if (strCartValues.Contains(","))
            {
                //### Split the cart items
                string[] strCartItems = strCartValues.Split(new char[] { ',' });
                string strCartTemp = strCartValues;
                bool bfound = false;

                foreach (string value in strCartItems)
                {
                    //### Trap for empty cart item values.
                    if (!String.IsNullOrEmpty(value))
                    {
                        //### Splite cart item details (ID, Quantity, Product Type)
                        string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                        iProdID = strCartItemDetails[0];
                        iQnty = strCartItemDetails[1];

                        if (int.Parse(iProdID) == iProductID)
                        {
                            bfound = true;
                            break;
                        }
                    }
                    ////### If the current product(item) being added does not exists in the session, add the new item. iProductID.ToString() != iProdID
                    //if (!strCartTemp.Contains("," + iProductID.ToString() + "|" + iQnty) && !strCartTemp.Contains(iProductID.ToString() + "|" + iQnty))
                    //{
                    //}
                    ////### If the current product(item) being added does exist in the session, update the existing item's quantity.
                    //else
                    //{
                    //    bfound = true;
                    //    break;
                    //}
                }

                if (bfound)
                {
                    strCartValues = strCartValues.Replace(iProductID.ToString() + "|" + iQnty, (iProductID.ToString() + "|" + (Convert.ToInt32(iQnty) + iQuantity)));
                    //strCartValues += "," + iProductID.ToString() + "|" + (Convert.ToInt32(iQnty) + iQuantity) + "|" + addType;
                }
                else
                {
                    strCartValues += "," + iProductID.ToString() + "|" + iQuantity;
                }


            }
            //### If there is only a single item in the cart, add the new item to the cart and add a "," to split the item from the existing item.
            else
            {
                if (strCartValues.Contains("|"))
                {
                    //### Split the cart item's details (ID, Quantity, Product Type).
                    string[] strCartItemDetails = strCartValues.ToString().Split(new char[] { '|' });

                    iProdID = strCartItemDetails[0];
                    iQnty = strCartItemDetails[1];

                    //### If the current product(item) being added does not exists in the session, add the new item.
                    if (!strCartValues.Contains("," + iProductID.ToString() + "|" + iQnty) && !strCartValues.Contains(iProductID.ToString() + "|" + iQnty))
                    {
                        strCartValues += "," + iProductID.ToString() + "|" + iQuantity;
                    }
                    //### If the current product(item) being added does exist in the session, update the existing item's quantity.
                    else
                    {
                        strCartValues = strCartValues.Replace(iProductID.ToString() + "|" + iQnty, "");
                        strCartValues += iProductID.ToString() + "|" + (Convert.ToInt32(iQnty) + iQuantity);
                    }
                }
                else
                {
                    strCartValues = iProductID.ToString() + "|" + iQuantity;
                }
            }

            //### Update the session.
            HttpContext.Current.Session["BevtecOrder"] = strCartValues;
        }

        //### If the user has no cookies available, create a new cookie.
        if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"] == null)
        {
            HttpCookie aCookie = new HttpCookie("OrderItemsBevtec");

            aCookie.Values["OrderItemInfo"] = HttpContext.Current.Session["BevtecOrder"].ToString();
            aCookie.Expires = DateTime.Now.AddDays(7);

            HttpContext.Current.Response.Cookies.Add(aCookie);
        }
        //### If the user has a cookie, update the existing cookie.
        else
        {
            if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"] != null)
            {
                //### We cannot add values to an existing cookie, therefore we need we delete the cookie 
                //### to create a new one later on

                HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now;
            }

            //### Write the updated "logged out order" session to a new cookie. 
            HttpCookie aCookie = new HttpCookie("OrderItemsBevtec");

            aCookie.Values["OrderItemInfo"] = HttpContext.Current.Session["BevtecOrder"].ToString();
            aCookie.Expires = DateTime.Now.AddDays(7);

            HttpContext.Current.Response.Cookies.Add(aCookie);
        }

        //myCartEvent(new EventArgs());
    }


    //### Static method to add a cart item from a session or cookie into the user's cart once the user logs in.
    public static void addCartItemFromSessionOrCookie()
    {
        string iProductID = "";
        string iQuantity = "";

        //### Check to see that a session exists.
        if (HttpContext.Current.Session["BevtecOrder"] != null)
        {
            //### If there is no order in session, create a new order.
            if (HttpContext.Current.Session["iOrderID"] == null)
            {
                CartSetup();
            }

            //### Split the cart items.
            string[] strCartItems = HttpContext.Current.Session["BevtecOrder"].ToString().Split(new char[] { ',' });

            foreach (string value in strCartItems)
            {
                if (!String.IsNullOrEmpty(value))
                {
                    //### Split the cart item's details.
                    string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                    iProductID = strCartItemDetails[0];
                    iQuantity = strCartItemDetails[1];
                }

                //### For each cart item in the session, add the item to the cart.
                addCartItem(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity));
            }

            //### Once the order has been made, kill the logged out order session.
            HttpContext.Current.Session["BevtecOrder"] = null;
        }
        else
        {
            //### If there is no logged out order in session, check to see if the user has an order cookie.
            if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"] != null)
            {
                //### If there is no order in session, create a new order.
                if (HttpContext.Current.Session["iOrderID"] == null)
                {
                    CartSetup();
                }

                //### Split the cart items in the cookie.
                string[] strCartItems = HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"].ToString().Split(new char[] { ',' });

                foreach (string value in strCartItems)
                {
                    if (!String.IsNullOrEmpty(value))
                    {
                        string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                        iProductID = strCartItemDetails[0];
                        iQuantity = strCartItemDetails[1];
                    }

                    //### For each cart item in the cookie, add the cart item to the cart.
                    addCartItem(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity));
                }
                //### Once the order has been made, kill the cookie.
                HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now;
            }
        }
    }

    #endregion

    #region GET COUNT METHODS

    //### Static method to get the cart count for the logged in user.
    public static int GetCartCount(int iOrderID)
    {
        int iCartCount = 0;

        clsOrderItems clsOrderItems = new clsOrderItems();
        DataTable dtOrderItems = clsOrderItems.GetOrderItemsList("iOrderID = " + iOrderID + "", "");

        iCartCount = dtOrderItems.Rows.Count;

        return iCartCount;
    }

    //### Static method to get the cart count for the logged out user.
    public static int GetLoggedOutCartCount()
    {
        getCart();

        string strCartValues = "";

        string iProdID = "";
        string iQnty = "";

        int iCartCount = 0;

        if (HttpContext.Current.Session["BevtecOrder"] != null)
        {
            strCartValues = HttpContext.Current.Session["BevtecOrder"].ToString();

            //### Trap for multiple cart items (if the string contains a "," then we know there are multiple cart items.)
            if (strCartValues.Contains(","))
            {
                //### Split the cart items
                string[] strCartItems = strCartValues.Split(new char[] { ',' });

                foreach (string value in strCartItems)
                {
                    //### Trap for empty cart item values.
                    if (!String.IsNullOrEmpty(value))
                    {
                        //### Splite cart item details (ID, Quantity, Product Type)
                        string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                        iProdID = strCartItemDetails[0];
                        iQnty = strCartItemDetails[1];
                    }

                    //### For each cart item in the session, add the quantity to the count.
                    iCartCount++;
                }
            }
            else
            {
                if (strCartValues.Contains("|"))
                {

                    //### Split the cart item's details (ID, Quantity, Product Type).
                    string[] strCartItemDetails = strCartValues.ToString().Split(new char[] { '|' });

                    iProdID = strCartItemDetails[0];
                    iQnty = strCartItemDetails[1];

                    iCartCount++;
                }
            }
        }

        return iCartCount;
    }

    #endregion

    #region GET CART ITEMS
    /// <summary>
    /// Gets all the items in the Cookie/Session
    /// </summary>
    /// <returns>DataTable</returns>
    public static DataTable Cart()
    {
        DataTable dtCart = new DataTable("tblCart");

        //if (HttpContext.Current.Session["clsAccountHolders"] != null)
        //{
        //    int iOrderID;
        //    iOrderID = Convert.ToInt32(HttpContext.Current.Session["iOrderID"]);

        //    //string iProdID = "";
        //    //string iQnty = "";

        //    //### Build a Datatable to return the data from the cookie.        
        //    object[] objcartItem = new object[2];

        //    dtCart.Columns.Add("iProductID");
        //    dtCart.Columns.Add("iQuantity");

        //    DataTable dtOrderItemsList = clsOrderItems.GetOrderItemsList("iOrderID=" + iOrderID.ToString(), "");

        //    foreach (DataRow r in dtOrderItemsList.Rows)
        //    {
        //        objcartItem[0] = r["iProductID"];
        //        objcartItem[1] = r["iQuantity"];

        //        dtCart.Rows.Add(objcartItem);
        //    }
        //}
        //else
        {
            //if (HttpContext.Current.Session["clsAccountHolders"] == null)
            //{}

            //Puts order item info into session BevtecOrder
            getCart();

            string strCartValues = "";
            string iProdID = "";
            string iQnty = "";

            int iCartCount = 0;

            //### Build a Datatable to return the data from the cookie.
            object[] objcartItem = new object[2];

            dtCart.Columns.Add("iProductID");
            dtCart.Columns.Add("iQuantity");

            if (HttpContext.Current.Session["BevtecOrder"] != null)
            {
                strCartValues = HttpContext.Current.Session["BevtecOrder"].ToString();

                //### Trap for multiple cart items (if the string contains a "," then we know there are multiple cart items.)
                if (strCartValues.Contains(","))
                {
                    //### Split the cart items
                    string[] strCartItems = strCartValues.Split(new char[] { ',' });

                    foreach (string value in strCartItems)
                    {
                        //### Trap for empty cart item values.
                        if (!String.IsNullOrEmpty(value))
                        {
                            //### Splite cart item details (ID, Quantity, Product Type)
                            string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                            iProdID = strCartItemDetails[0];
                            iQnty = strCartItemDetails[1];
                        }

                        //### For each cart item in the session, add the quantity to the count.
                        iCartCount += Convert.ToInt32(iQnty);

                        //### Set the details of the item to the objcartItem (datarow)
                        objcartItem[0] = iProdID;
                        objcartItem[1] = iQnty;

                        dtCart.Rows.Add(objcartItem);
                    }
                }
                else
                {
                    //### Split the cart item's details (ID, Quantity, Product Type).
                    string[] strCartItemDetails = strCartValues.ToString().Split(new char[] { '|' });

                    iProdID = strCartItemDetails[0];
                    iQnty = strCartItemDetails[1];

                    iCartCount += Convert.ToInt32(iQnty);

                    //### Set the details of the item to the objcartItem (datarow)
                    objcartItem[0] = iProdID;
                    objcartItem[1] = iQnty;

                    dtCart.Rows.Add(objcartItem);
                }
            }
            else
            {
            }
        }
        return dtCart;
    }

    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Gets the details of the cookie,puts it into the session
    /// </summary>
    private static HttpContext getCart()
    {
        if (HttpContext.Current.Session["BevtecOrder"] != "0")
        {
            if (HttpContext.Current.Session["BevtecOrder"] == null)
            {
                //### If there is no order in session check to see if a cookie exists.
                if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"] != null)
                {
                    if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"] != null)
                    {
                        //### If a cookie exists, add the cookie values to the current session.
                        HttpContext.Current.Session["BevtecOrder"] = HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"];
                    }
                }
            }
        }
        else
        {
            HttpContext.Current.Session["BevtecOrder"] = null;
            HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now.AddDays(-10);
        }

        return null;
    }
    
    #endregion

    #region REMOVE CART ITEMS

    public static void removeItemFromCart(int iProductID)//, int iQuantity)
    {
        //### Check for an Order in session
        if (HttpContext.Current.Session["iOrderID"] == null)
        {
            if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                //### If there is no order in session and the user is logged in, create a new order entry to tblOrders.
                CartSetup();
            }
        }

        //### Check to see if the user is logged in.
        if (HttpContext.Current.Session["clsAccountHolders"] != null)
        {
            //### If the user is logged in, create the order as normal.
            //addCartItem(iProductID);
            removeItemFromCookie(iProductID);
        }
        else
        {
            //### If the user is not logged in, create a logged out order(in session order + cookie order).
            //### The cookie gives the user the ability to register and continue with the order within 7 days.
            removeItemFromCookie(iProductID);
        }
    }

    private static void removeItemFromCookie(int iProductID)
    {
        //### Trap for logged out order in session.
        if (HttpContext.Current.Session["BevtecOrder"] == null)
        {
            //### If there is no order in session check to see if a cookie exists.
            if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"] != null)
            {
                if (HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"] != null)
                {
                    //### If a cookie exists, add the cookie values to the current session.
                    HttpContext.Current.Session["BevtecOrder"] = HttpContext.Current.Request.Cookies["OrderItemsBevtec"]["OrderItemInfo"];

                    //### Loop through this method to add the current item to the existing list of items.
                    removeItemFromCookie(iProductID);
                }
                else
                {
                }
            }
            else
            {
            }
        }
        else
        {
            //### If there is an order in session add a new cart item to that session.
            string strCartValues = "";

            string iProdID = "";
            string iQnty = "";
            //string strPrdTyp = "";

            //### Trap for empty Session.
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["BevtecOrder"].ToString()))
            {
                strCartValues = HttpContext.Current.Session["BevtecOrder"].ToString();
            }

            //### Trap for multiple cart items (if the string contains a "," then we know there are multiple cart items.)
            if (strCartValues.Contains(","))
            {
                //### Split the cart items
                string[] strCartItems = strCartValues.Split(new char[] { ',' });
                string strCartTemp = strCartValues;

                foreach (string value in strCartItems)
                {
                    //### Trap for empty cart item values.
                    if (!(String.IsNullOrEmpty(value)))
                    {
                        //### Split cart item details (ID, Quantity, Product Type)
                        string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                        iProdID = strCartItemDetails[0];
                        iQnty = strCartItemDetails[1];
                    }

                    //### If the current product(item) being added does not exists in the session, add the new item. iProductID.ToString() != iProdID
                    if (!strCartTemp.Contains("," + iProdID + "|" + iQnty) && !strCartTemp.Contains(iProductID.ToString() + "|" + iQnty))
                    {
                        //strCartValues += value;
                    }
                    //### If the current product(item) being added does exist in the session, update the existing item's quantity.
                    else
                    {
                        if (strCartTemp.Contains("," + iProductID.ToString() + "|" + iQnty))
                        {
                            strCartValues = strCartValues.Replace("," + iProductID.ToString() + "|" + iQnty, "");
                            break;
                        }
                        else
                        {
                            if (strCartTemp.Contains(iProductID.ToString() + "|" + iQnty + ","))
                            {
                                strCartValues = strCartValues.Replace(iProductID.ToString() + "|" + iQnty + ",", "");
                                break;
                            }

                        }
                    }
                }
            }
            else //### If there is only a single item in the cart, add the new item to the cart and add a "," to split the item from the existing item.
            {
                //### Split the cart item's details (ID, Quantity, Product Type).
                string[] strCartItemDetails = strCartValues.ToString().Split(new char[] { '|' });

                iProdID = strCartItemDetails[0];
                iQnty = strCartItemDetails[1];

                strCartValues = strCartValues.Replace(iProductID.ToString() + "|" + iQnty, "");
            }

            //### Update the session.
            HttpContext.Current.Session["BevtecOrder"] = strCartValues;
            if (strCartValues != "")
            {
                //### Write the updated "logged out order" session to a new cookie. 
                HttpCookie aCookie = new HttpCookie("OrderItemsBevtec");

                aCookie.Values["OrderItemInfo"] = HttpContext.Current.Session["BevtecOrder"].ToString();
                aCookie.Expires = DateTime.Now.AddDays(7);

                HttpContext.Current.Response.Cookies.Add(aCookie);
            }
            else
            {
                HttpContext.Current.Response.Cookies["OrderItemsBevtec"]["OrderItemInfo"] = null;
                HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now;
                HttpContext.Current.Session["BevtecOrder"] = null;
            }
        }
    }

    public static void FinalizeCart()
    {
        HttpContext.Current.Request.Cookies.Remove("OrderItemsBevtec");
    }

    #endregion

    #region LOGIN

    public static void doLogin()
    {
        CartSetup();

        if (HttpContext.Current.Session["BevtecOrder"] != null)
        {
            //### If the user added items to the cart while logged out then the BevtecOrder session should not be empty(if the session is stil alive)
            //### Create the order item in the DB
            createOrder(HttpContext.Current.Session["BevtecOrder"].ToString());
        }
        else
        {
            //### The user might not have added items to the cart loged out, or the session was lost, get the items from the cookie.
            //### This method will put the cookie back in the session for us.
            getCart();
            if (HttpContext.Current.Session["BevtecOrder"] != null)
            {
                //### Create the order item in the DB
                createOrder(HttpContext.Current.Session["BevtecOrder"].ToString());
            }
        }

        //### After processing the loged out order clear the cookie so that if you log out and back in you dont get a duplicate order.
        clearCookie();
    }

    private static void createOrder(string strOrder)
    {
        if (strOrder != "")
        {
            int iOrderID = 0;
            clsAccountHolders clsAccountHolders;
            clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];
            iOrderID = Convert.ToInt32(HttpContext.Current.Session["iOrderID"]);
            
            foreach (string item in strOrder.Split(','))
            {
                int iProductID = 0;
                int iQuantity = 0;

                iProductID = int.Parse(item.Split('|')[0]);
                iQuantity = int.Parse(item.Split('|')[1]);

                if (!clsCommonFunctions.DoesRecordExist("tblOrderItems", "iOrderID = " + iOrderID + " AND iProductID = " + iProductID + " AND bIsDeleted=0"))
                {
                    //### If the order item exists create a new entry.
                    clsOrderItems clsOrderItems = new clsOrderItems();

                    clsOrderItems.dtAdded = DateTime.Now;
                    clsOrderItems.iAddedBy = clsAccountHolders.iAccountHolderID;
                    clsOrderItems.iOrderID = iOrderID;
                    clsOrderItems.iProductID = iProductID;
                    //clsOrderItems.iOrderItemStatus = 0;
                    clsOrderItems.iQuantity += iQuantity;
                    //clsOrderItems.bIsJewellery = bIsJewellery;
                    clsOrderItems.Update();
                }
                else
                {
                    //### If the order item entry already exists, update the item's quantity.
                    clsOrderItems clsOrderItems = new clsOrderItems(iOrderID);

                    clsOrderItems.dtEdited = DateTime.Now;
                    clsOrderItems.iEditedBy = clsAccountHolders.iAccountHolderID;
                    clsOrderItems.iOrderID = iOrderID;
                    //clsOrderItems.iOrderItemStatus = 0;
                    clsOrderItems.iQuantity += iQuantity;
                    //clsOrderItems.bIsJewellery = bIsJewellery;
                    clsOrderItems.Update();
                }

            }
        }
    }

    private static void clearCookie()
    {
        HttpContext.Current.Response.Cookies["OrderItemsBevtec"]["OrderItemInfo"] = null;
        HttpContext.Current.Response.Cookies["OrderItemsBevtec"].Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Session["BevtecOrder"] = "0";
    }

    #endregion

    #region GET TOTALS METHOD

    //### Get the total cost for the order.
    public static double GetOrderTotal(int iOrderID)
    {
        Double dblOrderTotal = 0.00;

        clsOrders clsOrders = new clsOrders(iOrderID);

        dblOrderTotal = clsOrders.dblTotalAmount;

        return dblOrderTotal;
    }

    //### Get the total cost of the logged out order.
    public static double GetLoggedOutOrderTotal()
    {
        double dblOrderTotal = 0.00;

        string strCartValues = "";

        string iProdID = "";
        string iQnty = "";
        //string strAtp = "";

        if (!String.IsNullOrEmpty(HttpContext.Current.Session["BevtecOrder"].ToString()))
        {
            strCartValues = HttpContext.Current.Session["BevtecOrder"].ToString();
        }

        //### Trap for multiple cart items (if the string contains a "," then we know there are multiple cart items.)
        if (strCartValues.Contains(","))
        {
            //### Split the cart items
            string[] strCartItems = strCartValues.Split(new char[] { ',' });

            foreach (string value in strCartItems)
            {
                //### Trap for empty cart item values.
                if (!(String.IsNullOrEmpty(value)))
                {
                    //### Split cart item details (ID, Quantity, Product Type)
                    string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                    iProdID = strCartItemDetails[0];
                    iQnty = strCartItemDetails[1];
                }

                if (!(String.IsNullOrEmpty(iProdID)))
                {
                    clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProdID));
                    dblOrderTotal += (clsProducts.dblPrice) * Convert.ToInt32(iQnty);
                }
            }
        }
        else
        {
            //### Split the cart item's details (ID, Quantity).
            string[] strCartItemDetails = strCartValues.ToString().Split(new char[] { '|' });

            iProdID = strCartItemDetails[0];
            iQnty = strCartItemDetails[1];

            clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProdID));
            dblOrderTotal += (clsProducts.dblPrice) * Convert.ToInt32(iQnty);
        }

        return dblOrderTotal;
    }

    //### Get the total vat of the logged out order.
    public static double GetLoggedOutOrderVat()
    {
        double dblVat = 0.00;
        double dblOrderTotal = 0.00;

        dblOrderTotal = GetLoggedOutOrderTotal();

        dblVat = Convert.ToDouble(dblOrderTotal * (Convert.ToDouble(14) / Convert.ToDouble(100)));

        return dblVat;
    }

    #endregion
}