﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Bevec_Categories : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        popCategories();
    }
    private void popCategories()
    {
        DataTable dtCategories = clsCategories.GetCategoriesList("bIsFeatured = true", "strTitle ASC");

        int iBestSellerCount = dtCategories.Rows.Count;
        dtCategories.Columns.Add("FullPathForImage");
        dtCategories.Columns.Add("Link");
        dtCategories.Columns.Add("divReset");
        dtCategories.Columns.Add("classForColumn");

        int iCount = 0;

        foreach (DataRow dtrCategories in dtCategories.Rows)
        {
            ++iCount;

            if (iCount == 1)
            {
                //dtrProduct["firstProduct"] = "";
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
            }

            if (iCount % 3 == 1 && iCount != 1)
            {
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 2)
            {
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 0)
            {
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if ((iCount % 3 == 0) || (iCount == dtCategories.Rows.Count))
            {
                dtrCategories["divReset"] = "<div class=\"clear\"></div>";
            }

            if (!(dtrCategories["strMasterImage"].ToString() == "") || (dtrCategories["strMasterImage"] == null) || (dtrCategories["strPathToImages"] == null))
            {
                dtrCategories["FullPathForImage"] = "Categories/" + dtrCategories["strPathToImages"] + "/crop_" + dtrCategories["strMasterImage"];
                dtrCategories["Link"] = "BevTec-Products.aspx?iCategoryID=" + dtrCategories["iCategoryID"].ToString();

               
            }

            else
            {
                dtrCategories["FullPathForImage"] = "img/no-image.png";
                dtrCategories["Link"] = "BevTec-Products.aspx?iCategoryID=" + dtrCategories["iCategoryID"].ToString();
            }
        }
        rpCategories.DataSource = dtCategories;
        rpCategories.DataBind();
    }
}