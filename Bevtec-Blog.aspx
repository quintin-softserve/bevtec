﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bevtec-Blog.aspx.cs" Inherits="Bevtec_Blog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="images/bevtec-images/Bevtec-Logo-150.png" />
    <title>BevTec</title>
    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->
</head>
<body class="home  page-template page-template-template-agency-home page-template-template-agency-home-php  jt_agency_content is_front_page wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
    <form id="form1" runat="server">
        <div class="wrapper">
            <!-- Wrapper -->
            <div class="container-fluid padding-zero jt-main-banner-holder">
                <div class="jt-page-header  jt-blog-page" style="background: url('images/BLOGBANNER.png'); background-size: cover;">
                    <header class=" sticky-nav sticky-rev">
                        <nav class="navbar navbar-default navbar-static-top">
                            <div class="">
                                <div class="navbar-header">
                                    <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                        <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                    </a>
                                    <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                        <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                    </a>
                                </div>
                                <div class="menu-metas navbar-default navbar-right">
                                    <ul class="navbar-nav">
                                        <%--<li id="top-search" class="jt-menu-search">
                                        <a href="#" id="top-search-trigger">
                                            <i class="fa fa-search"></i>
                                            <i class="pe-7s-close"></i>
                                        </a>
                                        <form class="container search-new" method="get" id="searchform" action="#">
                                            <input type="text" name="s" class="form-control" placeholder="Type &amp; Hit Enter..">
                                        </form>
                                    </li>--%>
                                    </ul>
                                </div>
                                <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                    <ul id="menu-create-menu" style="margin-top: 8%" class="nav navbar-nav navbar-right jt-main-nav">
                                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-AboutUs.aspx">About Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Categories.aspx">Categories</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Products.aspx">Products</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Blog.aspx">Blog</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Cart.aspx">
                                                <img src="images/icons/shop-cart.png" alt=" " />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <!-- Slim Menu -->
                        <div class="hidden-big-screen ">
                            <div class=" sticky-nav sticky-rev jt-slim-top">
                                <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                    <img src="upload/logo-dark.png" alt="Juster" />
                                </a>
                                <div class="menu-main-menu-container">
                                    <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-AboutUs.aspx">About Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Categories.aspx">Categories</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Products.aspx">Products</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Cart.aspx">
                                                <img src="images/icons/shop-cart.png" alt=" " />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /Slim Menu -->


                    </header>
                    <div class="jt-agency-banner-content jt-arch-bantext-3null">
                        <h1 style="color:white" class="page_heading">Blog</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-zero jt_content_holder">
            <div class="entry-content page-container content-ctrl">
                <!-- Main Container -->
                <div class="container-fluid padding-zero">
                    <!-- Container -->
                    <div class="main-content col-lg-12 padding-zero">
                        <article id="post-143" class="post-143 page type-page status-publish hentry">
                            <div class="entry-content">
                                <div class="vc_row jt_row_class wpb_row vc_row-fluid column-have-space">
                                    <div class="container">
                                        <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                            <div>
                                                <div class="space-fix " style="">
                                                    <div class="wpb_wrapper">
                                                        <div class="blog-style-one blog-style-one-wide ">
                                                            <asp:Repeater ID="rpBlogs" runat="server">
                                                                <ItemTemplate>
                                                                    <div class="jt-blog-lists">
                                                                        <div class="jt-thumb-hv">
                                                                            <a href="#" class="jt-post-thumb">
                                                                                <img src='<%#Eval ("FullPathForImage") %>' alt="">
                                                                            </a>
                                                                        </div>
                                                                        <div class="jt-content-hv">
                                                                            <div class="jt-post-contents">
                                                                                <div class="jt-list-cat">
                                                                                    <a href="#" rel="category tag"><%#Eval ("BlogTypeTitle") %></a>
                                                                                </div>
                                                                                <a href="#" class="jt-post-title"><%#Eval ("strTitle") %></a>
                                                                                <p><%#Eval ("strTagLine") %></p>
                                                                                <ul class="jt-post-list-metas">
                                                                                    <li>
                                                                                        <img src="images/icons/clock.png" height="16" width="16" alt="">August 20, 2015</li>
                                                                                    <li>
                                                                                        <img src="images/icons/comments.png" alt=""><a href="#">Leave a comment</a></li>
                                                                                    <li>
                                                                                        <%-- <img src="images/icons/user.png" alt="">
                                                                                            By
                                                                            <a href="#" rel="author">victor</a>--%>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="jt-read-more-hv">
                                                                            <a href='<%#Eval ("Link") %>' class="jt-list-read-more">
                                                                                <span>Read More</span>
                                                                                <span class="arrow-right">
                                                                                    <img src="images/arrows/box-arrow-right.png" alt=""></span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---Style Five -->
                                                <!---Style Six -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- end main-content -->
                </div>
            </div>
            <!-- End Container -->
        </div>

        <!-- End Main Container -->

        <div class="container-fluid padding-zero foot-ctrl">
            <footer class="jt-footer-style-two   ">

                <div class="col-lg-8 col-md-8 col-sm-8 ">
                    <!-- Footer Widgets -->
                    <div class="container">
                        <p>
                            <div class="" style="height: 20px;"></div>
                            <img src="images/bevtec-images/Untitled-1.png" alt="" />
                            <div class="" style="height: 20px;"></div>
                            <ul class="jt-social-one social_eight_center">
                                <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                                <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                                <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                                <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                                <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                            </ul>
                        </p>
                    </div>
                </div>
                <!-- Copyright Widget Area -->

                <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <div class="widget">
                        <div class="jt-widget-content">
                            <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                <h3 class="widget-title">Keep in Touch</h3>
                                <ul class="jt-widget-address">
                                    <li class="jt-add-icon jt-add-li">
                                        <i class="pe-7s-map-2"></i>
                                        <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                    </li>
                                             <li class="jt-add-icon jt-mail-li">
                                        <i class="pe-7s-mail"></i>
                                         <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                    </li>
                                    <li class="jt-add-icon jt-phone-li">
                                        <i class="pe-7s-call"></i>
                                        <span>+27  83 709 6554</span>
                                    </li>
                           
                                    <li class="jt-add-icon jt-fax-li">
                                        <i class="pe-7s-call"></i>
                                        <span>+27  83 785 4996</span>
                                    </li>
                                </ul>
                            </div>
                            <!-- end widget -->
                        </div>
                    </div>
                </div>
                <div class="jt-copyright-area">
                    <div class="container">
                        <p>
                            <div class="" style="height: 12px;"></div>
                            <div class="" style="height: 12px;"></div>
                            <p></p>
                    </div>
                </div>
                <!-- Footer Copyrights -->
            </footer>
        </div>
    </form>
    <script src="js/index.js"></script>
    <script type='text/javascript' src='js/jquery/jquery.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/frontend/add-to-cart.min.js'></script>
    <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
    <script type='text/javascript' src='js/vendors/woocommerce-add-to-cart.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

    <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            // Sticky Navbar
            $(".sticky-nav").sticky({
                topSpacing: 0
            });
        });
    </script>

    <script type='text/javascript' src='js/parallax.js'></script>
    <script type='text/javascript' src='js/jquery.form.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
    <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
    <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
    <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

    <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type='text/javascript' src='js/owl.carousel.min.js'></script>
    <script type='text/javascript' src='js/animate-plus.min.js'></script>
    <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/scripts.js'></script>
    <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
    <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.sticky.js'></script>
    <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
    <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.easing.min.js'></script>
    <script type='text/javascript' src='js/dynamic-js.js'></script>
    <script type='text/javascript' src='js/js_composer_front.js'></script>
    <script type='text/javascript' src='js/wow.min.js'></script>
    <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
    <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
    <script type='text/javascript' src='js/tiltfx.js'></script>
    <script type='text/javascript' src='js/ellipsis.js'></script>
    <script type='text/javascript' src='js/chaffle.js'></script>
    <script type='text/javascript' src='js/contact-form.js'></script>

    <script type='text/javascript' src='js/juster-banner-effect.js'></script>
    <script type='text/javascript' src='js/juster-banner-one.js'></script>
    <script type='text/javascript' src='js/typed.js'></script>
    <script type='text/javascript' src='js/parallax.js'></script>
</body>
</html>
