﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BevTec_Products : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsProducts clsProducts;
    List<clsProducts> glstProducts;
    protected void Page_Load(object sender, EventArgs e)
    {
        popProducts();

    }
    private void popProducts()
    {
        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");

        int iCount = 0;

        foreach (DataRow dtrProduct in dtProducts.Rows)
        {
            ++iCount;

            if (iCount == 1)
            {
                //dtrProduct["firstProduct"] = "";
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
            }

            if (iCount % 3 == 1 && iCount != 1)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 2)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 0)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if ((iCount % 3 == 0) || (iCount == dtProducts.Rows.Count))
            {
                dtrProduct["divReset"] = "<div class=\"clear\"></div>";
            }

            if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
            {
                dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                dtrProduct["Link"] = "Conscious-Birth-Product-Information.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
            }
            else
            {
                dtrProduct["FullPathForImage"] = "img/no-image.png";
            }
        }
        rpProducts.DataSource = dtProducts;
        rpProducts.DataBind();
    }
}