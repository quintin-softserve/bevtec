﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BevTec-Products-listing.aspx.cs" Inherits="BevTec_Products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="images/bevtec-images/Bevtec-Logo-150.png" />
    <title>BevTec</title>
    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />

    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
</head>
<body class="page-template-default  jt_main_content wpb-js-composer vc_responsive">
    <form id="form1" runat="server">
        <div>
            <div class="wrapper">
                <!-- Wrapper -->
                <div class="container-fluid padding-zero jt-main-banner-holder">
                    <div class="jt-page-header  jt-blog-page">
                        <div class="jt-banner-overlay" style="background: ;"></div>
                        <header class=" sticky-nav sticky-rev">
                            <nav class="navbar navbar-default navbar-static-top">
                                <div class="">
                                    <div class="navbar-header">
                                        <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                            <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                        </a>
                                        <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                            <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                                        </a>
                                    </div>
                                    <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                        <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                                            <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-AboutUs.aspx">About Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-Categories.aspx">Categories</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-Products.aspx">Products</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Blog.aspx">Blog</a>
                                            </li>

                                            <li class="menu-item ">
                                                <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Cart.aspx">
                                                    <img src="images/icons/shop-cart.png" alt=" " />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- Slim Menu -->
                            <div class="hidden-big-screen ">
                                <div class=" sticky-nav sticky-rev jt-slim-top">
                                    <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                        <img src="upload/logo-dark.png" alt="Juster" />
                                    </a>
                                    <div class="menu-main-menu-container">
                                        <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                                            <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-AboutUs.aspx">About Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-Categories.aspx">Categories</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-Products.aspx">Products</a>
                                            </li>

                                            <li class="menu-item ">
                                                <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Cart.aspx">
                                                    <img src="images/icons/shop-cart.png" alt=" " />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /Slim Menu -->
                        </header>

                        <div class="slider-container jt-vintage-banner jt-vint-small-banner" style="background: url('images/bevtec-images/Tap-beer-gq-india.png'); background-size: cover;">
                        </div>
                        <div class="jt-agency-banner-content jt-arch-bantext-3null">
                            <h1 style="color: white" class="page_heading">Shop</h1>
                            <h3 style="color: white">
                                <asp:Literal ID="LitCateName" runat="server"></asp:Literal></h3>
                        </div>
                        <div class="jt-banner-graphic">
                            <div class="jt-ban-icon faa-burst animated fa-fast"></div>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1100ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1s" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="900ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="800ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="700ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="600ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="500ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="400ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="300ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        </div>
                    </div>
                </div>

                <div class="container-fluid padding-zero jt_content_holder">
                    <div class="entry-content page-container content-ctrl">
                        <!-- Main Container -->
                        <div class="container-fluid padding-zero">
                            <!-- Container -->
                            <div class="productContainer container main-content-center">
                                <div class="main-content col-lg-12 padding-zero">
                                    <article id="post-1976" class="post-1976 page type-page status-publish hentry">
                                        <div class="entry-content">
                                            <div class="vc_row jt_row_class wpb_row vc_row-fluid column-have-space">

                                                <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                                    <div>
                                                        <div class="space-fix " style="">
                                                            <div class="wpb_wrapper">

                                                                <!-- Products -->
                                                                <div class="vc-products jt-product-style-one jt-product-wide ">
                                                                    <div id="NoProduct" runat="server">

                                                                        <div class="main-content col-md-12 col-sm-12">
                                                                        </div>
                                                                    </div>

                                                                    <div class="main-content col-md-8 col-sm-7">

                                                                        <ul class="products">
                                                                            <li>
                                                                                <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                                                                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                                                                                </div>
                                                                            </li>

                                                                            <div>
                                                                                <div id="Hidecontent" runat="server">
                                                                                    <div class="space-fix " style="">
                                                                                        <div class="wpb_wrapper">
                                                                                            <table class="table table-bordered table-extrawidth table-normal ">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th style="">Title</th>
                                                                                                        <th style="">Description</th>
                                                                                                        <th style="">Price</th>
                                                                                                        <th style="display: none"></th>
                                                                                                    </tr>

                                                                                                    <asp:Repeater ID="rpProducts" runat="server">
                                                                                                        <ItemTemplate>

                                                                                                            <tr>

                                                                                                                <td style=""><a href='<%#Eval ("Link") %>'><%#Eval ("strTitle") %> </a></td>

                                                                                                                <td style=""><%#Eval ("strDescription") %></td>
                                                                                                                <td style=""><%#Eval ("dblPrice") %>
                                                                                                                   
                                                                                                                </td>
                                                                                                                <td style="width: 15%;">
                                                                                                                    <asp:LinkButton ID="btnAddToCart" runat="server" CssClass="button  " CommandArgument='<%#Eval("iProductID")%>' OnClick="btnAddToCart_Click">ADD TO CART</asp:LinkButton>
                                                                                                                    <br />
                                                                                                                </td>

                                                                                                            </tr>

                                                                                                        </ItemTemplate>
                                                                                                    </asp:Repeater>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                            
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-md-4 col-sm-5 padding-right-zero">
                                                                    <div class="sidebar ">
                                                                        <div id="search-3" class="widget sep-hover-control widget_search">
                                                                            <div id="" class="search-new">
                                                                                <div>
                                                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Search Here"> </asp:TextBox><asp:ImageButton ID="btnSearch" runat="server" Style="border-width: 0px; width: 50px;" OnClick="ImageButton1_Click" ImageUrl="~/images/bevtec-images/search-icon.png" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- end widget -->

                                                                        <div id="categories-3" class="widget sep-hover-control widget_categories">

                                                                            <h3 class="widget-title">Categories</h3>
                                                                            <div class="jt-sep-two"></div>
                                                                            <ul>
                                                                                <asp:Repeater ID="rpCategories" runat="server">
                                                                                    <ItemTemplate>
                                                                                        <li class="cat-item cat-item-26"><a href="<%#Eval ("Link") %>"><%#Eval ("strTitle") %></a>
                                                                                        </li>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </ul>
                                                                        </div>
                                                                        <!-- end widget -->

                                                                    </div>
                                                                </div>
                                                                <div class='wp-pagenavi'>
                                                                    <span class='pages'>Page 1 of 3</span><span class='current'>1</span><a class="page larger" href="#">2</a><a class="page larger" href="#">3</a><a class="nextpostslink" rel="next" href="#">&raquo;</a>
                                                                </div>
                                                                </ul>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </article>

                            </div>
                            <!-- end main-content -->

                        </div>
                    </div>
                    <!-- End Container -->
                </div>
            </div>
            <!-- End Main Container -->


            <div class="container-fluid padding-zero foot-ctrl">
                <footer class="jt-footer-style-two   ">

                    <div class="col-lg-8 col-md-8 col-sm-8 ">
                        <!-- Footer Widgets -->
                        <div class="container">
                            <p>
                                <div class="" style="height: 20px;"></div>
                                <img src="images/bevtec-images/Untitled-1.png" alt="" />
                                <div class="" style="height: 20px;"></div>
                                <ul class="jt-social-one social_eight_center">
                                    <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                                    <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                                    <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                                    <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                                    <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <!-- Copyright Widget Area -->

                    <div class="col-lg-3 col-md-3 col-sm-3 ">
                        <div class="widget">
                            <div class="jt-widget-content">
                                <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                    <h3 class="widget-title">Keep in Touch</h3>
                                    <ul class="jt-widget-address">
                                        <li class="jt-add-icon jt-add-li">
                                            <i class="pe-7s-map-2"></i>
                                            <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                        </li>
                                             <li class="jt-add-icon jt-mail-li">
                                            <i class="pe-7s-mail"></i>
                                             <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                        </li>
                                        <li class="jt-add-icon jt-phone-li">
                                            <i class="pe-7s-call"></i>
                                            <span>+27  83 709 6554</span>
                                        </li>
                                   
                                        <li class="jt-add-icon jt-fax-li">
                                            <i class="pe-7s-call"></i>
                                            <span>+27  83 785 4996</span>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end widget -->
                            </div>
                        </div>
                    </div>
                    <div class="jt-copyright-area">
                        <div class="container">
                            <p>
                                <div class="" style="height: 12px;"></div>
                                <div class="" style="height: 12px;"></div>
                                <p></p>
                        </div>
                    </div>
                    <!-- Footer Copyrights -->
                </footer>
            </div>
            <!-- Footer Copyrights -->

        </div>

        <script type='text/javascript' src='js/jquery/jquery.js'></script>
        <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/frontend/add-to-cart.min.js'></script>
        <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
        <script type='text/javascript' src='js/vendors/woocommerce-add-to-cart.js'></script>
        <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

        <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                "use strict";
                // Sticky Navbar
                $(".sticky-nav").sticky({
                    topSpacing: 0
                });
            });
        </script>
        <script type='text/javascript' src='js/jquery.form.min.js'></script>
        <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
        <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
        <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
        <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
        <script type='text/javascript' src='js/woocompare.js'></script>
        <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
        <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

        <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
        <script type='text/javascript' src='js/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/owl.carousel.min.js'></script>
        <script type='text/javascript' src='js/animate-plus.min.js'></script>
        <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/scripts.js'></script>
        <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
        <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/jquery.sticky.js'></script>
        <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
        <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
        <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
        <script type='text/javascript' src='js/slimmenu/jquery.easing.min.js'></script>
        <script type='text/javascript' src='js/dynamic-js.js'></script>
        <script type='text/javascript' src='js/js_composer_front.js'></script>
        <script type='text/javascript' src='js/wow.min.js'></script>
        <script type='text/javascript' src='js/dragcarousel/elastiStack.js'></script>
        <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
        <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
        <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
        <script type='text/javascript' src='js/tiltfx.js'></script>
        <script type='text/javascript' src='js/ellipsis.js'></script>
        <script type='text/javascript' src='js/chaffle.js'></script>
        <script type='text/javascript' src='js/contact-form.js'></script>



        </div>
    </form>
</body>
</html>
