﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bevtec-Product-Details.aspx.cs" Inherits="Bevtec_Product_Details" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8">

    <link rel="shortcut icon" href="images/bevtec-images/Bevtec-Logo-150.png" />
    <title>BevTec</title>

    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />

    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />

</head>
<body class="single single-product postid-93  jt_shop_content woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
    <form runat="server" id="form">
        <div class="wrapper " style="background: rgba(53, 55, 62, 0.5); position: fixed; z-index: 1; width: 100%">
            <!-- Wrapper -->

            <nav class="navbar navbar-default navbar-static-top">
                <div class="">
                    <div class="navbar-header">
                        <a href="index.html" rel="home" class="default navbar-logo default-logo">
                            <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                        </a>
                        <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                            <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Juster" />
                        </a>
                    </div>
                    <div class="menu-metas navbar-default navbar-right">
                        <ul class="navbar-nav">
                            <%--<li id="top-search" class="jt-menu-search">
                                        <a href="#" id="top-search-trigger">
                                            <i class="fa fa-search"></i>
                                            <i class="pe-7s-close"></i>
                                        </a>
                                        <form class="container search-new" method="get" id="searchform" action="#">
                                            <input type="text" name="s" class="form-control" placeholder="Type &amp; Hit Enter..">
                                        </form>
                                    </li>--%>
                        </ul>
                    </div>
                    <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                        <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                            <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                            </li>
                            <li class="menu-item ">
                                <a href="BevTec-AboutUs.aspx">About Us</a>
                            </li>
                            <li class="menu-item ">
                                <a href="BevTec-Categories.aspx">Categories</a>
                            </li>
                            <li class="menu-item ">
                                <a href="BevTec-Products.aspx">Products</a>
                            </li>
                            <li class="menu-item ">
                                <a href="Bevtec-Blog.aspx">Blog</a>
                            </li>

                            <li class="menu-item ">
                                <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                            </li>
                            <li class="menu-item ">
                                <a href="Bevtec-Cart.aspx">
                                    <img src="images/icons/shop-cart.png" alt=" " />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- /Menu List -->
        </div>
        <!-- Slim Menu -->
        <div class="hidden-big-screen ">
            <div class=" sticky-nav sticky-rev jt-slim-top">
                <a href="index.html" rel="home" class="default navbar-logo default-logo">
                    <img src="upload/logo-dark.png" alt="Juster" />
                </a>
                <div class="menu-main-menu-container">
                    <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                        </li>
                        <li class="menu-item ">
                            <a href="BevTec-AboutUs.aspx">About Us</a>
                        </li>
                        <li class="menu-item ">
                            <a href="BevTec-Categories.aspx">Categories</a>
                        </li>
                        <li class="menu-item ">
                            <a href="BevTec-Products.aspx">Products</a>
                        </li>

                        <li class="menu-item ">
                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                        </li>
                        <li class="menu-item ">
                            <a href="Bevtec-Cart.aspx">
                                <img src="images/icons/shop-cart.png" alt=" " />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Slim Menu -->
        <!-- Layout -->
        <div class="container-fluid padding-zero jt_content_holder">
            <div class="entry-content  content-ctrl">
                <!-- Main Container -->
                <div class="container-fluid padding-zero" style="padding: 201px;">
                    <!-- Container -->
                    <div class="container main-content-center">
                        <div class="shop-template product-style-2 product-extra-width col-lg-12 col-lg-12 product-col-3">
                            <div class="product-column-3">

                                <div id="product-93" class="post-93 product type-product status-publish has-post-thumbnail product_cat-fabric product_tag-commerce product_tag-shop product_tag-woo product_tag-wordpress downloadable shipping-taxable purchasable product-type-simple product-cat-fabric product-tag-commerce product-tag-shop product-tag-woo product-tag-wordpress instock">

                                    <asp:Literal ID="litProducts" runat="server"></asp:Literal>

                                    <div class="cart" method="post" enctype='multipart/form-data'>
                                        <div class="prd-qty-text">Quantity</div>

                                        <div class="quantity">
                                            <asp:TextBox runat="server" ID="txtQuantity" type="number" step="1" min="1" name="quantity" value="1" title="Qty" CssClass="input-text qty text" size="4"></asp:TextBox>
                                        </div>
                                        <asp:Repeater ID="rpLinkCaer" runat="server">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnAddToCart" runat="server" CssClass="single_add_to_cart_button button alt" CommandArgument='<%#Eval("iProductID")%>' Style="margin-left: 26px;" OnClick="btnAddToCart_Click">ADD TO CART</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:Repeater>


                                        <div class="yith-wcwl-add-to-wishlist add-to-wishlist-93">
                                            <div class="yith-wcwl-add-button show" style="display: block">

                                                <a href="#" rel="nofollow" data-product-id="93" data-product-type="simple" class="add_to_wishlist"></a>
                                                <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility: hidden" />
                                            </div>

                                            <div class="yith-wcwl-wishlistaddedbrowse hide" style="display: none;">
                                                <span class="feedback">Product added!</span>
                                                <a href="#"></a>
                                            </div>

                                            <div class="yith-wcwl-wishlistexistsbrowse hide" style="display: none">
                                                <span class="feedback">The product is already in the wishlist!</span>
                                                <a href="#"></a>
                                            </div>

                                            <div style="clear: both"></div>
                                            <div class="yith-wcwl-wishlistaddresponse"></div>

                                        </div>

                                        <div class="clear"></div>
                                        <div class="jt-compare-prd">
                                            <div class="woocommerce product compare-button"><a href="#" class="compare button" data-product_id="93">Compare</a></div>
                                        </div>

                                    </div>

                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-93">
                                        <div class="yith-wcwl-add-button show" style="display: block">


                                            <a href="#" rel="nofollow" data-product-id="93" data-product-type="simple" class="add_to_wishlist"></a>
                                            <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility: hidden" />
                                        </div>

                                        <div class="yith-wcwl-wishlistaddedbrowse hide" style="display: none;">
                                            <span class="feedback">Product added!</span>
                                            <a href="#"></a>
                                        </div>

                                        <div class="yith-wcwl-wishlistexistsbrowse hide" style="display: none">
                                            <span class="feedback">The product is already in the wishlist!</span>
                                            <a href="#"></a>
                                        </div>

                                        <div style="clear: both"></div>
                                        <div class="yith-wcwl-wishlistaddresponse"></div>

                                    </div>

                                    <div class="clear"></div>
                                    <a href="#" class="compare button" data-product_id="93">Compare</a>
                                    <div class="product_meta">



                                        <%-- <span class="posted_in"><h6>Category:</h6> <a href="#" rel="tag">Fabric</a>.</span>
                                        <span class="tagged_as"><h6>Tags:</h6> <a href="#" rel="tag">Commerce</a>, <a href="#" rel="tag">Shop</a>, <a href="#" rel="tag">Woo</a>, <a href="#" rel="tag">Wordpress</a>.</span>--%>
                                    </div>


                                </div>
                                <!-- .summary -->


                                <div class="woocommerce-tabs wc-tabs-wrapper">
                                    <ul class="tabs wc-tabs">
                                        <li class="description_tab">
                                            <a href="#tab-description">Description</a>
                                        </li>
                                    </ul>
                                    <div class="panel entry-content wc-tab" id="tab-description">

                                        <h2>Product Description</h2>
                                        <asp:Literal ID="LitDescription" runat="server"></asp:Literal>
                                    </div>



                                </div>

                                <h2 style="text-align:center;font-weight:bold;color:#333">Pdf</h2>
                                <hr />
                                <div class="col-md-12">
                                    <asp:Repeater ID="rpPopulatePdf" runat="server">
                                        <ItemTemplate>
                                            <div class="col-md-3" style="text-align: center">
                                                <img src="CMS/images/icon_pdf.png" /><br />
                                                <a href='<%#Eval ("FullPathForPdf") %>' target="_blank"><%#Eval ("PdfName") %></a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>





                                <meta itemprop="url" content="index.html" />

                            </div>
                            <!-- #product-93 -->



                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- End Container -->
        </div>
        <!-- End Main Container -->
        <div class="jt-nxt-pre-posts">
            <asp:HyperLink ID="HypePreview" CssClass="jt-prev-post" runat="server">Prev Product</asp:HyperLink>
            <asp:HyperLink ID="HyperNext" CssClass="jt-next-post" runat="server">Next Product</asp:HyperLink>
        </div>

        <div class="container-fluid padding-zero foot-ctrl">
            <footer class="jt-footer-style-two   ">

                <div class="col-lg-8 col-md-8 col-sm-8 ">
                    <!-- Footer Widgets -->
                    <div class="container">
                        <p>
                            <div class="" style="height: 20px;"></div>
                            <img src="images/bevtec-images/Untitled-1.png" alt="" />
                            <div class="" style="height: 20px;"></div>
                            <ul class="jt-social-one social_eight_center">
                                <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                                <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                                <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                                <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                                <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                            </ul>
                        </p>
                    </div>
                </div>
                <!-- Copyright Widget Area -->

                <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <div class="widget">
                        <div class="jt-widget-content">
                            <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                <h3 class="widget-title">Keep in Touch</h3>
                                <ul class="jt-widget-address">
                                    <li class="jt-add-icon jt-add-li">
                                        <i class="pe-7s-map-2"></i>
                                        <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                    </li>
                                        <li class="jt-add-icon jt-mail-li">
                                        <i class="pe-7s-mail"></i>
                                         <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                    </li>
                                    <li class="jt-add-icon jt-phone-li">
                                        <i class="pe-7s-call"></i>
                                        <span>+27  83 709 6554</span>
                                    </li>
                                
                                    <li class="jt-add-icon jt-fax-li">
                                        <i class="pe-7s-call"></i>
                                        <span>+27  83 785 4996</span>
                                    </li>
                                </ul>
                            </div>
                            <!-- end widget -->
                        </div>
                    </div>
                </div>
                <div class="jt-copyright-area">
                    <div class="container">
                        <p>
                            <div class="" style="height: 12px;"></div>
                            <div class="" style="height: 12px;"></div>
                            <p></p>
                    </div>
                </div>
                <!-- Footer Copyrights -->
            </footer>
        </div>
    </form>
    <script type='text/javascript' src='js/jquery/jquery.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

    <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            // Sticky Navbar
            $(".sticky-nav").sticky({
                topSpacing: 0
            });
        });
    </script>
    <script type='text/javascript' src='js/jquery.form.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
    <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
    <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
    <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

    <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type='text/javascript' src='js/owl.carousel.min.js'></script>
    <script type='text/javascript' src='js/animate-plus.min.js'></script>
    <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/scripts.js'></script>
    <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
    <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.sticky.js'></script>
    <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
    <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
    <script type='text/javascript' src='js/dynamic-js.js'></script>
    <script type='text/javascript' src='js/js_composer_front.js'></script>
    <script type='text/javascript' src='js/wow.min.js'></script>
    <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
    <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
    <script type='text/javascript' src='js/tiltfx.js'></script>
    <script type='text/javascript' src='js/ellipsis.js'></script>
    <script type='text/javascript' src='js/chaffle.js'></script>
    <script type='text/javascript' src='js/contact-form.js'></script>

    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_single_product_params = { "i18n_required_rating_text": "Please select a rating", "review_rating_required": "yes" };
        /* ]]> */
    </script>
    <script type='text/javascript' src='js/frontend/single-product.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>

</body>
</html>
