﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BevTec-Contact-Us.aspx.cs" Inherits="Bevtec_ConactUs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <script>
        function validateEmailAddress(target) {
            if (target.value != '' || target.value != null) {
                var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (target.value.length > 0) {
                    if (target.value.match(emailExpression)) {
                        document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageValid';
                        document.getElementById("<%= hfCanSave.ClientID %>").value = "true";
                    }
                    else {
                        document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                        document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
                    }
                }
                else {
                    document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                    document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
                }
            }
            else {
                document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
            }
        }

        function showInProgress(target) {
            if (target.value != '' || target.value != null) {
                target.className = 'validationImageInProgress';
            }
        }
    </script>
</head>
<body class="home page page-id-6 page-template-default  jt_main_content have_rev_slider is_front_page wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
    <form id="form1" runat="server">
        <div class="wrapper">
            <!-- Wrapper -->
            <div class="container-fluid padding-zero jt-main-banner-holder">
                <div class="jt-page-header  jt-blog-page">
                    <div class="jt-banner-overlay" style="background: ;"></div>
                    <header class=" sticky-nav sticky-rev">
                        <nav class="navbar navbar-default navbar-static-top">
                            <div class="">
                                <div class="navbar-header">
                                    <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                        <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Corporate" />
                                    </a>
                                    <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                        <img src="images/logo.png" alt="Corporate" />
                                    </a>
                                </div>
                                <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                    <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-AboutUs.aspx">About Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Categories.aspx">Categories</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Products.aspx">Products</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Blog.aspx">Blog</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Cart.aspx">
                                                <img src="images/icons/shop-cart.png" alt=" " />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <!-- Slim Menu -->
                        <div class="hidden-big-screen ">
                            <div class=" sticky-nav sticky-rev jt-slim-top">
                                <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                    <img src="upload/logo-dark.png" alt="Juster" />
                                </a>
                                <div class="menu-main-menu-container">
                                    <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                                        <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-AboutUs.aspx">About Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Categories.aspx">Categories</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="BevTec-Products.aspx">Products</a>
                                        </li>

                                        <li class="menu-item ">
                                            <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                        </li>
                                        <li class="menu-item ">
                                            <a href="Bevtec-Cart.aspx">
                                                <img src="images/icons/shop-cart.png" alt=" " />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /Slim Menu -->
                    </header>
                    <div class="slider-container jt-vintage-banner jt-vint-small-banner" style="background: url('images/^E19E062AFEC93F77E3C6F9F6A57718512FF81E30B2FC921F61^pimgpsh_fullsize_distr.jpg'); background-size: cover;">
                    </div>
                   <div class="jt-agency-banner-content jt-arch-bantext-3null">
                        <h1 style="color:#b81a1f" class="page_heading">Contact Us</h1>
                    </div>
                    <div class="jt-banner-graphic">
                        <div class="jt-ban-icon faa-burst animated fa-fast"></div>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1100ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1s" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="900ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="800ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="700ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="600ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="500ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="400ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="300ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        <span class="animate-plus" data-animations="fadeIn" data-animation-delay="200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                    </div>
                </div>
            </div>

            <div class="container-fluid padding-zero jt_content_holder pt80">
                <div class="entry-content page-container content-ctrl " style="padding: 0px 0px 0px">
                    <!-- Main Container -->
                    <div class="container-fluid padding-zero">
                        <!-- Container -->
                        <div class="main-content col-lg-12 padding-zero">
                            <article id="post-247" class="post-247 page type-page status-publish hentry">
                                <div class="entry-content">
                                    <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1441889660431 column-have-space">
                                        <div class="container">
                                            <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                                <div>
                                                    <div class="space-fix " style="0">
                                                        <div class="wpb_wrapper">
                                                            <div class="jt-heading " style="text-align: center;">
                                                                <h3 class="jt-main-head" style="">Get In Touch</h3>
                                                                <div class="jt-sep"></div>
                                                                <h4 class="sub-heading" style="">Have a project you’re interested in discussing with us? Drop us a line below<br />
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1441886186739 column-have-space">
                                        <div class="container">
                                            <div class="vc_col-sm-8 wpb_column vc_column_container  has_animation">
                                                <div>
                                                    <div class="space-fix  text-left" style="00">
                                                        <div class="wpb_wrapper">
                                                            <div role="form" class="wpcf7" id="wpcf7-f251-p247-o1" lang="en-US" dir="ltr">
                                                                <div class="screen-reader-response"></div>
                                                                <div id="contact-form">
                                                                    <div class="row">
                                                                        <div class="controlDiv">
                                                                            <div class="imageHolderCommonDiv">
                                                                                <div class="validationImageMandatory"></div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                    <asp:TextBox ID="txtName" CssClass="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" runat="server"  Text="Name" onblur="setValid(this)" onfocus="clearNameText();" Style="font-family: PT Sans, sans-serif;"></asp:TextBox>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="controlDiv">
                                                                                <div class="imageHolderCommonDiv">
                                                                                    <div id="divImageHolder" runat="server" class="validationImageMandatory"></div>
                                                                                </div>
                                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                                    <asp:TextBox ID="txtEmail" runat="server" onblur="validateEmailAddress(this)" Style="font-family: PT Sans, sans-serif;" CssClass="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"  Text="Email" onfocus="clearEmailText();"></asp:TextBox>
                                                                                </span>
                                                                                <br class="clearingSpacer" />
                                                                                <asp:HiddenField ID="hfCanSave" runat="server" Value="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="controlDiv">
                                                                                <div class="imageHolderCommonDiv">
                                                                                    <div class="validationImageMandatory"></div>
                                                                                </div>
                                                                                <span class="wpcf7-form-control-wrap textarea-950">
                                                                                    <asp:TextBox ID="txtMessage" runat="server" Style="font-family: PT Sans, sans-serif;" CssClass="wpcf7-form-control wpcf7-textarea"  Text="Message" onblur="setValid(this)" onfocus="clearMessageText();" TextMode="MultiLine" Styl></asp:TextBox>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <asp:Button ID="btnSubmit" runat="server" CssClass="main-button" Text="Submit" OnClick="btnSubmit_Click" />
                                                                            <div id="msg" class="message"></div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="vc_col-sm-4 wpb_column vc_column_container  has_animation">
                                                <div>
                                                    <div class="space-fix " style="">
                                                        <div class="wpb_wrapper">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <ul class="jt-contact-addresses ">
                                                                        <li>
                                                                            <h4 class="jt-special " style="color: #000; font-size: 12px; text-transform: uppercase;">Address</h4>
                                                                            <div class="jt-special " style="color: #777; font-size: 15px;">
                                                                                Bev Tec (Pty) Ltd, 416 Martin Crescent, Greenhills Industrial Estate Tunney Ext 6 Germiston 1401, 
                                                                            <br />
                                                                                PO Box 75157, Gardenview 2047
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <h4 class="jt-special " style="color: #000; font-size: 12px; text-transform: uppercase;">Phone</h4>
                                                                            <div class="jt-special " style="color: #777; font-size: 15px;">
                                                                                +27  83 709 6554
                                                                            <br />
                                                                                +27  83 785 4996
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <h4 class="jt-special " style="color: #000; font-size: 12px; text-transform: uppercase;">E-Mail</h4>
                                                                            <div class="jt-special " style="color: #777; font-size: 15px;">info@bevtec.co.za</div>
                                                                        </li>
                                                                        <li>
                                                                            <h4 class="jt-special " style="color: #000; font-size: 12px; text-transform: uppercase;">Working hours</h4>
                                                                            <div class="jt-special " style="color: #777; font-size: 15px;">Daily 9.00 to 6.00 ( Sundays Available)</div>
                                                                        </li>
                                                                        <li>
                                                                            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                                                                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                                                                            </div>

                                                                        </li>

                                                                    </ul>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row jt_row_class wpb_row vc_row-fluid vc_custom_1441886070882 column-zero-space" style="margin-bottom: 0px;">

                                        <div class="vc_col-sm-12 wpb_column vc_column_container  has_animation">
                                            <div>
                                                <div class="space-fix " style="margin-bottom: 0px; padding-top: 30px; padding-bottom: 0px;">
                                                    <div class="wpb_wrapper">
                                                        <div class="mm-google-map ">
                                                            <div class="map" id="map" style="height: 430px;"></div>
                                                            <div class="mm-zoom mm-zoom-in"></div>
                                                            <div class="mm-zoom mm-zoom-out"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                        </div>
                        <!-- end main-content -->

                    </div>
                    <!-- End Container -->
                </div>
            </div>
            <!-- End Main Container -->

            <div class="container-fluid padding-zero foot-ctrl">
                <footer class="jt-footer-style-two   ">

                    <div class="col-lg-8 col-md-8 col-sm-8 ">
                        <!-- Footer Widgets -->
                        <div class="container">
                            <p>
                                <div class="" style="height: 20px;"></div>
                                <img src="images/bevtec-images/Untitled-1.png" alt="" />
                                <div class="" style="height: 20px;"></div>
                                <ul class="jt-social-one social_eight_center">
                                    <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                                    <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                                    <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                                    <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                                    <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <!-- Copyright Widget Area -->

                    <div class="col-lg-3 col-md-3 col-sm-3 ">
                        <div class="widget">
                            <div class="jt-widget-content">
                                <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                    <h3 class="widget-title">Keep in Touch</h3>
                                    <ul class="jt-widget-address">
                                        <li class="jt-add-icon jt-add-li">
                                            <i class="pe-7s-map-2"></i>
                                            <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                        </li>
                                          <li class="jt-add-icon jt-mail-li">
                                            <i class="pe-7s-mail"></i>
                                           <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                        </li>
                                        <li class="jt-add-icon jt-phone-li">
                                            <i class="pe-7s-call"></i>
                                            <span>+27  83 709 6554</span>
                                        </li>
                                      
                                        <li class="jt-add-icon jt-fax-li">
                                            <i class="pe-7s-call"></i>
                                            <span>+27  83 785 4996</span>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end widget -->
                            </div>
                        </div>
                    </div>
                    <div class="jt-copyright-area">
                        <div class="container">
                            <p>
                                <div class="" style="height: 12px;"></div>
                                <div class="" style="height: 12px;"></div>
                                <p></p>
                        </div>
                    </div>
                    <!-- Footer Copyrights -->
                </footer>
            </div>

            <script type='text/javascript' src='js/jquery/jquery.js'></script>
            <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
            <script type='text/javascript' src='js/frontend/add-to-cart.min.js'></script>
            <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
            <script type='text/javascript' src='js/vendors/woocommerce-add-to-cart.js'></script>
            <script type='text/javascript' src='../js/jquery.themepunch.tools.min.js'></script>
            <script type='text/javascript' src='../js/jquery.themepunch.revolution.min.js'></script>

            <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
            <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    "use strict";
                    // Sticky Navbar
                    $(".sticky-nav").sticky({
                        topSpacing: 0
                    });
                });
            </script>
            <script lang="javascript" type="text/javascript">
                //### Password check
                function clearNameText() {
                    if (document.getElementById("<%=txtName.ClientID%>").value == "Name") {
                        document.getElementById("<%=txtName.ClientID%>").value = ""
                    }
                }
                function clearEmailText() {
                    if (document.getElementById("<%=txtEmail.ClientID%>").value == "Email") {
                        document.getElementById("<%=txtEmail.ClientID%>").value = ""
                    }
                }
                function clearMessageText() {
                    if (document.getElementById("<%=txtMessage.ClientID%>").value == "Message") {
                        document.getElementById("<%=txtMessage.ClientID%>").value = ""
                    }
                }
            </script>


            <script type='text/javascript' src='js/jquery.form.min.js'></script>
            <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
            <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
            <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
            <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
            <script type='text/javascript' src='js/woocompare.js'></script>
            <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
            <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

            <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
            <script type='text/javascript' src='js/bootstrap.min.js'></script>
            <script type='text/javascript' src='js/owl.carousel.min.js'></script>
            <script type='text/javascript' src='js/animate-plus.min.js'></script>
            <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
            <script type='text/javascript' src='js/scripts.js'></script>
            <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
            <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
            <script type='text/javascript' src='js/jquery.sticky.js'></script>
            <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
            <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
            <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
            <script type='text/javascript' src='js/slimmenu/jquery.easing.min.js'></script>
            <script type='text/javascript' src='js/dynamic-js.js'></script>
            <script type='text/javascript' src='js/js_composer_front.js'></script>
            <script type='text/javascript' src='js/wow.min.js'></script>
            <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
            <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
            <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
            <script type='text/javascript' src='js/tiltfx.js'></script>
            <script type='text/javascript' src='js/ellipsis.js'></script>
            <script type='text/javascript' src='js/chaffle.js'></script>
            <script type='text/javascript' src='js/contact-form.js'></script>

            <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false'></script>
            <script type='text/javascript' src='js/custom-gmap.js'></script>
            <script type="text/javascript">
                var labVcMaps = labVcMaps || [];
                labVcMaps.push({
                    id: 'map',
                    locations: [{
                        "marker_image": "http://defatch-demo.com/themes/juster/corporate/wp-content/themes/juster/images/icons/map/map-marker-2.png",
                        "retina_marker": "",
                        "latitude": "-26.158950",
                        "longitude": "28.017560",
                        "marker_title": "Melbourne",
                        "marker_description": "<p>To explore strange new world with Us</p>"
                    }, ],

                    zoom: 10,
                    scrollwheel: false,
                    dropPins: true,
                    panBy: [0, 0],
                    tilt: 0,
                    heading: 0,

                    mapType: 'roadmap',

                    panControl: false,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    overviewMapControl: false,
                    plusMinusZoom: true,

                    styles: [{
                        stylers: [{
                            "saturation": -100
                        }, {
                            "lightness": -10
                        }]
                    }, ],
                });
            </script>
</body>
</form>
</html>
