﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class BevTec_Products : System.Web.UI.Page
{
    clsCategories clsCategories;
    protected void Page_Load(object sender, EventArgs e)
    {

        if ((Request.QueryString["iCategoryID"] != "") && (Request.QueryString["iCategoryID"] != null))
        {
            int iCategoryID = Convert.ToInt32(Request.QueryString["iCategoryID"]);
            clsCategories = new clsCategories(Convert.ToInt32(Request.QueryString["iCategoryID"]));

            //### Populate the form
            NoProduct.Visible = false;
            popProducts(iCategoryID);
            popCategories();
            popProductDetails();

        }
        else
        {
            popProducts();
            popCategories();
            NoProduct.Visible = false;
        }
    }
    private void popProductDetails()
    {

        LitCateName.Text = clsCategories.strTitle;

    }
    protected void btnAddToCart_Click(object sender, EventArgs e)
    {
        int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);


        clsCart.addToCart(iProductID, 1);
        Response.Redirect("Bevtec-Cart.aspx");
    }


    private void popProducts()
    {
        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");

        int iCount = 0;

        foreach (DataRow dtrProduct in dtProducts.Rows)
        {
            ++iCount;

            if (iCount == 1)
            {
                //dtrProduct["firstProduct"] = "";
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
            }

            if (iCount % 3 == 1 && iCount != 1)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 2)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 0)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if ((iCount % 3 == 0) || (iCount == dtProducts.Rows.Count))
            {
                dtrProduct["divReset"] = "<div class=\"clear\"></div>";
            }

            if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
            {
                dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                dtrProduct["Link"] = "BevTec-Products-listing.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
            }
            else
            {
                dtrProduct["FullPathForImage"] = "img/no-image.png";
            }
        }
        rpProducts.DataSource = dtProducts;
        rpProducts.DataBind();
    }
    private void popProducts(int iCategoryID)
    {
        DataTable dtCategoryProducts = clsProductCategoriesLink.GetProductCategoriesLinkList("iCategoryID='" + iCategoryID + "'", "");

        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");

        DataTable dtNewProductList = new DataTable();

        dtNewProductList = dtProducts.Clone();
        dtNewProductList.Clear();

        int iCount = 0;
        bool containData = false;

        foreach (DataRow dtrCategoryProducts in dtCategoryProducts.Rows)
        {
            containData = true;

            int iProductTotal = dtCategoryProducts.Rows.Count;

            foreach (DataRow dtrProduct in dtProducts.Rows)
            {



                if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
                {
                    dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                    dtrProduct["Link"] = "BevTec-Products-listing.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
                }
                else
                {
                    dtrProduct["FullPathForImage"] = "img/no-image.png";
                }

                if (Convert.ToInt32(dtrCategoryProducts["iProductID"]) == Convert.ToInt32(dtrProduct["iProductID"]))
                {
                    ++iCount;

                    if (iCount == 1)
                    {
                        //dtrProduct["firstProduct"] = "";
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
                    }

                    if (iCount % 3 == 1 && iCount != 1)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if (iCount % 3 == 2)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if (iCount % 3 == 0)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if ((iCount % 3 == 0) || (iCount == iProductTotal))
                    {
                        dtrProduct["divReset"] = "<div class=\"clear\"></div>";
                    }

                    dtNewProductList.ImportRow(dtrProduct);
                }
                else
                {
                    NoProduct.Visible = true;
                }

            }

        }

        if (containData == false)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" ><h2>No products were found matching your selection </h2> </div>";
            rpProducts.DataSource = dtNewProductList;
            rpProducts.DataBind();
            Hidecontent.Visible = false;
        }
        else
        {
            rpProducts.DataSource = dtNewProductList;
            rpProducts.DataBind();
            mandatoryDiv.Visible = false;
           

        }

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (txtSearch.Text == "")
        {

        }
        else
        {


            DataTable dtProducts = clsProducts.GetProductsList("strTitle='" + txtSearch.Text + "'", "");

            int iBestSellerCount = dtProducts.Rows.Count;
            dtProducts.Columns.Add("FullPathForImage");
            dtProducts.Columns.Add("Link");
            dtProducts.Columns.Add("divReset");
            dtProducts.Columns.Add("classForColumn");

            int iCount = 0;

            foreach (DataRow dtrProduct in dtProducts.Rows)
            {
                ++iCount;

                if (iCount == 1)
                {
                    //dtrProduct["firstProduct"] = "";
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
                }

                if (iCount % 3 == 1 && iCount != 1)
                {
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                }

                if (iCount % 3 == 2)
                {
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                }

                if (iCount % 3 == 0)
                {
                    dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                }

                if ((iCount % 3 == 0) || (iCount == dtProducts.Rows.Count))
                {
                    dtrProduct["divReset"] = "<div class=\"clear\"></div>";
                }

                if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
                {
                    dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                    dtrProduct["Link"] = "BevTec-Products-listing.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
                }
                else
                {
                    dtrProduct["FullPathForImage"] = "img/no-image.png";
                }
            }
            if (dtProducts.Rows.Count == 0)
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" ><h2>No products were found matching your selection </h2> </div>";
                rpProducts.DataSource = dtProducts;
                rpProducts.DataBind();
            }
            else
            {
                rpProducts.DataSource = dtProducts;
                rpProducts.DataBind();
                mandatoryDiv.Visible = false;
                Hidecontent.Visible = false;

            }
        }






    }
    private void popCategories()
    {
        DataTable dtCategories = clsCategories.GetCategoriesList("", "strTitle ASC");

        int iBestSellerCount = dtCategories.Rows.Count;
        dtCategories.Columns.Add("FullPathForImage");
        dtCategories.Columns.Add("Link");
        dtCategories.Columns.Add("divReset");
        dtCategories.Columns.Add("classForColumn");

        int iCount = 0;

        foreach (DataRow dtrCategories in dtCategories.Rows)
        {
            ++iCount;

            if (iCount == 1)
            {
                //dtrProduct["firstProduct"] = "";
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
            }

            if (iCount % 3 == 1 && iCount != 1)
            {
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 2)
            {
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 0)
            {
                dtrCategories["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if ((iCount % 3 == 0) || (iCount == dtCategories.Rows.Count))
            {
                dtrCategories["divReset"] = "<div class=\"clear\"></div>";
            }

            if (!(dtrCategories["strMasterImage"].ToString() == "") || (dtrCategories["strMasterImage"] == null) || (dtrCategories["strPathToImages"] == null))
            {
                dtrCategories["FullPathForImage"] = "Categories/" + dtrCategories["strPathToImages"] + "/crop_" + dtrCategories["strMasterImage"];

                dtrCategories["Link"] = "BevTec-Products-listing.aspx?iCategoryID=" + dtrCategories["iCategoryID"].ToString();
            }
            else
            {
                dtrCategories["FullPathForImage"] = "img/no-image.png";
            }
        }
        rpCategories.DataSource = dtCategories;
        rpCategories.DataBind();
    }




}