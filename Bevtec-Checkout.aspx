﻿<%@ Page Title="Bevtec - Checkout" Language="C#" AutoEventWireup="true" CodeFile="BevTec-Checkout.aspx.cs" Inherits="Bevtec_Checkout" EnableEventValidation="true" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />

    <link rel="shortcut icon" href="images/bevtec-images/Bevtec-Logo-150.png" />

    <link rel='stylesheet' href='css/cform.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/settings.css' type='text/css' media='all' />

    <link rel='stylesheet' href='css/wc-quantity-increment.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pagenavi-css.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/colorbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/font-awesome-animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-headers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-footers.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jt-content-elements.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/slim-menu.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/dynamic-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/js_composer.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/jquery.pwstabs.min.css' type='text/css' media='all' />

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans%3Alighter%2Cnormal%2Csemi-bold%2Cbold%7CMontserrat%3Alighter%2Cnormal%2Csemi-bold%2Cbold&amp;ver=4.3.1' type='text/css' media='all' />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://defatch-demo.com/themes/juster/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->
    <script lang="javascript" type="text/javascript">
        function validateEmailAddress(target) {
            if (target.value != '' || target.value != null) {
                var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (target.value.length > 0) {
                    if (target.value.match(emailExpression)) {
                        document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageValid';
                    document.getElementById("<%= hfCanSave.ClientID %>").value = "true";
                }
                else {
                    document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                    document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
                }
            }
            else {
                document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
            }
        }
        else {
            document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
            document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
        }
    }

    function showInProgress(target) {
        if (target.value != '' || target.value != null) {
            target.className = 'validationImageInProgress';
        }
    }
    </script>
</head>
<body class="page page-id-8 page-template-default  jt_main_content woocommerce-checkout woocommerce-page wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
    <form id="form1" runat="server">
        <div>
            <div class="wrapper">
                <!-- Wrapper -->
                <div class="container-fluid padding-zero jt-main-banner-holder">
                    <div class="jt-page-header  jt-blog-page">
                        <div class="jt-banner-overlay" style="background: ;"></div>
                        <header class=" sticky-nav sticky-rev">
                            <nav class="navbar navbar-default navbar-static-top">
                                <div class="">
                                    <div class="navbar-header">
                                        <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                            <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Bevtec" />
                                        </a>
                                        <a href="index.html" rel="home" class="default navbar-logo retina-logo">
                                            <img src="images/bevtec-images/Bevtec-Logo-150.png" alt="Bevtec" />
                                        </a>
                                    </div>
                                    <div class="menu-metas navbar-default navbar-right">
                                    </div>
                                    <div id="main-nav" class="collapse navbar-collapse menu-main-menu-container">
                                        <ul id="menu-create-menu" class="nav navbar-nav navbar-right jt-main-nav">
                                            <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-AboutUs.aspx">About Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-Categories.aspx">Categories</a>
                                            </li>

                                            <li class="menu-item ">
                                                <a href="BevTec-Products.aspx">Products</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Blog.aspx">Blog</a>
                                            </li>

                                            <li class="menu-item ">
                                                <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Cart.aspx">
                                                    <img src="images/icons/shop-cart.png" alt=" " />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- Slim Menu -->
                            <div class="hidden-big-screen ">
                                <div class=" sticky-nav sticky-rev jt-slim-top">
                                    <a href="index.html" rel="home" class="default navbar-logo default-logo">
                                        <img src="upload/logo-dark.png" alt="Juster" />
                                    </a>
                                    <div class="menu-main-menu-container">
                                        <ul id="menu-main-menu" class="nav navbar-nav navbar-right jt-agency-menu-list slimmenu jt-slimmenu jt-top-slimmenu">
                                            <li class="megamenu menu-column-three mega-right-align menu-item current-menu-ancestor menu-item-has-children "><a title="Home" href="BevTec-Home.aspx" class="dropdown-toggle sub-collapser">Home <b class="caret"></b></a>

                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-AboutUs.aspx">About Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="BevTec-Categories.aspx">Categories</a>
                                            </li>

                                            <li class="menu-item ">
                                                <a href="BevTec-Products.aspx">Products</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Blog.aspx">Blog</a>
                                            </li>

                                            <li class="menu-item ">
                                                <a href="BevTec-Contact-Us.aspx">Contact Us</a>
                                            </li>
                                            <li class="menu-item ">
                                                <a href="Bevtec-Cart.aspx">
                                                    <img src="images/icons/shop-cart.png" alt=" " />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /Slim Menu -->
                        </header>

                        <div class="slider-container jt-vintage-banner jt-vint-small-banner" style="background: url('upload/Shop.jpg'); background-size: cover;">
                        </div>
                        <div class="jt-page-banner jt-main-bantext-1null">
                            <h2 class="page_heading">Checkout</h2>
                            <div class="jt-breadcrumbs">
                                <nav class="breadcrumb-trail breadcrumbs">
                                    <ul class="trail-items">
                                        <li class="trail-item trail-begin">
                                            <a href="index.html" rel="home"><span>Home</span></a>
                                        </li>
                                        <li class="trail-item trail-end">
                                            <span>Checkout</span>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="jt-banner-graphic">
                            <div class="jt-ban-icon faa-burst animated fa-fast"></div>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1100ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="1s" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="900ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="800ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="700ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="600ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="500ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="400ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="300ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                            <span class="animate-plus" data-animations="fadeIn" data-animation-delay="200ms" data-animation-when-visible="true" data-animation-reset-offscreen="true"></span>
                        </div>
                    </div>
                </div>

                <div class="container padding-zero jt_content_holder">
                    <div class="entry-content page-container content-ctrl">
                        <!-- Main Container -->
                        <div class="container">
                            <!-- Container -->
                            <div class="container main-content-center">
                                <div class="main-content col-lg-12 padding-zero">
                                    <article id="post-8" class="post-8 page type-page status-publish hentry">
                                        <div class="entry-content">
                                            <div class="woocommerce">
                                                <div name="checkout" method="post" class="checkout woocommerce-checkout" action="#" enctype="multipart/form-data">
                                                    <div class="col2-set" id="customer_details">
                                                        <div class="col-lg-9 padding-left-zero checkout-address-details">
                                                            <div class="woocommerce-billing-fields">

                                                                <h3>Billing Details</h3>
                                                                <div class="controlDiv">
                                                                    <div class="imageHolderCommonDiv">
                                                                        <div class="validationImageMandatory"></div>
                                                                    </div>
                                                                    <p class="form-row form-row form-row-first validate-required" id="billing_first_name_field">
                                                                        <label for="billing_first_name" class="">
                                                                            First Name
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                        <asp:TextBox ID="txtContactPerson" CssClass="input-text" onblur="setValid(this)" runat="server"></asp:TextBox>
                                                                    </p>
                                                                      <br class="clearingSpacer" />
                                                                </div>
                                                                <div class="controlDiv">
                                                                    <div class="imageHolderCommonDiv">
                                                                        <div class="validationImageMandatory"></div>
                                                                    </div>
                                                                    <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                        <label for="billing_last_name" class="">
                                                                            Last Name
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                        <asp:TextBox ID="txtLastname" CssClass="input-text" onblur="setValid(this)" runat="server"></asp:TextBox>
                                                                    </p>
                                                                      <br class="clearingSpacer" />
                                                                </div>

                                                                <p class="form-row form-row form-row-wide" id="billing_company_field">
                                                                    <label for="billing_company" class="">Company Name</label>
                                                                    <asp:TextBox ID="txtCompanyName" CssClass="input-text" runat="server"></asp:TextBox>
                                                                </p>

                                                                <div class="controlDiv">
                                                                    <div class="imageHolderCommonDiv">
                                                                        <div id="divImageHolder" runat="server" class="validationImageMandatory">
                                                                            <abbr class="required" title="required">*</abbr>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row form-row form-row-first validate-required validate-email" id="billing_email_field">
                                                                        <label for="billing_email" class="">
                                                                            Email Address
                                                                       
                                                                        </label>
                                                                        <div class="fieldDiv">
                                                                            <asp:TextBox ID="txtEmailAddress" CssClass="input-text" onblur="validateEmailAddress(this)" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        <br class="clearingSpacer" />
                                                                        <asp:HiddenField ID="hfCanSave" runat="server" Value="false" />
                                                                    </div>
                                                                </div>

                                                                <p class="form-row form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                                                                    <label for="billing_phone" class="">
                                                                        Phone
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                    <asp:TextBox ID="txtContactNumber" CssClass="input-text" runat="server"></asp:TextBox>
                                                                </p>
                                                                <div class="clear"></div>
                                                                <p class="form-row form-row form-row-wide address-field validate-required" id="billing_address_1_field">
                                                                    <label for="billing_address_1" class="">
                                                                        Address
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                    <asp:TextBox ID="txtPhysicalAddress" CssClass="input-text" runat="server"></asp:TextBox>

                                                                </p>

                                                                <p class="form-row form-row form-row-wide address-field" id="billing_address_2_field">
                                                                    <input type="text" class="input-text " name="billing_address_2" id="billing_address_2" placeholder="Apartment, suite, unit etc. (optional)" value="" />
                                                                </p>

                                                                <p class="form-row form-row form-row-wide address-field validate-required" id="billing_city_field">
                                                                    <label for="billing_city" class="">
                                                                        Town / City
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                     <asp:TextBox ID="txtBulling" CssClass="input-text" runat="server" onblur="setValid(this)"></asp:TextBox>
                                                                </p>

                                                                <p class="form-row form-row form-row-first address-field validate-required validate-state" id="billing_state_field">
                                                                    <label for="billing_state" class="">
                                                                        State / County
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                    <asp:TextBox ID="txtState" CssClass="input-text" runat="server" onblur="setValid(this)" ></asp:TextBox>
                                                                </p>

                                                                <p class="form-row form-row form-row-last address-field validate-required validate-postcode" id="billing_postcode_field">
                                                                    <label for="billing_postcode" class="">
                                                                        Postcode / Zip
                                                                        <abbr class="required" title="required">*</abbr></label>
                                                                    <input type="text" class="input-text " name="billing_postcode" id="billing_postcode" placeholder="Postcode / Zip" value="" />
                                                                </p>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="woocommerce-shipping-fields">

                                                                <p class="form-row form-row notes" id="order_comments_field">
                                                                    <label for="order_comments" class="">Order Notes</label>
                                                                  <asp:TextBox ID="txtOrder" CssClass="input-text" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                </p>

                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3 padding-zero your-order-design"></div>
                                                        <div class="col-lg-3 padding-zero your-order">
                                                            <h3 id="order_review_heading">Your order</h3>
                                                            <div id="order_review" class="woocommerce-checkout-review-order">
                                                                <table class="shop_table woocommerce-checkout-review-order-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="product-name">Product</th>
                                                                            <th class="product-total">Total</th>
                                                                        </tr>
                                                                    </thead>


                                                                    <tbody>
                                                                        <asp:Literal runat="server" ID="litGridData"></asp:Literal>
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr class="order-total">
                                                                            <th>Total</th>
                                                                            <td><strong><span id="spanTotal" runat="server" style="float: right; margin-right: 17px;"></span></strong></td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>

                                                                <div id="payment" class="woocommerce-checkout-payment">
                                                                    <div class="form-row place-order">
                                                                        <br />
                                                                        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="button alt" Text="Place Order" OnClick="lnkbtnSave_Click" />
                                                                    </div>
                                                                </div>
                                                                <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                                                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </article>

                                </div>
                                <!-- end main-content -->

                            </div>
                        </div>
                        <!-- End Container -->
                    </div>
                </div>
                <!-- End Main Container -->


                <div class="container-fluid padding-zero foot-ctrl">
                    <footer class="jt-footer-style-two   ">

                        <div class="col-lg-8 col-md-8 col-sm-8 ">
                            <!-- Footer Widgets -->
                            <div class="container">
                                <p>
                                    <div class="" style="height: 20px;"></div>
                                    <img src="images/bevtec-images/Untitled-1.png" alt="" />
                                    <div class="" style="height: 20px;"></div>
                                    <ul class="jt-social-one social_eight_center">
                                        <li><a href="http://www.facebook.com/" target="_blank">facebook</a></li>
                                        <li><a href="http://www.twitter.com/" target="_blank">twitter</a></li>
                                        <li><a href="http://www.plus.google.com/" target="_blank">google +</a></li>
                                        <li><a href="http://www.linkedin.com/" target="_blank">linkedin</a></li>
                                        <li><a href="http://www.instagram.com/" target="_blank">instagram</a></li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                        <!-- Copyright Widget Area -->

                        <div class="col-lg-3 col-md-3 col-sm-3 ">
                            <div class="widget">
                                <div class="jt-widget-content">
                                    <div id="widget-keep-in-touch-2" class="widget widget-custom">
                                        <h3 class="widget-title">Keep in Touch</h3>
                                        <ul class="jt-widget-address">
                                            <li class="jt-add-icon jt-add-li">
                                                <i class="pe-7s-map-2"></i>
                                                <span>Bev Tec (Pty) Ltd Unit 6 Greenway Park Flower Close Tunney Ext 9 Germiston 1401 PO Box 75157 Gardenview 2047 </span>
                                            </li>
                                                <li class="jt-add-icon jt-mail-li">
                                                <i class="pe-7s-mail"></i>
                                                 <span><a href="mailto:info@bevtec.co.za?subject=Website Enquiry">info@bevtec.co.za</a></span>
                                            </li>
                                            <li class="jt-add-icon jt-phone-li">
                                                <i class="pe-7s-call"></i>
                                                <span>+27  83 709 6554</span>
                                            </li>
                                        
                                            <li class="jt-add-icon jt-fax-li">
                                                <i class="pe-7s-call"></i>
                                                <span>+27  83 785 4996</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- end widget -->
                                </div>
                            </div>
                        </div>
                        <!-- Footer Copyrights -->
                    </footer>
                </div>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='js/jquery/jquery.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/frontend/add-to-cart.min.js'></script>
    <script type='text/javascript' src='js/wc-quantity-increment.min.js'></script>
    <script type='text/javascript' src='js/vendors/woocommerce-add-to-cart.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>

    <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            // Sticky Navbar
            $(".sticky-nav").sticky({
                topSpacing: 0
            });
        });
    </script>
    <script type='text/javascript' src='js/jquery.form.min.js'></script>
    <script type='text/javascript' src='js/jquery-blockui/jquery.blockUI.min.js'></script>
    <script type='text/javascript' src='js/frontend/woocommerce.min.js'></script>
    <script type='text/javascript' src='js/jquery-cookie/jquery.cookie.min.js'></script>
    <script type='text/javascript' src='js/frontend/cart-fragments.min.js'></script>
    <script type='text/javascript' src='js/woocompare.js'></script>
    <script type='text/javascript' src='js/jquery.colorbox-min.js'></script>
    <script type='text/javascript' src='js/jquery.selectBox.min.js'></script>

    <script type='text/javascript' src='js/jquery.yith-wcw.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type='text/javascript' src='js/owl.carousel.min.js'></script>
    <script type='text/javascript' src='js/animate-plus.min.js'></script>
    <script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/scripts.js'></script>
    <script type='text/javascript' src='js/lib/waypoints/waypoints.min.js'></script>
    <script type='text/javascript' src='js/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.sticky.js'></script>
    <script type='text/javascript' src='js/photography/gridgallery/modernizr.custom.js'></script>
    <script type='text/javascript' src='js/juster-banner/juster-banner-effect.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.slimmenu.min.js'></script>
    <script type='text/javascript' src='js/slimmenu/jquery.easing.min.js'></script>
    <script type='text/javascript' src='js/dynamic-js.js'></script>
    <script type='text/javascript' src='js/js_composer_front.js'></script>
    <script type='text/javascript' src='js/wow.min.js'></script>
    <script type='text/javascript' src='js/dragcarousel/draggabilly.pkgd.min.js'></script>
    <script type='text/javascript' src='js/jquery.pwstabs.min.js'></script>
    <script type='text/javascript' src='js/jquery.counterup.min.js'></script>
    <script type='text/javascript' src='js/tiltfx.js'></script>
    <script type='text/javascript' src='js/ellipsis.js'></script>
    <script type='text/javascript' src='js/chaffle.js'></script>
    <script type='text/javascript' src='js/contact-form.js'></script>
</body>
</html>
