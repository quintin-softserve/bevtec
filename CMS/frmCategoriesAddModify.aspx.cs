using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using SD = System.Drawing;
using System.Drawing.Drawing2D;

public partial class CMS_frmCategoriesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCategories clsCategories;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../frmAdminLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
            DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
            //### If the iCategorieID is passed through then we want to instantiate the object with that iCategorieID
            if ((Request.QueryString["iCategoryID"] != "") && (Request.QueryString["iCategoryID"] != null))
            {
                clsCategories = new clsCategories(Convert.ToInt32(Request.QueryString["iCategoryID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                PopCategories(0);

                clsCategories = new clsCategories();
            }
            Session["clsCategories"] = clsCategories;
        }
        else
        {
            clsCategories = (clsCategories)Session["clsCategories"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCategoriesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (treeCategories.SelectedNode == null)
        {
            bCanSave = false;
        }

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Categorie added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Categorie not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        //lstCategoryType.SelectedValue = clsCategories.iCategoryTypeID.ToString();
        txtTitle.Text = clsCategories.strTitle;
        txtDescription.Text = clsCategories.strDescription;
        PopCategories(clsCategories.iLevelID);
        hdniCategoryID.Value = clsCategories.iLevelID.ToString();

        cbIsFeatured.Checked = (clsCategories.bIsFeatured);

        if (!string.IsNullOrEmpty(clsCategories.strPathToImages))
        {
            lblUniquePath.Text = clsCategories.strPathToImages;
            getList(clsCategories.strPathToImages);
            Session["strUploadFileName"] = clsCategories.strMasterImage;
            //### Set Current Master Image
            List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
            try
            {
                foreach (string strImageFileName in lstImagesFileNames)
                {
                    if (strImageFileName == clsCategories.strMasterImage)
                    {
                        RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                        rdbMasterImage.Checked = true;
                        break;
                    }
                }
            }
            catch { }
        }
    }

    /// <summary>
    /// Populate the Tree view with the category types
    /// </summary>
    private void PopCategories(int clsCategoriesiCategoryID)
    {
        treeCategories.Nodes.Clear();
        TreeNode Mainnode = new TreeNode();
        bool bIsSelected = false;
        Mainnode.Value = "0";
        Mainnode.Text = "Top level category";
        if (clsCategoriesiCategoryID == 0)
        {
            Mainnode.Selected = true;
        }
        treeCategories.Nodes.Add(Mainnode);

        DataTable DT = new DataTable();
        DT = clsCategories.GetCategoriesList("iLevelID = 0", "strTitle");
        foreach (DataRow r in DT.Rows)
        {
            int iCategoryID = 0;
            iCategoryID = int.Parse(r["iCategoryID"].ToString());
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iCategoryID"].ToString());
            if (clsCategoriesiCategoryID == int.Parse(r["iCategoryID"].ToString()))
            {
                node.Selected = true;
            }

            //### Now we call the PopChildCategories method to build the sub categories
            PopChildCategories(ref node, int.Parse(r["iCategoryID"].ToString()), clsCategoriesiCategoryID, ref bIsSelected);

            if (bIsSelected)
            {
                node.Expanded = true;

                FeaturedCategory.Visible = true;
            }
            else
            {
                node.Expanded = false;
                FeaturedCategory.Visible = true;
            }
            treeCategories.Nodes.Add(node);
        }
    }

    /// <summary>
    /// Populate the Subcategories for the Category Treeview.
    /// This Method will call itself to build the Subcategory of the current Subcategory
    /// </summary>
    /// <param name="ChildNode">The current Top level category</param>
    /// <param name="iCategoryID">The ID of the Category</param>
    /// <param name="clsCategoriesiCategoryID">The ID of the clsCategories.iCategoryID</param>
    private void PopChildCategories(ref TreeNode ChildNode, int iCategoryID, int clsCategoriesiCategoryID, ref bool bIsSelected)
    {
        DataTable DT = new DataTable();
        DT = clsCategories.GetCategoriesList("iLevelID = " + iCategoryID.ToString(), "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iCategoryID"].ToString());
            if (clsCategoriesiCategoryID == int.Parse(r["iCategoryID"].ToString()))
            {
                node.Selected = true;
                bIsSelected = true;
            }
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            ChildNode.ChildNodes.Add(node);

            //### Now we call this methode again to build the subsubcategories
            PopChildCategories(ref node, int.Parse(r["iCategoryID"].ToString()), clsCategoriesiCategoryID, ref bIsSelected);
        }
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update      
        clsCategories.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCategories.iAddedBy = clsUsers.iUserID;
        clsCategories.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCategories.iEditedBy = clsUsers.iUserID;
        clsCategories.strPathToImages = lblUniquePath.Text;
        clsCategories.strMasterImage = clsCategories.strMasterImage = GetMainImagePath(dlImages);

        clsCategories.bIsFeatured = (cbIsFeatured.Checked);
        
        int iLevelID;

        if (treeCategories.Nodes.Count != 0)
        {
            if (treeCategories.SelectedNode != null)
            {
                iLevelID = int.Parse(treeCategories.SelectedNode.Value);
            }
            else
            {
                iLevelID = 0;
            }
        }
        else
        {
            iLevelID = 0;
        }

        clsCategories.iLevelID = iLevelID;
        //clsCategories.iCategoryTypeID = Convert.ToInt32(lstCategoryType.SelectedValue.ToString());
        clsCategories.strTitle = txtTitle.Text;
        clsCategories.strDescription = txtDescription.Text;

        clsCategories.Update();

        //### Go back to view page
        Response.Redirect("frmCategoriesView.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 3;

    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Categories";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }

    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string strImageName = Session["WorkingImage"].ToString();

        //### Validate a cropped area has been selected
        if (W.Value != "" && W.Value != "0" && H.Value != "" && H.Value != "0" && X.Value != "" && Y.Value != "")
        {
            int w = Convert.ToInt32(W.Value);
            int h = Convert.ToInt32(H.Value);
            int x = Convert.ToInt32(X.Value);
            int y = Convert.ToInt32(Y.Value);

            byte[] CropImage = Crop(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName, w, h, x, y);

            using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
            {
                ms.Write(CropImage, 0, CropImage.Length);
                using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                {
                    string SaveTo = strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + "crop_" + strImageName;
                    CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                }
            }

            CopyAndResizePic(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName);
            getList(lblUniquePath.Text);
        }
        else
        {
            //### Show message
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "Select an area you wish to crop.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "jCropUnique", "jCrop();", true);
        }
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        try
        {
            using (SD.Image OriginalImage = SD.Image.FromFile(Img))
            {
                using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                    using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);

                        MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);

                        return ms.GetBuffer();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw (Ex);
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\Categories" + iCount) == true)
        {
            iCount++;
        }
        return "Categories" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            Session["WorkingImage"] = FileUpload.FileName;

            clsCommonFunctions.ResizeImage(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + Session["WorkingImage"], strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" +
                Session["WorkingImage"], 500, 500, true);

            imgCrop.ImageUrl = "../Categories" + "/" + strUniquePath + "/" + strUploadFileName;
            ModalPopupExtenderCrop.Show();
            Session["WorkingImage"] = FileUpload.FileName;
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {
            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath.Replace(strFileName, "crop_") + strFileName, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 134, 134, false);

        }
        catch (Exception ex) { }
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iCategoryID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iCategoryID"]))
                iCategoryID = Request.QueryString["iCategoryID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}