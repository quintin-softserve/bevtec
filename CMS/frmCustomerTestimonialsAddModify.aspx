<%@ Page Title="Customer Testimonials" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmCustomerTestimonialsAddModify.aspx.cs" Inherits="CMS_frmCustomerTestimonialsAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Customer Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCustomerName" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
      <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Job Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtJobTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Testimonial:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTestimonial" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,450)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
