﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

public partial class CMS_frmProductsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtProductsList;
    //List<clsProducts> glstProducts;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            
        }

        PopulateFormData();
    
    }
    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle +' '+ strDescription +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtProductsList = clsProducts.GetProductsList(FilterExpression, "");

        Session["dtProductsList"] = dtProductsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmProductsAddModify.aspx");
    }

    private void PopulateFormData()
    {
        //### Populate datagrid using clsProductsList object
        try
        {
            dtProductsList = new DataTable();

            if (Session["dtProductsList"] == null)
                dtProductsList = clsProducts.GetProductsList("","strTitle ASC");
            else
                dtProductsList = (DataTable)Session["dtProductsList"];

            dgrGrid.DataSource = dtProductsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ProductsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ProductsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ProductsView_CurrentPageIndex"];
                }
            }
            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iProductID = int.Parse((sender as LinkButton).CommandArgument);

        clsProducts.Delete(iProductID);

        Session["dtProductsList"] = null;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ProductsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtProductsList = new DataTable();

        if (Session["dtProductsList"] == null)
            dtProductsList = clsProducts.GetProductsList();
        else
            dtProductsList = (DataTable)Session["dtProductsList"];

        DataView dvTemp = dtProductsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtProductsList = dvTemp.ToTable();
        Session["dtProductsList"] = dtProductsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iProductID
            int iProductID = 0;
            iProductID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmProductsAddModify.aspx?action=edit&iProductID=" + iProductID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     //public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     //{
     //     try
     //     {
     //        //### Add hyperlink to datagrid item
     //        HyperLink lnkDeleteItem = (HyperLink)sender;
     //        DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

     //        //### Get the iProductID
     //        int iProductID = 0;
     //        iProductID = int.Parse(dgrItemDelete.Cells[0].Text);

     //        //### Add attributes to delete link
     //        lnkDeleteItem.CssClass = "dgrDeleteLink";
     //        lnkDeleteItem.Attributes.Add("href", "frmProductsView.aspx?action=delete&iProductID=" + iProductID);
     //      }
     //      catch (Exception ex)
     //      {
     //            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
     //      }
     //} 

       #region Products FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strDescription +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtProducts = clsProducts.GetProductsList(FilterExpression, "");
        List<string> glstProducts = new List<string>();

        if (dtProducts.Rows.Count > 0)
        {
            foreach (DataRow dtrProducts in dtProducts.Rows)
            {
                glstProducts.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrProducts["strTitle"].ToString() +' '+"" + dtrProducts["strDescription"].ToString() +' '+"" + dtrProducts["strMasterImage"].ToString(), dtrProducts["iProductID"].ToString()));
            }
        }
        else
            glstProducts.Add("No Products Available.");
        strReturnList = glstProducts.ToArray();
        return strReturnList;
    }

    #endregion
}

