﻿<%@ Page Title="CMS Users" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmUsersAddModify.aspx.cs" Inherits="CMS_frmUsersAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script lang="javascript" type="text/javascript">

    //### Password check
    function passwordCheck() {
        if (document.getElementById("<%=txtPassword.ClientID%>").value != document.getElementById("<%=txtConfirmPassword.ClientID%>").value) {
            alert('Your passwords do not match.\nYour passwords have been reset.')
            document.getElementById("<%=txtPassword.ClientID%>").value = ""
            document.getElementById("<%=txtConfirmPassword.ClientID%>").value = ""
            document.getElementById("<%=txtPassword.ClientID%>").focus()
        }
    }

    function validateEmailAddress(target) {
        if (target.value != '' || target.value != null) {
            var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            if (target.value.length > 0) {
                if (target.value.match(emailExpression)) {
                    document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageValid';
                    document.getElementById("<%= hfCanSave.ClientID %>").value = "true";
                }
                else {
                    document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                    document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
                }
            }
            else {
                document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
                document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
            }
        }
        else {
            document.getElementById("<%= divImageHolder.ClientID %>").className = 'validationImageInvalid';
            document.getElementById("<%= hfCanSave.ClientID %>").value = "false";
        }
    }

    function showInProgress(target) {
        if (target.value != '' || target.value != null) {
            target.className = 'validationImageInProgress';
        }
    }
    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div class="labelDiv">Name:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtFullName" runat="server" CssClass="roundedCornerTextBox" onblur="setValid(this)" onKeyUp="return SetMaxLength(this,50)"/>
        </div>
        <br class="clearingSpacer" />
    </div>
       <div class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div class="labelDiv">Surname:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtSurname" runat="server" CssClass="roundedCornerTextBox" onblur="setValid(this)" onKeyUp="return SetMaxLength(this,50)"/>
        </div>
        <br class="clearingSpacer" />
    </div>
    <div class="controlDiv">
        <div class="imageHolderCommonDiv"><div id="divImageHolder" runat="server" class="validationImageMandatory"></div></div>
        <div class="labelDiv">Email:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="roundedCornerTextBox" onblur="validateEmailAddress(this)" onKeyUp="return SetMaxLength(this,80)"/>
        </div>
        <br class="clearingSpacer" />
        <asp:HiddenField  ID="hfCanSave" runat="server" Value="false"/>
    </div>

    <div id="divPassword" runat="server" class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div id="passwordLabel" class="labelDiv">Password:</div>
        <div id="passwordField" class="fieldDiv">
            <asp:TextBox ID="txtPassword" runat="server" CssClass="roundedCornerTextBox" TextMode="Password" onblur="setValid(this)" onKeyUp="return SetMaxLength(this,30)"/>
        </div>
        <br class="clearingSpacer" />
    </div>

    <div id="divConfirmPassword" runat="server" class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div id="confirmPasswordLabel" class="labelDiv">Confirm Password:</div>
        <div id="confirmPasswordField" class="fieldDiv">
            <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="roundedCornerTextBox" TextMode="Password" onblur="passwordCheck(); setValid(this);" onKeyUp="return SetMaxLength(this,30)"/>
        </div>
        <br class="clearingSpacer" />
    </div>

    <div class="Line"></div>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />        
    </div>

</asp:Content>