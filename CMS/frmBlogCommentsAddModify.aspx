<%@ Page Title="BlogComments" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmBlogCommentsAddModify.aspx.cs" Inherits="CMS_frmBlogCommentsAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtName" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Email Address:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Phone Number:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,18)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Message:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMessage" runat="server" CssClass="roundedCornerTextBoxMultiLine5" onKeyUp="return SetMaxLength(this,5000)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Display Blog Comment  </div>
         <div class="fieldDiv" style="margin-top: 11px;">
             Check Box to display Blog Comment <asp:CheckBox ID="CheckDesplay" runat="server" />
         </div>
         <br class="clearingSpacer" />
     </div>

    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
