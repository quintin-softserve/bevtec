using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCustomerTestimonialsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCustomerTestimonials clsCustomerTestimonials;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iCustomerTestimonialID is passed through then we want to instantiate the object with that iCustomerTestimonialID
            if ((Request.QueryString["iCustomerTestimonialID"] != "") && (Request.QueryString["iCustomerTestimonialID"] != null))
            {
                clsCustomerTestimonials = new clsCustomerTestimonials(Convert.ToInt32(Request.QueryString["iCustomerTestimonialID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCustomerTestimonials = new clsCustomerTestimonials();
            }
            Session["clsCustomerTestimonials"] = clsCustomerTestimonials;
        }
        else
        {
            clsCustomerTestimonials = (clsCustomerTestimonials)Session["clsCustomerTestimonials"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCustomerTestimonialsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtCustomerName, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTestimonial, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CustomerTestimonial added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CustomerTestimonial not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtCustomerName.Text = "";
        clsValidation.SetValid(txtCustomerName);
        txtTestimonial.Text = "";
        clsValidation.SetValid(txtTestimonial);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtCustomerName.Text = clsCustomerTestimonials.strCustomerName;
         txtTestimonial.Text = clsCustomerTestimonials.strTestimonial;
         txtJobTitle.Text = clsCustomerTestimonials.strJobdesc;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCustomerTestimonials.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomerTestimonials.iAddedBy = clsUsers.iUserID;
        clsCustomerTestimonials.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomerTestimonials.iEditedBy = clsUsers.iUserID;
        clsCustomerTestimonials.strCustomerName = txtCustomerName.Text;
        clsCustomerTestimonials.strJobdesc = txtJobTitle.Text;
        clsCustomerTestimonials.strTestimonial = txtTestimonial.Text;
        clsCustomerTestimonials.bIsApproved = true;

        clsCustomerTestimonials.Update();

        Session["dtCustomerTestimonialsList"] = null;

        //### Go back to view page
        Response.Redirect("frmCustomerTestimonialsView.aspx");
    }

    #endregion
}