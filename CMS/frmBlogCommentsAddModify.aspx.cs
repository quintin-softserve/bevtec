using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmBlogCommentsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsBlogComments clsBlogComments;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iBlogCommentID is passed through then we want to instantiate the object with that iBlogCommentID
            if ((Request.QueryString["iBlogCommentID"] != "") && (Request.QueryString["iBlogCommentID"] != null))
            {
                clsBlogComments = new clsBlogComments(Convert.ToInt32(Request.QueryString["iBlogCommentID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsBlogComments = new clsBlogComments();
            }
            Session["clsBlogComments"] = clsBlogComments;
        }
        else
        {
            clsBlogComments = (clsBlogComments)Session["clsBlogComments"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmBlogCommentsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtName, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtPhoneNumber, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtMessage, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">BlogComment added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - BlogComment not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtName.Text = "";
        clsValidation.SetValid(txtName);
        txtEmailAddress.Text = "";
        clsValidation.SetValid(txtEmailAddress);
        txtPhoneNumber.Text = "";
        clsValidation.SetValid(txtPhoneNumber);
        txtMessage.Text = "";
        clsValidation.SetValid(txtMessage);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtName.Text = clsBlogComments.strName;
         txtEmailAddress.Text = clsBlogComments.strEmailAddress;
         txtPhoneNumber.Text = clsBlogComments.strPhoneNumber;
         txtMessage.Text = clsBlogComments.strMessage;
        if (clsBlogComments.bDoNotDisplay == true)
        {
            CheckDesplay.Checked = true;
        }
        else
        {
            CheckDesplay.Checked = false;
        }
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsBlogComments.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogComments.iAddedBy = clsUsers.iUserID;
        clsBlogComments.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogComments.iEditedBy = clsUsers.iUserID;
        clsBlogComments.strName = txtName.Text;
        clsBlogComments.strEmailAddress = txtEmailAddress.Text;
        clsBlogComments.strPhoneNumber = txtPhoneNumber.Text;
        clsBlogComments.strMessage = txtMessage.Text;
        clsBlogComments.bDoNotDisplay = CheckDesplay.Checked;

        clsBlogComments.Update();

        Session["dtBlogCommentsList"] = null;

        //### Go back to view page
        Response.Redirect("frmBlogCommentsView.aspx");
    }

    #endregion
}