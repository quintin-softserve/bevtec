<%@ Page Title="Colours" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmColoursAddModify.aspx.cs" Inherits="CMS_frmColoursAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory"></div>
        </div>
        <div class="labelDiv">Title:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" onblur="setValid(this);" />
        </div>
        <br class="clearingSpacer" />
    </div>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory"></div>
        </div>
        <div class="labelDiv">Colour Code:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtColourCode" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" onblur="setValid(this);" />
        </div>
        <br class="clearingSpacer" />
    </div>

    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
    </div>
</asp:Content>
