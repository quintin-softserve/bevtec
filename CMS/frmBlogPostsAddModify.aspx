<%@ Page Title="BlogPosts" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmBlogPostsAddModify.aspx.cs" Inherits="CMS_frmBlogPostsAddModify" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" lang="javascript">
        function CheckOnOff(rdoId, gridName) {
            var rdo = document.getElementById(rdoId);
            /* Getting an array of all the INPUT controls on the form.*/
            var rdo = document.getElementById(rdoId);
            var all = document.getElementsByTagName("input");
            for (i = 0; i < all.length; i++) {
                /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
                if (all[i].type == "radio" && all[i].id != rdo.id) {
                    var count = all[i].id.indexOf(gridName);
                    if (count != -1) {
                        all[i].checked = false;
                    }
                }
            }
            rdo.checked = true; /* Finally making the clicked radio button CHECKED */
        }
    </script>
    <link type="text/css" rel="Stylesheet" href="scripts/jquery.Jcrop.css" />
    <script type="text/javascript" src="scripts/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.Jcrop.js"></script>
    <script lang="javascript" type="text/javascript">
        function jCrop() {
            jQuery('#<%=imgCrop.ClientID%>').Jcrop({
                onSelect: storeCoords,
                aspectRatio: 1 / 1,
                onChange: storeCoords,
                onSelect: storeCoords,
                allowMove: true,
                minSize: [134, 134],
            });
        }

        jQuery(window).load(jCrop);

        function storeCoords(c) {
            jQuery('#<%=X.ClientID%>').val(c.x);
            jQuery('#<%=Y.ClientID%>').val(c.y);
            jQuery('#<%=W.ClientID%>').val(c.w);
            jQuery('#<%=H.ClientID%>').val(c.h);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>

            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
            </div>

            <%--            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Stock Code:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtStockCode" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,80)" />
                </div>
                <br class="clearingSpacer" />
            </div>--%>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">BlogType:</div>
                <div class="fieldDiv">
                    <asp:DropDownList ID="lstBlogType" runat="server" CssClass="roundedCornerDropDownList" AutoPostBack="true"></asp:DropDownList>
                </div>
                <br class="clearingSpacer" />
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Title:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" onblur="setValid(this);" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Tag Line:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtTagLine" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clearingSpacer" />
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Description:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clearingSpacer" />
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Images:</div>
                <div class="fieldDiv">
                    <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" Style="float: left;" Width="350px" onblur="setValid(this);" />
                    <div style="float: left;">
                        <asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" OnClick="btnUpload_Click" Style="margin-left: 5px;" />
                    </div>
                    <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear: both" />
                    <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                <ItemTemplate>
                                    <table cellspacing="5">
                                        <%# Container.DataItem %>
                                    </table>
                                    <div align="center" style="padding-top: 5px;">
                                        <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <b>
                        <asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Video Link:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtVideoLink" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Geo Map Location:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtGeoMapLocation" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clearingSpacer" />
            </div>



            <div class="Line"></div>
            <div class="buttonsRightDiv">
                <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
                <asp:Button ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
                <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>

    </asp:UpdatePanel>
    <asp:Panel ID="pnlCrop" runat="server" CssClass="PopUp">
        <div>
            Please crop the area you would like as a thumbnail.
        </div>
        <div style="margin-top: 10px;" align="center">
            <asp:Image ID="imgCrop" runat="server" />
        </div>
        <br />
        <asp:HiddenField ID="X" runat="server" />
        <asp:HiddenField ID="Y" runat="server" />
        <asp:HiddenField ID="W" runat="server" />
        <asp:HiddenField ID="H" runat="server" />
        <div>
            <img src="images/imgDivider.png" alt="" title="" />
        </div>
        <div class="buttonsRightDiv" style="padding-right: 10px">
            <asp:LinkButton ID="btnCrop" runat="server" CssClass="cropButton" AlternateText="Crop" OnClick="btnCrop_Click" Style="margin-right: 5px;" />
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="backButton" AlternateText="Back" />
        </div>
    </asp:Panel>
    <asp:Button ID="btnPopupCrop" runat="server" Style="display: none" />
    <cc1:modalpopupextender id="ModalPopupExtenderCrop" runat="server" targetcontrolid="btnPopupCrop"
        popupcontrolid="pnlCrop" backgroundcssclass="modalBackground" cancelcontrolid="btnCancel"
        repositionmode="RepositionOnWindowResize" y="0">        
</cc1:modalpopupextender>
</asp:Content>
