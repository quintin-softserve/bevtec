
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsBlogTypesView
/// </summary>
public partial class CMS_clsBlogTypesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtBlogTypesList;

    List<clsBlogTypes> glstBlogTypes;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstBlogTypes"] == null)
            {
                glstBlogTypes = new List<clsBlogTypes>();
                Session["glstBlogTypes"] = glstBlogTypes;
            }
            else
                glstBlogTypes = (List<clsBlogTypes>)Session["glstBlogTypes"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtBlogTypesList = clsBlogTypes.GetBlogTypesList(FilterExpression, "");

        Session["dtBlogTypesList"] = dtBlogTypesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmBlogTypesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsBlogTypesList object
        try
        {
            dtBlogTypesList = new DataTable();

            if (Session["dtBlogTypesList"] == null)
                dtBlogTypesList = clsBlogTypes.GetBlogTypesList();
            else
                dtBlogTypesList = (DataTable)Session["dtBlogTypesList"];

            dgrGrid.DataSource = dtBlogTypesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["BlogTypesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["BlogTypesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["BlogTypesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iBlogTypeID = int.Parse((sender as LinkButton).CommandArgument);

        clsBlogTypes.Delete(iBlogTypeID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["BlogTypesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtBlogTypesList = new DataTable();

        if (Session["dtBlogTypesList"] == null)
            dtBlogTypesList = clsBlogTypes.GetBlogTypesList();
        else
            dtBlogTypesList = (DataTable)Session["dtBlogTypesList"];

        DataView dvTemp = dtBlogTypesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtBlogTypesList = dvTemp.ToTable();
        Session["dtBlogTypesList"] = dtBlogTypesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iBlogTypeID
            int iBlogTypeID = 0;
            iBlogTypeID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmBlogTypesAddModify.aspx?action=edit&iBlogTypeID=" + iBlogTypeID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iBlogTypeID
             int iBlogTypeID = 0;
             iBlogTypeID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmBlogTypesView.aspx?action=delete&iBlogTypeID=" + iBlogTypeID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region BlogTypes FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtBlogTypes = clsBlogTypes.GetBlogTypesList(FilterExpression, "");
        List<string> glstBlogTypes = new List<string>();

        if (dtBlogTypes.Rows.Count > 0)
        {
            foreach (DataRow dtrBlogTypes in dtBlogTypes.Rows)
            {
                glstBlogTypes.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrBlogTypes["strTitle"].ToString(), dtrBlogTypes["iBlogTypeID"].ToString()));
            }
        }
        else
            glstBlogTypes.Add("No BlogTypes Available.");
        strReturnList = glstBlogTypes.ToArray();
        return strReturnList;
    }

    #endregion
}
