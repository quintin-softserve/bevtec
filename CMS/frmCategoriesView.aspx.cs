using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for clsCategoriesView
/// </summary>
public partial class CMS_clsCategoriesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCategoriesList;

    List<clsCategories> glstCategories;

    private static string lstCategoriesValue;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../frmAdminLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtCategoriesList"] = null;
            //popCategoryType();
            PopulateFormData("");
            popCategory();
        }
        else
        {
            if (Session["glstCategories"] == null)
            {
                glstCategories = new List<clsCategories>();
                Session["glstCategories"] = glstCategories;
            }
            else
                glstCategories = (List<clsCategories>)Session["glstCategories"];
        }
        PopulateFormData("");
    }

    protected void lstCategoryFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        int iCategoryID = Convert.ToInt32(lstCategoryFilter.SelectedValue.ToString());

        if (iCategoryID != -1)
            PopulateFormData("iLevelID=" + iCategoryID);
        else
            PopulateFormData("");
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";

        DataTable dtCategoriesList = clsCategories.GetCategoriesList(FilterExpression, "");

        Session["dtCategoriesList"] = dtCategoriesList;

        PopulateFormData("");
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCategoriesAddModify.aspx");
    }

    //protected void lstCategoryType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    dtCategoriesList = new DataTable();
    //    if (lstCategoryType.SelectedValue != "0")
    //    {
    //        dtCategoriesList = clsCategories.GetCategoriesList("iCategoryTypeID = " + lstCategoryType.SelectedValue.ToString(), "strTitle");
    //    }
    //    else
    //    {
    //        dtCategoriesList = clsCategories.GetCategoriesList("", "strTitle");
    //    }

    //    dgrGrid.DataSource = dtCategoriesList;

    //    Session["dtCategoriesList"] = dtCategoriesList;

    //    dgrGrid.DataBind();

    //    lstCategoriesValue = lstCategoryType.SelectedValue;
    //}
   
    #endregion

    #region POPULATE METHODS
    //private void popCategoryType()
    //{
    //    DataTable dtCategoryTypesList = new DataTable();
    //    lstCategoryType.DataSource = clsCategoryTypes.GetCategoryTypesList();

    //    //### Populates the drop down list with PK and TITLE;
    //    lstCategoryType.DataValueField = "iCategoryTypeID";
    //    lstCategoryType.DataTextField = "strTitle";

    //    //### Bind the data to the list;
    //    lstCategoryType.DataBind();

    //    //### Add default select option;
    //    lstCategoryType.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}

    private void popCategory()
    {
        DataTable dtCategorysList = new DataTable();
        lstCategoryFilter.DataSource = clsCategories.GetCategoriesList("iLevelID=0", "strTitle ASC");

        //### Populates the drop down list with PK and TITLE;
        lstCategoryFilter.DataValueField = "iCategoryID";
        lstCategoryFilter.DataTextField = "strTitle";

        //### Bind the data to the list;
        lstCategoryFilter.DataBind();

        //### Add default select option;
        //lstCategoryFilter.Items.Insert(0, new ListItem("--Not Selected--", "-2"));
        lstCategoryFilter.Items.Insert(0, new ListItem("-- Top Level Menu Items --", "0"));
        lstCategoryFilter.Items.Insert(0, new ListItem("-- All --", "-1"));
    }
    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData(string strFilter)
    {
        //### Populate datagrid using clsCategoriesList object
        try
        {
            dtCategoriesList = new DataTable();

            if (Session["dtCategoriesList"] == null)
                dtCategoriesList = clsCategories.GetCategoriesList(strFilter, "strTitle");
            else
                dtCategoriesList = (DataTable)Session["dtCategoriesList"];

            dgrGrid.DataSource = dtCategoriesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CategoriesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CategoriesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CategoriesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCategoryID = int.Parse((sender as LinkButton).CommandArgument);

        clsCategories.Delete(iCategoryID);

        PopulateFormData("");
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CategoriesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData("");
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCategoriesList = new DataTable();

        if (Session["dtCategoriesList"] == null)
            dtCategoriesList = clsCategories.GetCategoriesList();
        else
            dtCategoriesList = (DataTable)Session["dtCategoriesList"];

        DataView dvTemp = dtCategoriesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCategoriesList = dvTemp.ToTable();
        Session["dtCategoriesList"] = dtCategoriesList;

        PopulateFormData("");
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCategoryID
            int iCategoryID = 0;
            iCategoryID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCategoriesAddModify.aspx?action=edit&iCategoryID=" + iCategoryID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iCategoryID
            int iCategoryID = 0;
            iCategoryID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmCategoriesView.aspx?action=delete&iCategoryID=" + iCategoryID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Categories FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {


        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";

        //if (lstCategoriesValue != "0" && lstCategoriesValue != null)
        //{
        //    FilterExpression += "AND iCategoryTypeID = " + lstCategoriesValue;
        //}

        DataTable dtCategories = clsCategories.GetCategoriesList(FilterExpression, "");
        List<string> glstCategories = new List<string>();

        if (dtCategories.Rows.Count > 0)
        {
            foreach (DataRow dtrCategories in dtCategories.Rows)
            {
                glstCategories.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCategories["strTitle"].ToString(), dtrCategories["iCategoryID"].ToString()));
            }
        }
        else
            glstCategories.Add("No Categories Available.");
        strReturnList = glstCategories.ToArray();
        return strReturnList;
    }
    #endregion
}


