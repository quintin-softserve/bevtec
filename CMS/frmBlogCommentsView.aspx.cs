
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsBlogCommentsView
/// </summary>
public partial class CMS_clsBlogCommentsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtBlogCommentsList;

    List<clsBlogComments> glstBlogComments;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strName +' '+ strEmailAddress +' '+ strPhoneNumber +' '+ strMessage) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtBlogCommentsList = clsBlogComments.GetBlogCommentsList(FilterExpression, "");

        Session["dtBlogCommentsList"] = dtBlogCommentsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmBlogCommentsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsBlogCommentsList object
        try
        {
            dtBlogCommentsList = new DataTable();

            if (Session["dtBlogCommentsList"] == null)
                dtBlogCommentsList = clsBlogComments.GetBlogCommentsList();
            else
                dtBlogCommentsList = (DataTable)Session["dtBlogCommentsList"];

            dgrGrid.DataSource = dtBlogCommentsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["BlogCommentsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["BlogCommentsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["BlogCommentsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iBlogCommentID = int.Parse((sender as LinkButton).CommandArgument);

        clsBlogComments.Delete(iBlogCommentID);

        PopulateFormData();
        Session["dtBlogCommentsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["BlogCommentsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtBlogCommentsList = new DataTable();

        if (Session["dtBlogCommentsList"] == null)
            dtBlogCommentsList = clsBlogComments.GetBlogCommentsList();
        else
            dtBlogCommentsList = (DataTable)Session["dtBlogCommentsList"];

        DataView dvTemp = dtBlogCommentsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtBlogCommentsList = dvTemp.ToTable();
        Session["dtBlogCommentsList"] = dtBlogCommentsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iBlogCommentID
            int iBlogCommentID = 0;
            iBlogCommentID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmBlogCommentsAddModify.aspx?action=edit&iBlogCommentID=" + iBlogCommentID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region BlogComments FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strName +' '+ strEmailAddress +' '+ strPhoneNumber +' '+ strMessage) LIKE '%" + prefixText + "%'";
        DataTable dtBlogComments = clsBlogComments.GetBlogCommentsList(FilterExpression, "");
        List<string> glstBlogComments = new List<string>();

        if (dtBlogComments.Rows.Count > 0)
        {
            foreach (DataRow dtrBlogComments in dtBlogComments.Rows)
            {
                glstBlogComments.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrBlogComments["strName"].ToString() +' '+"" + dtrBlogComments["strEmailAddress"].ToString() +' '+"" + dtrBlogComments["strPhoneNumber"].ToString() +' '+"" + dtrBlogComments["strMessage"].ToString(), dtrBlogComments["iBlogCommentID"].ToString()));
            }
        }
        else
            glstBlogComments.Add("No BlogComments Available.");
        strReturnList = glstBlogComments.ToArray();
        return strReturnList;
    }

    #endregion
}
