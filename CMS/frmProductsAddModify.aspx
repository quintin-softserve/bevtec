<%@ Page Title="Products" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmProductsAddModify.aspx.cs" Inherits="CMS_frmProductsAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" lang="javascript">
        function CheckOnOff(rdoId, gridName) {
            var rdo = document.getElementById(rdoId);
            /* Getting an array of all the INPUT controls on the form.*/
            var rdo = document.getElementById(rdoId);
            var all = document.getElementsByTagName("input");
            for (i = 0; i < all.length; i++) {
                /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
                if (all[i].type == "radio" && all[i].id != rdo.id) {
                    var count = all[i].id.indexOf(gridName);
                    if (count != -1) {
                        all[i].checked = false;
                    }
                }
            }
            rdo.checked = true; /* Finally making the clicked radio button CHECKED */
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Title:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,300)" onblur="setValid(this);" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Images:</div>
                <div class="fieldDiv">
                    <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" Style="float: left;" Width="350px" onblur="setValid(this);" />
                    <div style="float: left;">
                        <asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" OnClick="btnUpload_Click" Style="margin-left: 5px;" />
                    </div>
                    <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear: both" />
                    <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                <ItemTemplate>
                                    <table cellspacing="5">
                                        <%# Container.DataItem %>
                                    </table>
                                    <div align="center" style="padding-top: 5px;">
                                        <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <b>
                        <asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
                </div>
                <br class="clearingSpacer" />
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <%--<asp:Label ID="lblDescribe" runat="server" Text="Please make sure file has the name that you wish and that spaces are represented as '-' dashes."></asp:Label>--%>
                <div class="labelDiv">Document:</div>
                <div class="fieldDiv">
                    <asp:FileUpload ID="DocumentUpload" runat="server" CssClass="roundedCornerTextBoxUpload" Style="float: left;" Width="350px" onchange="this.form.submit()" />
                    <%--<div style="float:left;"><asp:LinkButton ID="btnDocumentUpload" runat="server" CssClass="uploadButton" onclick="btnDocumentUpload_Click" style="margin-left:5px;" /></div>--%>
                    <br class="clearingSpacer" />
                    <asp:Label ID="lblUniqueDocumentPath" runat="Server" Visible="false"></asp:Label><br />
                    <asp:Label ID="lblFileSize" runat="Server" Visible="false"></asp:Label>
                    <asp:UpdatePanel ID="udpDocument" runat="server" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <asp:DataList ID="dlDocument" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                <ItemTemplate>
                                    <table cellspacing="5">
                                        <%# Container.DataItem %>
                                    </table>
                                    <div align="center" style="padding-top: 5px;">
                                        <asp:RadioButton ID="rdbMainDocument" runat="server" Checked="true" GroupName="MainDocument" Text="Main Doc" onclick="javascript:CheckOnOff(this.id,'dlDocument');" Visible="false" /><br />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <b>
                        <asp:Label ID="lblUploadDocError" runat="server"></asp:Label></b>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Tag Line:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtTagLine" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Price:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtPrice" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,20)" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <p id="p1">Number of characters left = 300</p>
                <div class="labelDiv">Description:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtDescription" onkeydown="txtOnKeyPress(this);"  runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="2" />
                    <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtDescription" ID="RegularExpressionValidator3" ValidationExpression = "^[\s\S]{5,300}$" runat="server" ErrorMessage=""></asp:RegularExpressionValidator>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Video Link:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtVideoLink" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Size:</div>
                <div class="fieldDiv" style="text-align: left;">
                    <asp:CheckBoxList ID="lstSize" runat="server" CssClass="roundedCornerDropDownList"></asp:CheckBoxList>
                </div>
                <br class="clearingSpacer" />
            </div>

            <%--<div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Colour:</div>
                <div class="fieldDiv" style="text-align: left;">
                    <asp:CheckBoxList ID="lstColour" runat="server" CssClass="roundedCornerDropDownList"></asp:CheckBoxList>
                </div>
                <br class="clearingSpacer" />
            </div>--%>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Categories:</div>
                <div class="fieldDiv" style="text-align: left;">
                    <asp:CheckBoxList ID="lstCategories" runat="server" CssClass="roundedCornerDropDownList"></asp:CheckBoxList>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Stock Code:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtStockCode" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,80)" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Style:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtStyle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,150)" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Weight:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtWeight" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,20)" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Contents:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtContents" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,80)" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="dummyHolder"></div>
                </div>
                <div class="labelDiv">Warranty:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtWarranty" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,450)" TextMode="MultiLine" Rows="4" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="Line"></div>
            <div class="buttonsRightDiv">
                <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
                <asp:Button ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
                <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="lnkbtnSave" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        function txtOnKeyPress(txt1) {
            var x = 900;

            if (x > 0) {
                x = x - 1;
            }
            x = 300 - document.getElementById('<%=txtDescription.ClientID%>').value.length;
               x.toString();

               document.getElementById("p1").innerHTML = "Number of characters left = " + x;

           }
    </script>
</asp:Content>
