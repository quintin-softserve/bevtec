﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Bevtec_Checkout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["BevtecOrder"] != null) && (Session["BevtecOrder"].ToString() != "0"))
        {
            StringBuilder sbRows = new StringBuilder();
            StringBuilder sbBackOrder = new StringBuilder();

            int iRowCount = 0;
            int iProductID = 0;
            string iQuantity = "";

            string[] strCartItems = HttpContext.Current.Session["BevtecOrder"].ToString().Split(new char[] { ',' });

            foreach (string value in strCartItems)
            {
                if (!String.IsNullOrEmpty(value))
                {
                    iRowCount++;

                    string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                    iProductID = Convert.ToInt32(strCartItemDetails[0]);
                    iQuantity = strCartItemDetails[1];


                    sbRows.Append(BuildCartRows(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity)));
                }
                else
                {
                    sbRows.Append("<div style='text-align: center; padding: 20px;'>No Items currently in your cart.</div>");
                }
            }

            litGridData.Text = sbRows.ToString();
        }
        else
        {
            litGridData.Text = "<div style='text-align: center; padding: 20px;'>No Items currently in your cart.</div>";
        }
        popTotals();
    }

    protected string BuildCartRows(int iProductID, int iQuantity)
    {

        StringBuilder sbGrid = new StringBuilder();
        {
            clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));
            sbGrid.Append(@"<tr class='cart_item'>"
                        + "<td class='product-name'>" + clsProducts.strTitle + "&nbsp; <strong class='product-quantity'>&times; " + iQuantity + "</strong>"
                        + "</td>"
                        + "<td class='product-total'>"
                        + "<span class='amount'>R" + (clsProducts.dblPrice * Convert.ToInt32(iQuantity)).ToString("N2") + "</span>"
                        + "</td>"
                        + "</tr>");
            return sbGrid.ToString();
        }
    }
    protected void popTotals()
    {
        double dblCartTotal = clsCart.GetLoggedOutOrderTotal();
        double dblFinalTotal = dblCartTotal;
        spanTotal.InnerHtml = "R" + dblFinalTotal.ToString("n2");
    }
    protected string BuildCartOrderRows(int iProductID, int iQuantity)
    {
        StringBuilder sbGrid = new StringBuilder();
        {
            clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

            sbGrid.Append(@"<td align='center'>
                                <img src='http://bevtec.client-demo.co.za/" + getProductImage(Convert.ToInt32(iProductID)) + "' alt='' height='30px' style='border: none;' />"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span style=\"font-family:Arial; font-size:13px;\"><div style='padding-left:10px; padding-right: 10px;'>" + clsProducts.strTitle + "</div><br/></span>"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span  style=\"font-family:Arial; font-size:13px;\">" + iQuantity + "<s/pan>"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span  style=\"font-family:Arial; font-size:13px;\">" + (clsProducts.dblPrice * Convert.ToInt32(iQuantity)).ToString("N2") + "</span>"
                        + "</td>"
                        + "<td>"
                        );

            return sbGrid.ToString();
        }
    }

    protected string BuildItemList()
    {
        //if ((Session["loggedOutOrder"] != null) && (Session["loggedOutOrder"].ToString() != "0"))
        //{
        StringBuilder sbRows = new StringBuilder();

        int iRowCount = 0;
        string iProductID = "";
        string iQuantity = "";

        string[] strCartItems = HttpContext.Current.Session["BevtecOrder"].ToString().Split(new char[] { ',' });

        foreach (string value in strCartItems)
        {
            if (!String.IsNullOrEmpty(value))
            {
                iRowCount++;

                if (iRowCount % 2 == 0)
                {
                    sbRows.Append(@"<tr class='dgrAltItem'>");
                }
                else
                {
                    sbRows.Append(@"<tr class='dgrItem'>");
                }

                string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                iProductID = strCartItemDetails[0];
                iQuantity = strCartItemDetails[1];
                sbRows.Append(BuildCartOrderRows(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity)));
                sbRows.Append("</tr>");
            }
        }

        return sbRows.ToString();
    }

    protected void SendMail()
    {
        String address = txtPhysicalAddress.Text + txtBulling.Text + txtState.Text;
        Attachment[] empty = new Attachment[] { };
        StringBuilder strbMail = new StringBuilder();

        string strContent = "";
        //quintin@softservedigital.co.za
        string strFrom = "quintin@softservedigital.co.za";
        string strTo = txtEmailAddress.Text; //txtEmailAddress.Text; //clsAccountHolders.strEmailAddress;
        string strName = txtContactPerson.Text; //clsAccountHolders.strEmailAddress;

        //string strTo = "renier@softservedigital.co.za";

        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">Dear " + strName + "<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">Thank you for your order.<br /><br /></sapn>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<b style=\"font-family:Arial; font-size:14px;\">Delivery address:</b><br />");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\">" + address + "</span><br/>");
        strbMail.AppendLine("<br/");
        strbMail.AppendLine("<b style=\"font-family:Arial; font-size:14px;\">Contact Number:</b><br />");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\">" + txtContactNumber.Text + "<br /></span");
        strbMail.AppendLine("<br/");
        strbMail.AppendLine("<table cellpadding=\"3\" cellspacing=\"0\">");
        strbMail.AppendLine("<tr class=\"dgrHeader\">");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\"></td></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Product Name</td></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Quantity</td><span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Price</td></span>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine(BuildItemList());
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><tr><td><br />Total R" + (clsCart.GetLoggedOutOrderTotal().ToString("N2")) + "</td></tr></span>");
        strbMail.AppendLine("</table>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<br/>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">If you have not joined us on facebook, please visit our  <a href='#' style='color:#21285f;'>Facebook page</a><br /></span>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");
        strContent = strbMail.ToString();
        clsCommonFunctions.SendMail(strFrom, strTo, "", "", "Bevtec - Order", strContent, empty, true);//andrew@softservedigital.co.za
    }

    protected void SendAdminMail()
    {
        String address = txtPhysicalAddress.Text + txtBulling.Text + txtState.Text;
        Attachment[] empty = new Attachment[] { };
        StringBuilder strbMail = new StringBuilder();

        //clsAccountHolders clsAccountHoldersRecord = new clsAccountHolders(clsAccountHolders.iAccountHolderID);

        string strContent = "";
        string strFrom = "quintin@softservedigital.co.za";
        string strTo = "quintin@softservedigital.co.za"; //"orders@consciousbirth.co.za";//txtEmailAddress.Text; //clsAccountHolders.strEmailAddress;
        string strName = txtContactPerson.Text; //clsAccountHolders.strEmailAddress;

        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">Dear Admin,<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">You have a new order placed by " + strName + ".<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><b>Email:</b><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">" + txtEmailAddress.Text + "<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><b>Contact Number:</b><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">" + txtContactNumber.Text + "<br /><br /></span>");
        strbMail.AppendLine("<b style='font-family:Arial; font-size:14px;'>Delivery address:</b><br />");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\">" + txtPhysicalAddress.Text + "<br /><br /></span>");
        strbMail.AppendLine("<b style=\"font-family:Arial; font-size:14px;\"></b><br /><br />");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<table cellpadding=\"3\" cellspacing=\"0\">");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Product Name</td></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Quantity</td></span>");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Price</td></span>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine(BuildItemList());
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><tr><td><br /> Total R" + (clsCart.GetLoggedOutOrderTotal().ToString("N2")) + "</td></tr></span>");
        strbMail.AppendLine("</table>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");
        strbMail.AppendLine("<div style='text-align: center;'>");
        strbMail.AppendLine("<centre >");
        strbMail.AppendLine("<h3>Message</h3> ");
        strbMail.AppendLine(txtOrder.Text);
        strbMail.AppendLine("</centre >");
        strbMail.AppendLine("</div>");


        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail(strFrom, strTo, "quintin@softservedigital.co.za", "", "Bevtec - Order", strContent, empty, true);// 
        //orders@consciousbirth.co.za
        //    andrew@softservedigital.co.za
    }
    protected string getProductImage(int iProductID)
    {
        clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

            foreach (string strImageProduct in strImagesProduct)
            {
                if (strImageProduct.Contains("_sml"))
                {
                    strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
                    break;
                }
            }
        }
        catch
        {
        }

        return strHTMLImageProduct;
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {

        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtContactPerson);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhysicalAddress);
        bCanSave = clsValidation.IsNullOrEmpty(txtLastname);
        bCanSave = clsValidation.IsNullOrEmpty(txtState);
        bCanSave = clsValidation.IsNullOrEmpty(txtBulling);



     if ((bCanSave == true) && ((Convert.ToBoolean(hfCanSave.Value)) == true))
        {
            mandatoryDiv.Visible = false;
            SendMail();
            SendAdminMail();
            Response.Redirect("Bevtec-Thank-youaspx.aspx");
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div style='color: red;' class=\"madatoryPaddingDiv\" >Please fill out all mandatory fields </div></div>";

        }

    }
}